from lxml import etree
from pathlib import Path
from natsort import natsorted

def div_gathering(folder):
	# variables et canvas
	namespaces = {'tei': 'http://www.tei-c.org/ns/1.0'}
	parser = etree.XMLParser(recover=True)
	xml_folder = Path.cwd() / "xml" / folder

	canvas = f"""<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
	<teiHeader>
		<fileDesc>
			<titleStmt>
				<title>Factum</title>
			</titleStmt>
			<publicationStmt>
				<publisher>Centre de recherches historiques</publisher>
				<availability status="restricted">
					<licence target="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"/>
				</availability>
			</publicationStmt>
			<sourceDesc>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="2023-09"/>
			</creation>
			<langUsage>
				<language ident="fr"/>
			</langUsage>
			<textClass>
				<keywords ana="litiges">
					<term/>
				</keywords>
				<keywords ana="parties">
					<term/>
				</keywords>
				<keywords ana="juridiction">
					<term/>
				</keywords>
			</textClass>
		</profileDesc>
	</teiHeader>
	<text>
		<body>
		</body>
	</text>
</TEI>"""
	
	# on parse le canvas
	tree = etree.fromstring(canvas, parser=parser)
	body_canvas = tree.xpath('.//tei:body', namespaces=namespaces)

	# on récupère les divs des factums dans les fichiers individuels
	xml_files = xml_folder.glob('*.xml')
	xml_files = natsorted(xml_files, key=str)
	for file in xml_files:
		print(file)
		root = etree.parse(str(file))
		div = root.xpath('.//tei:body//tei:div', namespaces=namespaces)
		num_tag = root.xpath('.//tei:body//tei:div[@n]', namespaces=namespaces)
		num = num_tag[0].attrib["n"]

		# Obtenez le premier élément <head> dans la <div>
		head_element = div[0].find(".//tei:head", namespaces=namespaces)

		# Vérifiez si le <head> existe, s'il n'existe pas, créez-le
		if head_element is None:
				head_element = etree.Element("{http://www.tei-c.org/ns/1.0}head")
				div[0].insert(0, head_element)

		# Récupérez le texte existant du <head> (s'il existe)
		existing_text = head_element.text if head_element.text else ""

		# Modifiez le texte du <head> pour ajouter "N° num " au début
		head_element.text = f"n° {num} {existing_text}"

		# Ajoutez les div individuels dans le canvas
		body_canvas[0].append(div[0])

	# print(etree.tostring(body_canvas[0], pretty_print=True).decode('utf-8'))
	result = etree.tostring(tree, encoding='utf-8', xml_declaration=True, doctype="")
	xml_string = result.decode('utf-8')
	
	# on écrit le tout dans le fichier du dossier output
	output_folder = Path.cwd() / "xml" / "output_from_script"
	filename = folder + ".xml"
	with open(output_folder / filename, "w") as writting:
		writting.write(xml_string)
