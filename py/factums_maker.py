#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2022-08-09
Update : 2023-03-30
"""

import os
import re
from bs4 import BeautifulSoup
from lxml import etree

from .metadata_csv import fact_metadata


def div_separator(xml_file, collection, path_coll)-> None:
    """takes each TEI/text/body/div and makes a new file
    with it, with a brand new teiheader
    :param xml_file: path to the xml file with <div>
    :type xml_file: str
    :param collection: collection name
    :type collection: str
    """
    fact_info = fact_metadata(os.path.join ("..", "factums_export_avril_2023.csv"))

    soup = BeautifulSoup(xml_file, 'xml')
    header = soup.teiHeader
    div_list = soup.find_all('div', {'type' : 'factum'})
    div_dict = {}
    for div in div_list:
        div_dict[div] = header

    # TEIHEADER : new_respStmt
    new_respStmt = soup.new_tag('respStmt')
    new_resp = soup.new_tag('resp')
    new_resp.string = 'Fichier XML-TEI encodé en 2023'
    new_orgName = soup.new_tag('orgName')
    new_orgName.string = 'Centre de recherches historiques (UMR 8558/ CNRS-EHESS)'
    new_respStmt.append(new_resp)
    new_respStmt.append(new_orgName)

    # TEIHEADER : editionStmt
    new_editionStmt = soup.new_tag('editionStmt')
    new_edition = soup.new_tag('edition')
    new_editionStmt.append(new_edition)
    """individuals = {'Marraud': ['Mathieu', 'Chargé de recherches du CNRS, ', 'direction scientifique'],
                   'Généro': ['Jean-Damien', 'Ingénieur d\'études du CNRS, ', 'direction technique']}
    for individual in individuals:
        indiv_respStmt = soup.new_tag('respStmt')
        indiv_persName = soup.new_tag('persName')
        forename = soup.new_tag('forename')
        forename.string = individuals[individual][0]
        surname = soup.new_tag('surname')
        surname.string = individual
        affiliation = soup.new_tag('affiliation')
        affiliation.string = individuals[individual][1]
        indiv_orgName = soup.new_tag('orgName')
        indiv_orgName.string = 'Centre de recherches historiques'
        indiv_resp = soup.new_tag('resp')
        indiv_resp.string = individuals[individual][2]
        affiliation.append(indiv_orgName)
        indiv_persName.append(forename)
        indiv_persName.append(surname)
        indiv_persName.append(affiliation)
        indiv_respStmt.append(indiv_persName)
        indiv_respStmt.append(indiv_resp)
        new_editionStmt.append(indiv_respStmt)"""

    # TEIHEADER : msDesc
    msDesc = soup.new_tag('msDesc')
    msIdentifier = soup.new_tag('msIdentifier')
    repository = soup.new_tag('repository')
    idno = soup.new_tag('idno')
    head = soup.new_tag('head')
    ms_title = soup.new_tag('title')
    origDate = soup.new_tag('origDate')
    msIdentifier.append(repository)
    msIdentifier.append(idno)
    msDesc.append(msIdentifier)
    head.append(ms_title)
    head.append(origDate)
    msDesc.append(head)
    soup.sourceDesc.append(msDesc)

    # TEIHEADER : titleStmt
    unwanted = ['n° ', '[', ']', ',', '.', ')', '(', "'", "’", "\""]
    letters =  ['é', 'è', 'î', 'ê', 'ù', 'â', 'û']
    counter = 0
    pattern = re.compile(r'n° (\d+) ([\w \.\-\-èéîêá,]+),? ?([\w \.\d\-\-\(\)\/]+)?( n° ? ?\d+)? \[(\d+)(env\.)?\] \[([\wé]+)\]')
    # possible d'avoir une erreur à cause de , ([\w \.\d\-\-\(\)]+)? : remettre la virgule entre ()
    
    for div in div_dict:
        counter += 1
        orig_title = div.head.text
        print(orig_title)
        # ex. 1 orig_title : n° 0318 Arch. Ordre des Avocats [1763] [imprimé]
        # ex. 2 orig_title : n° 0291 Bib. Ste-Geneviève, 4 Z 2283 INV 2375 FA [1775] [imprimé]
        # NB : orig_title is the same string than div.head.text
        title = re.sub(pattern, collection + ' (n°\\1, \\2, \\5)', orig_title)
        date = re.sub(pattern, "\\5", orig_title)
        fact_id = re.sub(pattern, "\\1", orig_title)

        ## TITLE = /titleStmt/title ##
        ### 1.1/
        # if you want only the first sentence of the text to be the file title :
        # full_title = div.p.hi.string
        ### 1.2/
        # if you want the title from the filemaker base :
        # query on the dict to get full title :
        full_title = fact_info[fact_id][0]
        print(full_title)
        ### 2/ the title is made with : the collection name, the file order in the collection,
        ###    the title (from 1.1 OR 1.2), the date.
        div_dict[div].title.string.replace_with(collection + ' (n°' + str(counter).zfill(3) + '). "' + full_title +'" (' + date + ")")
        ### 3/ adding @type and @level to <title>.
        div_dict[div].title['type'] = 'main'
        div_dict[div].title['level'] = 'm'

        # making teiheader
        soup.titleStmt.append(new_respStmt)
        soup.titleStmt.insert_after(new_editionStmt)
        soup.edition.string = re.sub(pattern, '\\2, \\3, \\5, \\7', orig_title)
        # soup.edition.string = 'Édition et encodage du factum ' + collection + ', ' + div.head.text
        # soup.edition.string = re.sub(r'n° \d+ (.+)', '\\1', div.head.text)
        soup.availability['status'] = 'free'
        soup.licence['target'] = 'https://github.com/etalab/licence-ouverte/blob/master/open-licence.md'
        soup.licence.string = 'Distributed under an Open License 2.0'

        info = re.search(pattern, str(soup))
        if info.group(3):
            soup.repository.string = info.group(2)
            soup.idno.string = info.group(3)
            soup.sourceDesc.head.title.string = orig_title
            soup.origDate['when'] = info.group(5)
            soup.origDate.string = info.group(5)
        else:
            soup.repository.string = info.group(2)
            soup.sourceDesc.head.title.string = orig_title
            soup.origDate['when'] = info.group(5)
            soup.origDate.string = info.group(5)

        # PROFILEDESC > keywords
        cats = {"litiges": 1, "parties": 2, "juridiction": 3}
        for cat in cats:
            for keyword in div_dict[div].find_all("keywords", {'ana': True}):
                keyword.term.string = fact_info[fact_id][cats[keyword["ana"]]]

        # making tei tree
        tei_tree = f"""<?xml version="1.0" encoding="utf-8"?>
<TEI xml:lang="fr" xmlns="http://www.tei-c.org/ns/1.0">
{div_dict[div]}
<text>
<body>
{div}
</body>
</text>
</TEI>"""

        # main file name
        title_file = re.sub(r'(' + collection + r' \(n°\d+\)).+', '\\1', div_dict[div].title.string)
        for string in unwanted:
            title_file = title_file.replace(string, '')
        for letter in letters:
            title_file = title_file.replace(letter, 'e')
        title_file = title_file.replace('(', '_').replace('-', '_').replace('n°', '')
        title_file = title_file.lower().replace(' ', '_').replace("é", "e")
        filename = os.path.join('.', 'xml', path_coll, title_file + '.xml')
        # add bold italic for "Contre..." line
        if ". Contre" in full_title:
            tei_tree = tei_tree.replace('<p><hi rend="italic">Contre ',
                '<p><hi rend="bolditalic">Contre ')
        # remove file entry number from <head>
        tei_tree = re.sub(r'(<head>)n°[  ]\d+ ',
            "\\1", tei_tree)
        # changing title
        tei_tree = re.sub(r'(<title level="m" type="main">)' + collection + r' \(n°(\d+)\)\. ',
            '\\1\\2. ', tei_tree)
        # writting file
        with open(filename, 'w', encoding='utf-8') as writting:
            print(filename, "\n")
            writting.write(tei_tree)


def add_new_div(new_xml: str, old_xml: str, dict_index: dict) -> None:
    """
    Inserts <div> elements from a new TEI XML file into a specified 
    location in an old TEI XML file.
    :param new_xml: The filepath of the new TEI XML file containing 
                    the <div> elements to be inserted.
    :param old_xml: The filepath of the old TEI XML file into which 
                    the <div> elements should be inserted.
    :param dict_index: A dictionary containing the mapping of 
                    <div> @n values in the new XML file to 
                    corresponding <div> n values in the old XML file.
    :return: none. The old XML file is modified in-place with the 
                    inserted <div> elements.
    """
    namespaces = {'tei': 'http://www.tei-c.org/ns/1.0'}
    parser = etree.XMLParser(recover=True)

    # Open the XML files with lxml
    tree_new = etree.parse(new_xml, parser)
    with open(old_xml, 'r') as old:
        tree_old = etree.parse(old, parser)

        # Find the <div> elements in both XML trees
        new_divs = tree_new.xpath(".//div")
        print(new_divs)
        old_divs = tree_old.xpath(".//tei:div", namespaces=namespaces)
        print(old_divs)

        # Loop through the new <div> elements to be inserted
        for item in new_divs:
            n = item.attrib["n"]
            pattern = ".//tei:div[@n='" + str(dict_index[n]) + "']"

            # Find the corresponding <div> element in the old_xml tree
            old_div = tree_old.xpath(pattern, namespaces=namespaces)

            # Insert the new <div> element after the corresponding element in the old_xml tree
            if old_div:
                old_div[0].addnext(item)

    # Write the result to the old_xml file
    with open(old_xml, 'wb') as byte_old:
        byte_old.write(etree.tostring(tree_old, encoding='utf-8', method='xml', pretty_print=True))
