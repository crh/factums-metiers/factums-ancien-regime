#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2022-03-30
Update : 2023-03-30
"""

import os
import re
from bs4 import BeautifulSoup
from lxml import etree

def pandoc_convert(name) -> None:
    """transform a docx into tei file with pandoc
    :param name: random factum filename from list factums_collections (index 2)
    :type name: str
    """
    os.system("pwd")
    os.system("pandoc -f docx -t tei -o {} -s {}".format(
            os.path.join(".", "xml", "output_from_pandoc", name + ".xml"),  # output
            os.path.join(".", "docx", name + ".docx")  # input
        ))


def regex_cleaning(path):
    """look above at txt_cleaning() function's docstring"""
    with open(path, 'r') as opening:
        text = opening.read()
        # remove page break in paragraphs
        text = re.sub(r'\n([^<])', ' \\1', text)
        text = re.sub(r'</p> +<p>', '</p>\n<p>', text)
        # text = re.sub(r'<note> +<p>', '<note><p>', text)
        # text = re.sub(r'</p>\n</note>', '</p></note>', text)
        text = re.sub(r'\n<hi', ' <hi', text)
        text = re.sub(r'\n</quote>', '</quote>', text)
        # <hi>
        text = text.replace('hi rendition="simple:bold"', 'hi rend="bold"')
        text = text.replace('hi rendition="simple:italic"', 'hi rend="italic"')
        text = text.replace('hi rendition="simple:superscript"', 'hi rend="sup"')
        # add <div> and <head>
        text = re.sub(r'<p( rend="b")?>(<hi rend="[a-z]+">)?(n°[  ](\d+).+)(<\/hi>)?<\/p>',
            '</div>\n<div type="factum" n="\\4">\n<head>\\3</head>', text)
        text = re.sub(r'<body>\n</div>', '<body>', text)
        text = re.sub(r'</body>', '</div>\n</body>', text)
        text = re.sub(r'<head>(.+)<\/hi>', '<head>\\1', text)
        text = re.sub(r'<head>(.+)<hi rend="[a-z]+">', '<head>\\1', text)
        # add page begining
        text = re.sub(r'&lt;([0-9]+)&gt;', '<pb n="\\1"/>', text)
        # delete line break before and after <note>
        text = re.sub(r'<note>\n *<p', '<note><p', text)
        text = re.sub(r'</p>\n *</note>', '</p></note>', text)
        # remove spaces
        text = re.sub(r"  +", " ", text)
        text = re.sub(r'([a-z]>) (<[a-z])', '\\1\\2', text)
        text = text.replace('n° ', 'n° ')
        # and « »
        text = re.sub(r"«[  ]", '" ', text)
        text = re.sub("[  ]»", ' "', text)
        # add indent in <p>
        text = re.sub(r'<p>\+(\d)\+', '<p rend="lmarg\\1">', text)
        text = re.sub(r'<p>(<hi rend="[a-z]+">)\+(\d)\+', '<p rend="lmarg\\2">\\1', text)
        # images
        text = re.sub(r'===IMAGE=([\w\d\-\.]+)===',
            '<figure><graphic url="./img/\\1"/></figure>', text)
        # remove xml declaration to prevent the "ValueError: Unicode strings
        # with encoding declaration are not supported. Please use bytes input
        # or XML fragments without declaration."
        text = re.sub(r'<\?xml version="1.0" encoding="utf-8"\?>\n', '', text)
        text = text.replace("’", "'")
    return text

def xml_cleaning(xml):
    """look above at txt_cleaning() function's docstring"""
    # Add good teiHeader
    # 1/ parsing the tree
    parser = etree.XMLParser()
    root = etree.fromstring(xml, parser)

    # 2/ remove unused namespace declarations
    for elem in root.getiterator():
        # Skip comments and processing instructions,
        # because they do not have names
        if not (
            isinstance(elem, etree._Comment)
            or isinstance(elem, etree._ProcessingInstruction)
        ):
            # Remove a namespace URI in the element's name
            elem.tag = etree.QName(elem).localname
    etree.cleanup_namespaces(root)

    # 3/ add fileDesc + profileDesc
    old_element_parent = root.find('.//fileDesc/..')  # fileDesc parent = teiHeader
    old_element_parent.remove(root.find('.//fileDesc')) # remove old fileDesc
    # add new fileDesc in teiHeader (old_element_parent)
    old_element_parent.append(etree.XML(f"""<fileDesc>
      <titleStmt>
        <title>Factum</title>
      </titleStmt>
      <publicationStmt>
        <publisher>Centre de recherches historiques</publisher>
        <availability status="restricted">
          <licence target="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"/>
        </availability>
      </publicationStmt>
      <sourceDesc>
      </sourceDesc>
    </fileDesc>"""))
    # add new profileDesc after new fileDesc
    root.find('.//fileDesc').addnext(etree.XML(f"""<profileDesc>
      <creation>
        <date when="2023-04"/>
      </creation>
      <langUsage>
        <language ident="fr"/>
      </langUsage>
      <textClass>
        <keywords ana="litiges">
          <term/>
        </keywords>
        <keywords ana="parties">
          <term/>
        </keywords>
        <keywords ana="juridiction">
          <term/>
        </keywords>
      </textClass>
    </profileDesc>"""))

    return etree.tostring(root)

def txt_cleaning(xml_file):
    """clean a xml file made by pandoc
    !! result is printed in terminal !!
    :param xml_file: random factum filename from list factums_collections (index 2)
    :type name: str
    """
    file = os.path.join(".", "xml", "output_from_pandoc", xml_file + '.xml')
    fact = regex_cleaning(file)
    with open("./essai.xml", 'w', encoding="utf-8") as w:
        w.write(fact)
    print("REGEX CLEAN 1 DONE")
    result = xml_cleaning(fact)
    print(result)
    with open(os.path.join(".", "xml", "output_from_script", xml_file + '.xml'), 'wb') as w:
        w.write(result)
    print("XML CLEAN 2 DONE")
    return result
