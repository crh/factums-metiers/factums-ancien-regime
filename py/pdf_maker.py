#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2023-02-15
Update : 2023-03-30
"""


import os
import re
import lxml
from lxml import etree
from bs4 import BeautifulSoup


def make_soup(file: str):
	"""open a xml file and return a BeautifulSoup object"""
	with open(file, 'r', encoding="utf-8") as opening:
		xml = BeautifulSoup(opening, 'xml')
	return xml


def make_doc_list(collections):
	"""#"""
	doc_dict = {}
	for collection in collections:
		folder = os.path.join(".", "xml", collection[2])
		for doc in os.listdir(folder):
			if '.xml' in doc:
				soup = make_soup(os.path.join(folder, doc))
				# create new <head>
				head = soup.new_tag("head")
				head["type"] = "factum"
				head.string = soup.find_all("title", {"type": "main"})[0].text
				# add archival reference to head
				head.string.insert_after(" [" + soup.edition.text + "]")
				# search the old <head>
				delete_head = soup.body.find_all("head")
				# add new head before the old one
				soup.div.head.insert_before(head)
				# delete old <head>
				delete_head[0].decompose()
				# add factum number to @n in <div>
				soup.body.div["n"] = re.search(r'(\d{3})\. ',
					str(soup.div.head)).group(1)
				doc_dict[os.path.join(folder, doc)] = soup.div
	return doc_dict


def make_tei_doc(collections, div_dict):
	"""add in a tei template all div from a list"""
	template = f"""<TEI>
  <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Factums des Métiers</title>
            <author>Mathieu Marraud</author>
            <author>Jean-Damien Généro</author>
         </titleStmt>
         <publicationStmt>
            <p>Édition des factums des métiers</p>
         </publicationStmt>
         <sourceDesc>
            <p>Transcriptions et éditions de documents issus de fonds d'archives et de bibliothèques</p>
         </sourceDesc>
      </fileDesc>
  </teiHeader>
  <text>
      <body>
      </body>
  </text>
</TEI>"""
	soup = BeautifulSoup(template, 'xml')
	for collection in collections:
		# make a div by factum collection
		# and add a <head> with collection's title and date
		coll_div = soup.new_tag("div")
		coll_div["type"] = "collection"
		coll_head = soup.new_tag("head")
		coll_head["type"] = "collection"
		coll_head.string = collection[1] + " (" + collection[3] + ")"
		coll_div.append(coll_head)
		# add <div> that corresponds to the current
		# collection from div_dict 
		folder = os.path.join(".", "xml", collection[2])
		for doc in os.listdir(folder):
			full_folder = os.path.join(folder, doc)
			for item in div_dict:
				if full_folder == item:
					coll_div.append(div_dict[item])
		soup.body.append(coll_div)
	# write it all in a full_factum file
	with open('./xml/full_factum.xml', 'w') as foo:
		# escape all #
		result = str(soup).replace("#", "\\#")
		foo.write(result)


def xsl_transformation(xsl_file, xml_file):
    """
    Transforming xml file in tex file with an xslt stylesheet.
    :param xsl_file: path to an xsl stylesheet
    :type xsl_file: str
    :param xml_file: path to an xml file
    :type xml_file: str
    :return: tex file
    :rtype: lxml.etree._XSLTResultTree
    """
    # parsing xml file
    source = etree.parse(xml_file)
    # Remove namespace prefixes
    tei = source.getroot()
    for elem in tei.getiterator():
        elem.tag = etree.QName(elem).localname
    # Remove unused namespace declarations
    etree.cleanup_namespaces(tei)
    # xls transformation
    xsl_doc = etree.parse(xsl_file)
    xsl_transformer = etree.XSLT(xsl_doc)
    output_doc = xsl_transformer(source)
    return output_doc


def tex_compil(xsl_file, xml_file) -> None:
    """
    Compiling a tex document.
    :param xsl_file: path to a xsl style sheet
    :type xsl_file: str
    :param xml_file: path to a xml file that will be transformed
    :type xml_file: str
    :return:
    """
    unnecessaries_extensions = ["toc", "gz", "xml", "out", "blg", "bcf", "bbl", "aux"]
    tex = xsl_transformation(xsl_file, xml_file)
    tex = str(tex).replace("&", "\&")  # escape all &
    tex = tex.replace("_", "\_")  # escape all _
    tex = tex.replace("-\&-", "&") # remove escape for -\&- in tex tables
    tex = tex.replace('" ', '\og ')
    tex = tex.replace(' "', ' \\fg{}')
    tex = re.sub(r" ([?!:,;])", "\\1", tex)
    tex = tex.replace("œ", "\\oe ")
    tex = tex.replace(" ", "~")
    tex = re.sub(r"(\d)°", "\\1\\\\up{o}", tex)
    tex = re.sub(r"M\n +\n +e\n +\n +", r"M\\textsuperscript{e} ", tex)
    tex = re.sub(r"\\footnote{\n", "\\\\footnote{", tex)
    tex = tex.replace("\\caption{Mon image}", "\\caption{}")
    texfolder = "."
    texname = re.sub(r"\.\/(.+)\.xml", "\\1.tex", xml_file)
    logname = re.sub(r"\.\/(.+)\.xml", "\\1.log", xml_file)
    pdfname = re.sub(r"\.\/(.+)\.xml", "\\1.pdf", xml_file)
    texpath = os.path.join(texfolder, texname)
    with open(texpath, 'w', encoding="utf-8") as newtexfile:
        newtexfile.write(str(tex))
    print("***** COMPILATION N°1 *****")
    os.system("xelatex ./{}".format(texpath))
    print("***** COMPILATION N°2 *****")
    os.system("xelatex ./{}".format(texpath))
    print("{} ----> Sucess !".format(pdfname))
    os.system("mv ./{} ./pdf/".format(pdfname))
    os.system("mv ./{} {}".format(logname, texfolder))
    for file_extension in unnecessaries_extensions:
        os.system("rm *.{}".format(file_extension))
        print("*.{} ----> deleted !".format(file_extension))
    print(pdfname)
