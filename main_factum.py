#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2022-09-01
Update : 2023-03-30

NOTICE WHEN ADDING NEW FILE :
1/ Clean docx files : remove the table of content, headers, pagebreak
2/ Add informations to the factums_collections list in main_factums.py
3/ Create the new folder in ./xml
4/ If you're adding new factums to an allready existing factums collection,
   you need to add informations to the new_factums dict in main_factums.py,
   where key = the new <div> @n, value = the previous <div> @n.
   Ex: if you add a new factum numbered 0099 after an existing <div>
   that is numbered 0432, then new_factums = {"0099": "0432"}, so
   <div n="0099"/> will go after <div n="0432"/>

Common errors :
- OSError: [Errno 36] File name too long
  something (such as parantheses) is in the category title
- div_separator(factum_cleaner(factums_collections[int(collection_number)][0]),
  File "factums-ancien-regime/py/factums_maker.py", line 98, in div_separator
    sentence = div.p.hi.string  # factum's 1st sentence, for the title (//titleStmt/title)
	AttributeError: 'NoneType' object has no attribute 'hi'
  there is a "zzz" string at the top of the xml input file. remove it.
- AttributeError: 'NoneType' object has no attribute 'string's
remove 
      <text:tracked-changes xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" text:track-changes="false"/>
- lxml.etree.XMLSyntaxError: Opening and ending tag mismatch: head line 41 and p, line 41, column 123
remove <anchor> tag
"""

import os
import re
from bs4 import BeautifulSoup
from lxml import etree
from pathlib import Path
from natsort import natsorted

from py.factums_cleaner import pandoc_convert, txt_cleaning
from py.factums_maker import div_separator, add_new_div
from py.pdf_maker import make_doc_list, make_tei_doc, xsl_transformation, tex_compil
from py.factums_gathering import div_gathering

# list of lists where [0] = full xml file path / [1] = collection's name / [2] = folder's name / [4] = collection's dates
factums_collections = [
	['./xml/output_from_script/tailleurs_pourpointiers.xml', 
	'Tailleurs d\'habits contre Pourpointiers', 
	'tailleurs_pourpointiers', '1626-1640 env.'],
	['./xml/output_from_script/corps_communautes_commanderie_latran.xml',
	'Corps et communautés contre Commanderie Saint-Jean de Latran',
	'corps_communautes_commanderie_latran', '1635env.-1694'],
	['./xml/output_from_script/drapiers_merciers.xml', 
	'Drapiers contre Merciers', 
	'drapiers_merciers', '1635env.-1687'],
	['./xml/output_from_script/drapiers_drapants.xml', 
	'Drapiers drapants de Beauvais contre Sergers de Beauvais', 
	'drapiers_drapants', '1650env.'],
	['./xml/output_from_script/drapiers_chaussetiers.xml', 
	'Drapiers-chaussetiers contre Merciers, Lingères', 
	'drapiers_chaussetiers', '1664'],
	['./xml/output_from_script/orfevres_graveurs.xml', 
	'Orfèvres contre Graveurs', 
	'orfevres_graveurs', '1665-1733'],
	['./xml/output_from_script/orfevres_lapidaires.xml', 
	'Orfèvres contre Lapidaires', 
	'orfevres_lapidaires', '1666-1777'],
	['./xml/output_from_script/consulat_et_codification_coutumiere.xml',
	'Consulat et codification coutumière', 
	'consulat_codification_coutumiere', '1689-1727'],
	['./xml/output_from_script/vinaigriers_limonadiers.xml', 
	'Vinaigriers contre Limonadiers', 
	'vinaigriers_limonadiers', '1692-1695'],
	['./xml/output_from_script/epiciers_limonadiers.xml', 
	'Épiciers contre Limonadiers', 
	'epiciers_limonadiers', '1705-1776'],
	['./xml/output_from_script/corps_communautes_abbaye_saint_antoine.xml',
	'Corps et communautés contre Abbaye et Faubourg Saint-Antoine',
	'corps_communautes_abbaye_saint_antoine', '1706-1755'],
	['./xml/output_from_script/corps_communautes_temple.xml',
	'Corps et communautés contre Temple',
	'corps_communautes_temple', '1707'],
	['./xml/output_from_script/corps_communautes_bailliage_palais.xml',
	'Corps et communautés contre Bailliage du Palais',
	'corps_communautes_bailliage_palais', '1711'],
	['./xml/output_from_script/corps_communautes_prieure_champs.xml',
	'Corps et communautés contre Prieuré Saint-Martin des Champs',
	'corps_communautes_prieure_champs', '1714-1734'],
	['./xml/output_from_script/corps_communautes_rue_oursine.xml',
	'Corps et communautés contre Rue de l\'Oursine',
	'corps_communautes_rue_oursine', '1718'],
	['./xml/output_from_script/barbiers_perruquiers_lieux_privil.xml',
	'Barbiers-perruquiers contre Lieux privilégiés',
	'barbiers_perruquiers_lieux_privil', '1719-1749'],
	['./xml/output_from_script/drapiers-manuf-draps-abbeville.xml', 
	'Drapiers contre la Manufacture de draps d\'Abbeville', 
	'drapiers_contre_manufacture', '1720-1724'],
	['./xml/output_from_script/metiers_lieux_privil.xml',
	'Métiers contre tous Lieux et Particuliers privilégiés',
	'metiers_lieux_privil', '1720env.-1734'],
	['./xml/output_from_script/corps_communautes_abbaye_saint_germain.xml',
	'Corps et communautés contre Abbaye Saint-Germain des Près',
	'corps_communautes_abbaye_saint_germain', '1723'],
	['./xml/output_from_script/consulat_faillites.xml', 
	'Consulat et faillites', 
	'consulat_faillites', '1733-1770'],
	['./xml/output_from_script/six_corps_derogeance.xml', 
	'Six corps, dérogeance, liberté du commerce en gros', 
	'six_corps_derogeance', '1741-1777'],
	['./xml/output_from_script/epiciers_contre_apothicaires_chef_oeuvre.xml', 
	'Épiciers contre Apothicaires chef d\'oeuvre', 
	'epiciers_contre_apothicaires_chef_oeuvre', '1768-1770'],
	['./xml/output_from_script/epiciers_grainiers.xml', 
	'Épiciers contre Grainiers', 
	'epiciers_grainiers', '1698-1767'],
	['./xml/output_from_script/grainiers_chandeliers.xml', 
	'Grainiers contre Chandeliers', 
	'grainiers_chandeliers', '1690-1693']
]

# dict for new factums added to an allready existing factums collection
new_factums = {"0518": "0393"}

# messages that will be printed in terminal
txt_factums = f"""Enter the number of the collection you want to transform :
* 0  = 'Tailleurs d'habits contre Pourpointiers'
* 1  = 'Corps et communautés contre Commanderie Saint-Jean de Latran'
* 2  = 'Drapiers contre Merciers'
* 3  = 'Drapiers drapants de Beauvais contre Sergers de Beauvais'
* 4  = 'Drapiers chaussetiers'
* 5  = 'Orfèvres contre Graveurs'
* 6  = 'Orfèvres contre Lapidaires'
* 7  = 'Consulat et codification coutumière'
* 8  = 'Vinaigriers contre Limonadiers'
* 9  = 'Épiciers contre Limonadiers'
* 10 = 'Corps et communautés contre Abbaye et Faubourg Saint-Antoine'
* 11 = 'Corps et communautés contre Temple'
* 12 = 'Corps et communautés contre Bailliage du Palais'
* 13 = 'Corps et communautés contre Prieuré Saint-Martin des Champs'
* 14 = 'Corps et communautés contre Rue de l'Oursine'
* 15 = 'Barbiers-perruquiers contre Lieux privilégiés'
* 16 = 'Drapiers contre la Manufacture de draps d'Abbeville'
* 17 = 'Métiers contre tous Lieux et Particuliers privilégiés'
* 18 = 'Corps et communautés contre Abbaye Saint-Germain des Près'
* 19 = 'Consulat et faillites'
* 20 = 'Six corps, dérogeance, liberté du commerce en gros'
* 21 = 'Épiciers contre Apothicaires Chef d'Oeuvre'
* 22 = 'Épiciers contre Grainiers'
* 23 = 'Grainiers contre Chandeliers'"""

answer = input(f"""What do you want to do ?
P = compile the PDF
N = new file
O = old files
Answer : """)

# MAKES THE TEX FILE AND COMPILE THE PDF
if answer == "P":
	make_tei_doc(factums_collections, make_doc_list(factums_collections))
	tex_compil("./xsl/pandoc-factums-xslt.xsl", './xml/full_factum.xml')

# MAKES THE TRANSFORMATION FROM NEW DOCX
elif answer == "N":
	# new file = docx file with new factums
	new_file = input("Enter new file name (whitout odt/ and .docx) : ")
	
	# new_file (.docx) is transformed to tei with pandoc (new_file.xml)
	pandoc_convert(new_file)
	
	# new_file (.xml) is cleaned by txt_cleaning()
	txt_cleaning(new_file)
	
	# the user choices an index in the list of all factums
	print(txt_factums)
	collection_number = input("Your choice : ")
	
	# old_path is the path to the choosen factum XML file
	old_path = factums_collections[int(collection_number)][0]

	# on rassemble les fichiers XML individuels dans le dossier output script
	div_gathering(factums_collections[int(collection_number)][2])
	
	# new_factums_file is the path the new factum XML file made by txt_cleaning()
	new_factums_file = os.path.join(".", "xml", "output_from_script", new_file + '.xml')
	
	# add_new_div() adds <div> from new_factums_file to old_path
	add_new_div(new_factums_file, old_path, new_factums)

	# one file is created for each <div> in old_path
	with open(old_path, 'r') as opening:
		div_separator(opening,
			factums_collections[int(collection_number)][1],
			factums_collections[int(collection_number)][2])


# MAKES THE TRANSFORMATION FROM DOCX
elif answer == "O":
	print(txt_factums)
	collection_number = input("Your choice : ")
	pandoc_convert(factums_collections[int(collection_number)][2])
	print("PANDOC DONE")
	div_separator(txt_cleaning(factums_collections[int(collection_number)][2]),
								 factums_collections[int(collection_number)][1],
								 factums_collections[int(collection_number)][2])
