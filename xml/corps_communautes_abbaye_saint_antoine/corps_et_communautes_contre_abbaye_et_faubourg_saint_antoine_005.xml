<?xml version="1.0" encoding="utf-8"?>
<TEI xml:lang="fr" xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
<fileDesc>
<titleStmt>
<title level="m" type="main">005. "Sommaire de la Requeste presentée au Roy par les Proprietaires des maisons du Fauxbourg de Saint Antoine de Paris." (1717)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, FOL-FM-12951, 1717, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF FOL-FM-12950</repository><idno>F12 781C</idno></msIdentifier><head><title>n° 0479 BNF, FOL-FM-12951 [1717] [imprimé]</title><origDate when="1717">1717</origDate></head></msDesc></sourceDesc>
</fileDesc>
<profileDesc>
<creation>
<date when="2023-09"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>travail privilégié dans les faubourgs — droit de visite — liberté du travail</term>
</keywords>
<keywords ana="parties">
<term>propriétaires du faubourg Saint-Antoine contre commission royale</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc>
</teiHeader>
<text>
<body>
<div n="0479" type="factum">
<head>BNF, FOL-FM-12951 [1717] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Sommaire de la Requeste presentée au Roy par les Proprietaires des maisons du Fauxbourg de Saint Antoine de Paris.</hi></p>
<p>Les Franchises du Fauxbourg de Saint Antoine pour les Ouvriers &amp; gens de Mestier ont subsisté de tous temps.</p>
<p>Au mois d'Octobre 1642, l'envie des Communautez des Arts &amp; Mestiers de Paris surprit de la Religion du Roy Loüis le Juste un Edit pour y établir des Maistrises &amp; Jurandes particulieres ; dès lors les Proprietaires des heritages du Fauxbourg cesserent d'y bastir ; quelques années après, une Armée voulant éviter un engagement avec celle du feu Roy de glorieuse memoire vint se jetter dans ce Fauxbourg ; les maisons furent démolies pour faire des retranchements &amp; en faire un lieu de deffense ; les deux Armées vinrent aux mains dans le Fauxbourg, &amp; le desolerent entièrement ; ce malheur fut suivi d'une inondation extraordinaire qui acheva de le detruire ; les Proprietaires des maisons ne se murent point en peine de les retablir ; l'Edit qui avoit privé le Fauxbourg des franchises leur avoit osté toute esperance de les pouvoir loüer.</p>
<p>Au mois de Février 1657, le feu Roy, informé de la surprise qui avoit esté faite à la Religion du Roy Loüis XIII son pere, revoqua l'Edit de 1642 &amp; retablit le Fauxbourg dans ses franchises &amp; exemptions ; alors les Proprietaires firent des efforts pour retablir leurs maisons, &amp; emprunterent à cet effet les sommes necessaires.</p>
<p>En 1667, le Fauxbourg se trouve rebasti ; alors le feu Roy ordonna que la seconde Compagnie de ses Mousquetaires, qui logeoit au Village de Charenton, fût logée dans ce Fauxbourg ; les Proprietaires furent obligez de leur fournir, les uns le paquet, les autres le logement, ce qu'ils ont fait pendant plus de trente ans.</p>
<p>En 1699, par Arrest du Conseil d'Estat du 16 Octobre, Sa Majesté <pb n="2"/> ordonna qu'il fût basti un Hostel dans le Fauxbourg pour loger cette mesme Compagnie. En vertu de cet Arrest, les Proprietaires ont esté taxez à la somme de cent cinquante mil livres pour le bastiment &amp; emmeublement de cet Hostel, laquelle ils ont payée, &amp; outre cette depense ils ont esté chargez de contribuer dans la suite à ce qui seroit necessaire pour les reparations, entretien &amp; emmeublement du même Hostel.</p>
<p>Cet Hostel n'estoit pas achevé que les Sieurs Prevost des Marchands &amp; Echevins de la Ville ont fait signifier aux Proprietaires un Rolle en vertu duquel ils les ont contraints de payer par chacun an une sommes très considerable ; pour cette contribution, &amp; en vertu d'un nouveau Rolle, ils ont encore voulu les surcharger, ce qui a reduit les Proprietaires à se pourvoir pardevers Sa Majesté, &amp; a donné matiere à l'instance qui est au Conseil au raport de Monsieur de Baudry, M<hi rend="sup">e</hi> des Requestes.</p>
<p>En 1672, en vertu de la declaration du mois d'Avril, les Proprietaires ont esté taxez à des sommes très fortes pour la permission de bastir hors des limites, &amp; depuis peu de temps pour le rachat des Boües &amp; Lanternes, ce qu'ils ont payé.</p>
<p>En cet état, on propose au Conseil du Roy de revoquer, sinon pour le tout, du moins en partie les Lettres Patentes du feu Roy, qui ont confirmé le Privilege du Fauxbourg.</p>
<p>Les Proprietaires repondent que les choses ne sont plus au même état où elles estoient en 1642 ; alors ils ne se plaignirent point de l'Edit qui avoit revoqué le Privilege du Fauxbourg en y établissant des Maistrises &amp; Jurandes, s'imputant à eux-mêmes la faute qu'ils avoient faite de bastir sur la foy d'un Privilege qui estoit à la verité un ancien usage, mais qui n'avoit point pour fondement l'autorité royale, à qui seule il appartient d'établir des Privileges.</p>
<p>Il n'en est pas de même aujourd'huy ; ils ont réédifié leurs maisons, &amp; en ont basti de nouvelles sur la foy d'un Privilege établi par l'autorité royale &amp; par des Lettres Patentes accordées en connoissance de cause ; si c'est une erreur à eux d'avoir réédifié leurs maisons &amp; d'en avoir basti de nouvelles, ils peuvent dire que les Lettres Patentes en sont la cause, &amp; que la grace du feu Roy feroit aujourd'huy leur ruïne ; leur condition seroit bien meileure si Sa Majesté avoit eu moins de bonté pour eux ; ils n'auroient jamais esté sujets au logement des Mousquetaires, ni à toutes les autres charges cy-dessus énoncées, qui font voir que le Privilege de ce Fauxbourg n'est plus gratuit pour les Proprietaires, &amp; que Sa Majesté en a retiré des utilitez qu'elle n'auroit jamais euës si, dès l'année 1642, <pb n="3"/> le Fauxbourg eust repris son ancienne forme de Jardins &amp; de Marais.</p>
<p>On objecte que le Fauxbourg est d'une trop grande étenduë, &amp; contient trop de Privilegiez.</p>
<p>Les Proprietaires repondent que son étenduë est la mesme qu'en 1642, qu'en vertu de la Declaration de 1672, ayant payé de grosses taxes, ils ont eu la permission de bastir dans l'étenduë de son enceinte, que dès l'année 1642 le Fauxbourg contenoit une si grande multitude d'Ouvriers que, par l'Edit du mois</p>
<p>d'Octobre de la mesme année, on y avoit établi des Maîtrises &amp; des Jurandes, &amp; que s'il est à présent bien rebasti &amp; bien repeuplé d'Ouvriers, c'est l'effet des Lettres Patentes qui ont engagé les Proprietaires des maisons de mettre toute leur fortune à les bastir, &amp; de travailler à leur propre ruïne si la beauté où ils ont mis le Fauxbourg &amp; le nombre de ses Habitans est aujourd'huy un motif pour le detruire, plustost que pour le conserver.</p>
<p>Chalopin, Avocat.</p>
<p>De l'Imprimerie de Jean-François Knapen, ruë de la Huchette, à l'Ange.</p>
</div>
</body>
</text>
</TEI>