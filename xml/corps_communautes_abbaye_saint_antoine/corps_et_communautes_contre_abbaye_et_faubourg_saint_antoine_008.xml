<?xml version="1.0" encoding="utf-8"?>
<TEI xml:lang="fr" xmlns="http://www.tei-c.org/ns/1.0">
<teiHeader>
<fileDesc>
<titleStmt>
<title level="m" type="main">008. "Memoire du procureur du Roy en la commission des privileges, sur les privileges et franchises exercées par les dames abbesse et religieuses de l'abbaye de St.Antoine, par rapport aux arts et metiers." (1723)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>A.N., F12 781C, 1723, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>A.N.</repository><idno>F12 781E</idno></msIdentifier><head><title>n° 0439 A.N., F12 781C [1723] [manuscrit]</title><origDate when="1720">1720</origDate></head></msDesc></sourceDesc>
</fileDesc>
<profileDesc>
<creation>
<date when="2023-09"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>travail privilégié dans les faubourgs — droit de visite — liberté du travail</term>
</keywords>
<keywords ana="parties">
<term>commission royale contre abbaye Saint-Antoine</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc>
</teiHeader>
<text>
<body>
<div n="0439" type="factum">
<head>A.N., F12 781C [1723] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Memoire du procureur du Roy en la commission des privileges, sur les privileges et franchises exercées par les dames abbesse et religieuses de l'abbaye de St.Antoine, par rapport aux arts et metiers.</hi></p>
<p>La multiplication qui se faisoit de jour en jour des lieux prétendus privilegiez, et les abus qui s'y etoient glissez, firent etablir en 1716 une commission du Conseil d'Estat pour en connoistre. Par l'arrest du 28 9bre portant etablissement de cette commission, il fut ordonné que toutes personnes qui auroient ou pretendroient avoir dans la ville et fauxbourgs de Paris des droits de justice ou de police, des privileges ou affranchissemens de maitrise, etc., seroient tenues de representer leurs titres de concession et de confirmation pardevant les commissaires nommez par sa Majesté.</p>
<p>Tous les seigneurs qui exercent ou prétendent pouvoir exercer ces privileges et franchises se fondent sur ce qu'ils sont inserapables du droit de police attaché à leur haute justice. Tels sont les abbé et prieur de St.Germain des Prez et de St.Martin des Champs, le grand Prieuré de France, le commandeur de St.Jean de Latran pour l'ordre de Malthe, etc.</p>
<p>Les dames abbesse et religieuses de St.Antoine sont dans un cas particulier. Elles ne peuvent alleguer en leur faveur aucuns droits de haute justice. Car on scait qu'elles ne l'ont jamais possedé.</p>
<p><pb n="2"/> Elles n'ont que le droit de censive. Encore n'a-t-il lieu que pour le district dependant de leur abbaye. Et ce n'est qu'une mediocre partie du fauxbourg St.Antoine, le reste estant sous la censive du Roy, de l'archevesque de Paris, du seigneur de Bercy, etc.</p>
<p>Lorsqu'elles ont esté sommées par le procureur du Roy de la commission de représenter les titres, elles se sont contentées de publier trois memoires imprimés, le premier sous leur nom, et les deux autres sous celuy des proprietaires de maisons et des ouvriers en franchise du fauxburg St.Antoine. Elles y exposent, ou y font exposer qu'elles doivent estre dispensées de produire leurs anciens titres attendu qu'ils ont est bruslez ou soustraits pendant les guerres civiles. Le seul qu'elles représentent est une concession de franchises faite par lettres patentes du mois de fevrier 1657 sous la minorité du Roy Louis XIV, en faveur des pauvres ouvriers de la frontiere de Picardie et de Champagne qui avoient esté contraints de se refugier à Paris après qu'elle eut esté ravagée par l'ennemy. Elles ont encore cité quelques arrests rendus en differents temps sur ces lettres patentes.</p>
<p>Le procureur du Roy de la commission a opposé aux dames abbesse et religieuses</p>
<p>- que l'allegation de l'incendie prétendu de leurs titres n'etoit pas un titre suffisant, qu'elles ne peuvent en avoir eu de valable et de permanent, puisque ce n'auroit pu estre qu'en vertu des droits de haute justice et de police, dont elles n'ont jamais joüy,</p>
<p>- qu'à l'egard des lettres patentes de 1657 elles avoient eu pour principale cause la necessité de donner retraitte aux pauvres ouvriers de la frontiere, au nombre de trois ou quatre cent seulement,</p>
<p>- que la cause de cette concession ayant cessé par la mort de ces ouvriers, l'effet devoit aussy cesser,</p>
<p>- que pour ce qui concerne les arrests du Conseil et du Parlement intervenus en leur faveur sur ces lettres patentes, il ne decident point la question comme elles le pretendent, d'autant qu'on leur suppose un plus grand nombre d'arrests rendu contre elles en pareil cas, surtout l'edit de 1666 en forme de reglement, il attribue aux seuls juges royaux, à l'exclusion de tous autres, l'exercice de la police des arts et metiers sur toute la ville et fauxbourgs de Paris, et par consequent le droit d'y faire faire des visites sous leur autorité par les jurez des arts et metiers, visites contre lesquelles les dames abbesse et religieuses se recrient si vivement, puisqu'elles ne feignent point de declarer dans leur memoire, et en termes exprès, qu'il n'y a rien de plus incompatible avec leurs franchises, qu'il n'y a point de milieu, et qu'il faut supprimer celles-cy, ou supprimer les autres,</p>
<p>- que ces dames avoient manifestement abusé de la concession faite par les lettres patentes, puisqu'au lieu de trois ou quatre cent ouvriers pour qui elles avoient esté accordées, elles y en avoient recus depuis en franchise <pb n="3"/> plus de soixante mil, ainsy qu'elles en conviennent elles mesmes dès la premiere page de leur memoire,</p>
<p>A ces premiers moyens, on a ajouté que quand mesme leur prétendu droit de franchise seroit incontestable, elles seroient au moins obligées de le renfermer dans leur district et ne pourroient l'etendre comme elles ont fait sur toutes les parties du faubourg St.Antoine, soumises à d'autres seigneurs, où elles tiennent sous leur protection ce peuple d'ouvriers privilegiez.</p>
<p>Sur quoy le procureur du Roy de la commission, avant que de prendre des conclusions deffinitives, requit qu'avant de faire droit, lesdites dames abbesse et religieuses fussent tenues de representer pardevant Messieurs les commissaires, outre leurs lettres patentes de 1657, les autres titres sur lesquels elles</p>
<p>prétendent se fonder, et en particulier un concordat qu'elles disent avoir fait avec la ville de Paris, comme aussy de produire une carte topographique du ressort de leur censive, pour donner lieu de connoistre dans quelles bornes devoit estre renfermé leur droit de franchise, supposé qu'il leur fut accordé.</p>
<p>Cette requeste du procureur du Roy de la commission a esté signifiée aux dames abbesse et religieuses en 1717, elles n'y ont point répondu pendant les six années qui se sont écoulées depuis, ce qui fait juger qu'elles manquent et de titres et de raisons solides qu'elles puissent opposer aux objections qui leur ont esté faites.</p>
</div>
</body>
</text>
</TEI>