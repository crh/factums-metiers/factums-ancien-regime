<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">011. "Memoire pour la communauté des maitres et marchands miroitiers lunetiers bimblotiers doreurs sur cuir garnisseurs et enjoliveurs de la ville et fauxbourgs de Paris." (1720)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>A.N., F12 781D, 1720, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>A.N.</repository><idno>F12 781C</idno></msIdentifier><head><title>n° 0460 A.N., F12 781D [1720env.] [manuscrit]</title><origDate when="1720">1720</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>lieux privilégiés — détournement de compagnons</term>
</keywords>
<keywords ana="parties">
<term>miroitiers contre lieux privilégiés</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0460" type="factum">
<head>A.N., F12 781D [1720env.] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Memoire pour la communauté des maitres et marchands miroitiers lunetiers bimblotiers doreurs sur cuir garnisseurs et enjoliveurs de la ville et fauxbourgs de Paris.</hi></p>
<p>Il ne s'est point rendu d'arrests plus importants pour le maintien du commerce et des arts, pour l'utilité publique et pour l'interest de l'Etat, que ceux qui ordonnent la representation des titres de concession et de confirmation pour la franchise des metiers.</p>
<p>Le feu Roy de glorieuse memoire, persuadé que rien n'etoit plus digne de son attention que de connoître et de reprimer les abus qui s'estoient glissez dans l'usage et l'exercice de ces pretendus affranchissemens des maitrises, et dans l'extention outrée que l'on y a donné, avoit supprimé les corps de metiers, communautez, maitrises et jurandes des fauxbourgs et banlieüe de Paris, établis par les seigneurs <pb n="2"/> particuliers, contre le besoin public et le droit de sa Majesté, à laquelle seule il appartient d'etablir des corps de metiers dans le Royaume, et avoir jugé necessaire de les reunir aux corps et communautez de metiers de la ville.</p>
<p>La reputation des ouvriers de Paris, la quantité des ouvrages qui s'y façonnent et qui se repandent dans les pays estrangers, même les plus reculez, font sentir l'extreme consequence d'empêcher que dans ces ouvrages il ne s'en trouve point de defectueux, soit pour la façon, soit pour la quantité, soit par rapport aux organes de la vüe.</p>
<p>Autrement ce sroit decrediter les ouvrages et les ouvriers de Paris et du Royaume, et priver journellement l'Etat de sommes et de secours considerables.</p>
<p>En effet les franchises facilitent aux ouvriers ignorans et sans qualité l'exercice des arts dont ils n'ont souvent aucune connoissance. Ce leur est un moyen seur de se <pb n="3"/> soustraire, de même que leurs ouvrages les plus defectueux, à l'examen et aux visites des maitres experimentez et habiles, et de debiter leurs marchandises et leurs ouvrages par preference aux meilleurs maîtres, parce que n'etant pas sujets comme les autres aux charges, impositions et frais de visite des communautez, ils sont en état de les donner à plus juste prix, ce qui causeroit infailliblement la ruine des arts et metiers s'il n'y étoit remedié par la suppression de ces franchises et privileges.</p>
<p>Ce sont des motifs si essentiels qui ont porté les vües superieures de sa Majesté à son avenement à la Couronne, à rendre les arrests qui obligent ceux qui ont des concessions ou confirmations de ces sortes de franchises, à rapporter les titres de tant de privileges énormes pour les restraindre dans de justes bornes.</p>
<p>On voit à present dans la ville de Paris des nouveaux quartiers de franchises bastis depuis peu, qui font des villes entieres d'ouvriers, <pb n="4"/> ou du moins en aussi grand nombre qu'il en faudroit pour fournir des villes considerables.</p>
<p>L'abus de ces privileges est d'autant plus sensible qu'il ne s'en trouve peut être aucun qui puisse avoir été accordé avec la faculté aux communautés ecclesiastiques ou regulieres d'etendre les franchises à des ouvriers de toutes sortes d'arts et d'en multiplier le nombre à l'infiny.</p>
<p>La faveur des maisons religieuses, dont on a voulu procurer la retraite et la tranquilité, a fait que l'on a toleré qu'ils eussent dans leurs enclos, pour leur usage, les ouvriers les plus communs et les plus necessaires à la vie, qui n'etoient pas astraints comme les autres à la police et visite des communautés, parce que leurs ouvrages ne se repandoient pas dans le public, et qu'on auroit regardé alors les visites que les maitres auroient fait chez ces ouvriers comme un trouble de la solitude des maisons regulieres.</p>
<p>Mais les franchises, qui n'etoient qu'une <pb n="5"/> tolerance et un abus, se sont soutenues par le credit des communautez regulieres qui, dans la suitte des tems, se sont fait un droit de cette tolerance, qui bien que destitué de fondement et de justice, n'a pas laissé de subsister, quoyque très prejudiciable au commerce et aux arts.</p>
<p>Après ces observations sur l'origine des lieux de franchises et privilegiez, il est evident qu'on n'y doit avoir aucun egard, à moins que les concessions n'en soient fondées sur des titres très precis et très autentiques, duement enregistrés dans les cours avec les procureurs generaux, sans quoy on ne les doit considerer que comme des privileges abusifs, exhorbitans et surpris.</p>
<p>C'est sur ces caracteres qu'il est important d'examiner les pretendus privileges et concessions qui doivent être rapportez pour en fixer la consistance et l'etendue.</p>
<p>Il est même evident que peu de ces communautez regulieres se trouvent en etat de justiffier de <pb n="6"/> pareilles concessions, puisqu'elles ne se mettent pas en etat de satisfaire aux arrests et de rapporter leurs titres, quoyque le delay fatal fixé par quelques uns de ces arrests soit deja expiré.</p>
<p>Les miroitiers ont toujours attendu qu'ils fussent remis au greffe de la commission pour en prendre communication et dire ce qu'ils estiment convenable pour le bien de l'Etat, du commerce, du public et de leur communauté.</p>
<p>Mais comme ces communautez regulieres pourroient differer trop longtems, et que, ne cherchant qu'à prolonger la jouissance, elles eluderont tant qu'il leur sera possible la representation de leurs titres, les miroitiers ont cu de leur devoir de faire leurs observations de tous ces privileges et ces franchises, et sur l'importance de les restraindre dans leurs justes bornes, et les moyens d'y parvenir.</p>
<p>Les lieux de franchises où se retirent et <pb n="7"/> demeurent une infinité d'ouvriers sans qualité sont entr'autres le fauxbourg Saint Antoine, l'abbaye de Saint Germain des Prez, l'enclos de St. Martin des Champs, ceux du Temple, de Saint Jean de Latran, de Saint Denis de la Chartre, et plusieurs colleges.</p>
<p>Quoyque le feu Roy, par son edit du mois de decembre 1678, en supprimant les corps de metiers, communautez, maitrises et jurandes des fauxbourgs de Paris, et en les unissant aux corps de metiers et communautez de la ville, ait ordonné formellement que les statuts des communautez de la ville seront executez dans toute l'etendue de la ville et fauxbourgs de Paris, cependant cette disposition de l'edit a toujours été éludée et meprisée impunement par tous les ouvriers qui ont vecu sans regle et sans discipline entr'eux, se sont soustraits aux visites des jurez et à l'execution des statuts.</p>
<p>Ceux de la communauté des miroitiers portent en termes precis que nul ne peut faire aucun ouvrage dependant dudit metier s'il n'a fait apprentissage chez un maitre pendant <pb n="8"/> cinq années, s'il n'y est ensuite reçu maitre par chef-d'œuvre, et s'il n'a payé les droits ordinaires. Neantmoins ceux qui se meslent de travailler dans les lieux pretendus privilegiez n'ont fait ni apprentissage ni chef-d'œuvre, et ne peuvent par conequent avoir la capacité necessaire pour parvenir à la maitrise et pour faire des ouvrages de la qualité requise par les statuts.</p>
<p>Ces faux ouvriers, qui ne craignent ni visites, ni jurez, ni condamnation d'amende, ni confiscation d'ouvrages et de marchandises de mauvaise qualité, n'ont pour principe que d'en employer et d'en faire de defectueuses et à vil prix, affin de se procurer du debit.</p>
<p>Ils employent par exemple du verre de vitre pour faire des lunettes, ou autres ouvrages dependants de l'optique, qui indubitablement affoiblissent, gastent et attenuent le rayon visuel, et alterent le nerf optique. De là vient que l'on voit un si grand nombre de personnes qui <pb n="9"/> perdent la vue ou qui sont hors d'etat de gagner leur vie et reduits à mendier leur pain.</p>
<p>Ces inconveniens et abus ne sont point à craindre de la part des maitres, parce que leurs ouvrages sont sujets aux visites de leurs jurez et gardes, qui saisissent les marchandises defectueuses, les font confisquer et condamner les maitres à l'amende.</p>
<p>Il est vray que ces jurez et gardes sont autorisez à faire leurs visites chez les ouvriers des pretendus lieux de franchises et privilegiez, qu'ils se peuvent faire assister par des commissaires et huissiers, et que les archers du guet sont même tenus de leur prester main forte et assistance. Mais toutes ces precautions et injonctions sont inutiles, car ces faux ouvriers, qui ont un mutuel interest de se soutenir, et qui sont protegez par les communautez ou les seigneurs des lieux pretendus privilegiez, s'attroupent de toutes vaccations, quelques fois avec port d'armes, <pb n="10"/> injurient et menacent les jurez et gardes, et les officiers qui les accompagnent, lesquels sont obligez de se retirer pour eviter les emotions populaires, en sorte que les abus se perpetuent et qu'ils augmentent tous les jours, surtout depuis que l'on a elevé dans ces lieux pretendus privilegiez de vastes bastiments qui renferment des ouvriers pour fournir des villes entieres.</p>
<p>Ce nombre prodigieux d'ouvriers qui, soit par la mauvaises qualité des ouvrages, soit par l'exemption des charges et impositions auxquelles les autres maitres sont sujets, peuvent toujours, comme on l'a observé, donner leurs marchandises à moindre prix et se procurer du débit, reduisent plusieurs maitres au triste etat d'etre sans occupation et sans ouvrage, et leurs familles à la mendicité.</p>
<p>Souvent même les ouvriers sans qualité debauchent les apprentifs et les compagnons des maitres, qui ne peuvent les obliger de revenir, <pb n="11"/> n'ayant point de jurisdiction sur ces ouvriers etrangers, ce qui prive les maitres du secours de leurs apprentifs et de leurs garçons, et qui met les apprentifs hors d'etat de bien apprendre leur metier et à faire des ouvrages de bonne qualité et conformes aux statuts et reglemens.</p>
<p>Rien donc n'est plus important que l'interest du commerce et des arts et celuy de l'Etat que de faire cesser tous ces abus, et d'ordonner qu'aucun ouvrier de telle vaccation que ce puisse être ne pourra travailler qu'il n'ait fait l'apprentissage et le chef-d'œuvre, qu'il ne soit reçu maitre et n'ait payé les droits reglez à cet effet, et qu'il ne soit soumis aux visites des jurez et gardes de la communauté, et à toutes les autres dispositions des statuts qui leur sont devenües communes depuis l'edit de 1678, et dont l'execution est si necessaire pour faire fleurir le metier de miroitier.</p>
<p>C'est le seul moyen de soutenir les communautez et la reputation des ouvrages <pb n="12"/> de Paris, et de faire entrer dans le Royaume des sommes considerables par le commerce qui s'en fait avec les estrangers.</p>
<p>Sa Majesté recevra même un avantage particulier en relevant les communautez abbatues et ruinées par l'abus des franchises et des lieux privilegiez, puisqu'elle trouvera dans ces corps, lorsqu'ils seront retablis, des ressources infinies dans les tems de guerre et dans les besoins de l'Etat.</p>
<p>Vanetel, Menard.</p>
</div>
</body>
</text>
</TEI>