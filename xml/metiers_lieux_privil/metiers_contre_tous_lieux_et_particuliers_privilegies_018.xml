<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">018. "Mémoire pour les manufactures royales des tapisseries d'Aubusson et Feletin." (1725)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>A.N., F12 781D, 1725, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>A.N.</repository><idno>F12 781D</idno></msIdentifier><head><title>n° 0459 A.N., F12 781D [1725] [imprimé]</title><origDate when="1720">1720</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>manufacture privilégiée — vente des manufactures provinciales dans Paris</term>
</keywords>
<keywords ana="parties">
<term>manufactures de tapisserie d'Aubusson et Felletin contre tapissiers</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0459" type="factum">
<head>A.N., F12 781D [1725] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Mémoire pour les manufactures royales des tapisseries d'Aubusson et Feletin.</hi></p>
<p>Les Fabriques de ces Villes situées dans la Province de la Marche sont les plus anciennes du Royaume. Elles ont subsisté un très longtems auparavant celle qui fut établie à Paris, en 1607, en faveur des sieurs Comans &amp; Delaplanche, fabriquans originaires du Pays de Flandres.</p>
<p>Les Patentes de 1607 font mention et déclarent que cet établissement ne préjudiciera en aucune chose à celui des Tapisseries de Hautelisse de Feletin, Beauvais et autres faites dans le Royaume.</p>
<p>En 1665 et 1688, sa Majesté autorisa par ses Patentes les Statuts qui furent redigés pour les Manufactures d'Aubusson &amp; de Felerin.</p>
<p>En 1717, les Fabriquans d'Aubusson dresserent de nouveaux Statuts pour la plus grande perfection de leurs Ouvrages : ils en demandent l'homologation au Conseil.</p>
<p>Les Tapisseries de Paris ont conçu de la jalousie du grand débit qu'ils voyent faire journellement de ces Manufactures ; ils veulent détruire ce commerce en assujetissans les Marchandises qui s'y fabriquent à leur visite &amp; à leur marque, en contraignant les Entrepreneurs &amp; Fabriquans de n'employer que du fin &amp; grand teint, &amp; des Laines fines.</p>
<p>Ils ont pour cet effet redigé de leur côté des Statuts qu'ils ont fait homologuer par des Patentes registrées au Parlement <pb n="2"/> de Paris en 1719 ; mais ces mêmes Patentes ayant, suivant le stile ordinaire, conservé le droit d'autrui, &amp; d'ailleurs les Marchands Tapissiers d'Aubusson &amp; Feletin ayans formé opposition à l'Arrest d'enregistrement par raport à quelques articles qui leur feroient préjudice s'ils subsistoient, les Tapissiers de Paris se sont pourvûs au Conseil où ils tentent de faire réüssir leur mauvais dessein.</p>
<p>Ils y ont d'abord presenté de nouveaux Articles au nombre de 18 ; maintenant ils se réduisent à sept, qui sont à la suite de leur dernier Mémoire, dont ils demandent l'homologation.</p>
<p>Dans ces sept Articles, il y a trois choses essentiellement contraires à l'interest &amp; au droit des Manufactures de la Marche, c'est la visite &amp; la marque, le fin &amp; grand teint, &amp; les Laines fines, à quoi ils prétendent assujettir les Marchandises qui sortent de ces Manufactures.</p>
<p>Il ne leur appartient pas de surveiller à la fabrique de ces Manufactures. Qu'ils se contentent de veiller sur leurs propres ouvrages, ou plûtôt qu'ils commencent par aprendre leur métier ! Ils usurpent la qualité de Tapissiers Hautelissiers ; ils sont tout au plus rentrayeurs de vieilles Tapisseries &amp; Tapis ; on ne voit sortir de leurs mains aucuns Ouvrages neuf qui mérite l'aprobation du Public ; ce sont gens nouveaux qui, pour s'autoriser en quelque maniere &amp; se donner l'être, ont été obligés de s'allier aux Communautés des Courtepointiers &amp; des Couverturiers. Il a été necessaire en 1607 de faire venir des Pays-Bas, à leur confusion, deux étrangers nommez Comans &amp; Delaplanche pour former leur établissement &amp; leur aprendre leur métier ; ils n'ont pas sçû en profiter ; ils courent les Inventaires où ils cabalent &amp; font des associations illicites contre l'interest public &amp; la liberté des inventires ; ils y achettent toutes sortes de vieilles Tapissereis, quelques deffectueuses qu'elles soient, qu'ils rentrayent pour les revendre, &amp; font le courtage des neuves de toutes les fabriques ; c'est en quoi consiste tout leur Art &amp; travail.</p>
<p>Les Manufactures de la Marche, au contraire, sont composées d'Entrepreneurs &amp; de Fabriquans habiles, qui ont herité ce talent de leurs peres depuis plusieurs siècles, &amp; se sont toûjours maintenus dans la science de leur Art, qui se perfectionne de jour en jour. Celle d'Aubusson a même eu l'honneur de presenter à Sa Majesté une grande pièce de Tapisserie relevée en or, representante l'Element de la Terre, sur le dessin <pb n="3"/> du sieur Lebrun ; Sa Majesté en fut très satisfaite, Elle l'accepta, récompensa l'Ouvrier &amp; la fit tendre dans l'un des principaux Apartemens du Château de Versailles où elle est actuellement.</p>
<p>C'est la raison pour laquelle Sa Majesté, en connoissance de cause, par ses Patentes des années 1665 &amp; 1688, a autorisé les Statuts de ces Manufactures. Elle a même promis de fournir à ses frais un Peintre &amp; un Teinturier pour d'autant plus donner de relief à ces fabriques ; mais les dépenses de l'Etat ont privé ses Manufactures de ce secours qui seront néanmoins très utile.</p>
<p>Quoiqu'il en soit, ces Parentes portent que leurs Tapisseries, après qu'elles auront été fabriquées, visitées &amp; marquées sur le Lieu, seront exemptes de toute autre marque &amp; Visite, par toutes les Villes du Royaume où elles pourront être transportées, débitées &amp; venduës.</p>
<p>Faculté dans laquelle ils ont été maintenus toutesfois &amp; quantes que les Tapissiers de Paris ou autres les ont troublé, à laquelle il n'a jamais été donné d'atteinte. L'on rapporte une infinité de Sentences renduës tant par Messieurs les Lieutenans de Police qu'autres Juges, confirmées par Arrests du Parlement, qui ont condamné les entreprises que l'on a voulu faire sur ce Privilege.</p>
<p>Les Tapissiers de Paris reconnoissent que le droit de ceux de la Marche est bien établi, &amp; que sans une dérogation expresses aux Patentes de 1665 &amp; de 1688 sur lesquelles il est fondé, ils ne peuvent réüssir ;</p>
<p>c'est la raison pour laquelle ils demandent qu'il plaise à Sa Majesté d'y déroger specialement. Mais les même motifs qui l'ont fait accorder subsistent encore ; il n'y a que l'envie &amp; jalousie de métier qui invite de demander cette dérogation.</p>
<p>Pour y parvenir, les Tapissiers de Paris objectent que les Tapisseries de la Marche ne sont pas teintes de fin &amp; grand teint, ni fabriquées de Laines fines, ainsi que le Règlement de 1669 le requiert, art. 32 &amp; 39. C'est un défaut essentiel, disent-ils, qui rend ces Tapisseries sujetes à la vermine.</p>
<p><hi rend="italic"><hi rend="bold">Reponse.</hi></hi></p>
<p>La disposition du Reglement de 1669, qui exige le fin &amp; grand teint, &amp; la Laine fine, n'a d'application qu'aux ouvrages qui sont tres fins, riches &amp; du premier ordre, &amp; nullement à ceux qui sont communs &amp; grossiers, tels que la meilleure <pb n="4"/> partie de ceux d'Aubusson &amp; de Feletin.</p>
<p>Cela n'empêche pas que les couleurs vertes &amp; bleües qu'on employe ne soient de bon teint ; mais pour les autres couleurs mentionnées au Mémoire des Jurez, elles ne peuvent être que d'un teint ordinaire, quoique bon à proportion de la qualité de l'ouvrage.</p>
<p>Ces couleurs s'achettent chez les Marchands de laine à Paris, les Bourgeois, les Communautez Religieuses en achetent de semblables pour faire leurs ouvrages ordinaires, il n'y a que l'Ecarlatte qui est en fin teint ; ce fait est si constant que de cent livres de laine qui se vendent chez les Marchands, il n'y en a pas dix de grand teint.</p>
<p>Le Reglement de 1669 ne s'execute pas sur les Tapisseries de Bergame, Points d'Hongrie, Mocades, Serges &amp; autres Ouvrages de toutes couleurs qui se vendent chez les Marchands de la rue saint Denis &amp; ailleurs, que les Tapissiers Courtepointiers &amp; fripiers employent dans les Ouvrages qu'ils font pour le public, ni sur les petites Etoffes de laine &amp; soye qui se fabriquent à Lyon, à Tours &amp; autres lieux du Royaume.</p>
<p>L'on n'y employe pas de fin teint, preuve certaine que la disposition du Reglement de 1669 n'a d'application qu'aux Ouvrages très fins, riches &amp; du premier ordre.</p>
<p>Depuis ce Reglement, il s'est écoulé plus de cinquante années sans qu'on ait inquieté les Fabriquans d'Aubusson &amp; de Felerin, lesquels néanmoins n'ont point employé dans la meilleure partie de leurs Ouvrages de fin &amp; grand teint, tel que les Tapissiers de Paris veulent l'introduire.</p>
<p>Au surplus, les Marchands &amp; fabriquans Tapissiers d'Aubusson &amp; de Feletin ne prennent aucun interest, ni ne prétendent pas s'opposer aux Statuts &amp; Reglemens que les Tapissiers de Paris voudront faire pour les Ouvrages qui se fabriquent à Paris, pourvû qu'ils n'y inserent rien qui préjudicie aux Privileges des fabriques de la Marche, qui ne sont en aucune maniere subordonnées à celle de Paris : elles sont régies &amp; gouvernées les unes &amp; les autres par leurs Statuts particuliers, elles sont en possession de leur maniere de fabriquer depuis un très long tems leurs Ouvrages communs &amp; grossiers.</p>
<p>Quant à la laine fine que les Tapissiers de Paris veulent qu'on employe, cette prétention résiste à la nature des Ouvrages de la fabrique de la Marche, qui sont la meilleure partie communs &amp; grossiers ; ce seroit vouloir obliger ces fabriquans à ne faire que des Ouvrages fins &amp; du premier ordre.</p>
<p><pb n="5"/> Par ce moyen, le commun du Royaume, les Eglises de Province seroient privez de l'usage des Tapisseries ; ils ne pourroient pas acheter ni mettre le prix à ces fins Ouvrages qui couteroient trois fois que ceux que l'on fabrique ordinairement dans les Manufactures de la Marche ; tel peut bien acheter une tenture de Tapisserie de la valeur de quatre à cinq cens livres, qui n'a pas le moyen d'en achepter une de quinze cent livres.</p>
<p>Ce seroit en même tems ruiner plus de dix mille familles qui subsistent dans le Pays de la Marche par le moyen de ce commerce, auquel elles s'adonnent. Le Pays est sterile par lui-même ; il n'y a que l'industrie qui puisse le mettre en estat de soûtenir les familles, suporter les charges &amp; payer les impositions, étant à observer qu'il se débite tous les ans plus de trois mille Tentures de Tapisseries de la fabrique de la Marche. Les Bourgeois &amp; les autres Citoyens moins opulens, même les étrangers, n'ayans pas le moyen d'acheter des Tapisseries de Flandres, ils se contentent de celles d'Aubusson &amp; de Feletin qui, quoique d'un moindre prix, ne laissent pas que d'estre bonnes dans leurs proportions, &amp; agréables pour l'ornement des maisons &amp; même des Eglises moins considerables.</p>
<p>Dans la confection des Tapisseries de Flandres, qui sont d'un prix très superieur, les laines qu'on y employe pour les nuances des bordures ne sont pas toutes de fin teint, &amp; dans celles d'Ourdenarde, qui sont les plus grossieres, on est en usage d'y ajouter plusieurs traits de peinture, ce qui ne se pratique pas dans celles de la Marche.</p>
<p>Dans ces circonstances, les Marchands &amp; fabriquans de la Marche demandent qu'il plaise à Sa Majesté ordonner le Rapport des Patentes de 1719, obtenuës par les Tapissiers de Paris, en ce qu'elles sont contraires aux Statuts d'Aubusson &amp; de Felerin, homologuez par Patentes de 1665 &amp; 1688, ordonner que sans avoir égard auxdites Patentes de 1719, ni aux nouveaux articles proposés par les Tapissiers de Paris, les Patentes de 1665 &amp; 1688 seront executées, homologuer les nouveaux Statuts dressez le 5 Février 1717 pour la Manufacture d'Aubusson, sauf aux Tapissiers de Paris à faire observer entr'eux les Statuts par eux dressés, sans aucune extension sur les Fabriques de la Marche, vente &amp; débit d'icelles en la Ville de Paris en la maniere accoûtumée.</p>
<p><pb n="6"/> <hi rend="bold">Titres pour les Manufactures de la Marche.</hi></p>
<p><hi rend="italic">1607.</hi> Patentes d'Etablissement de la Manufacture de Paris, en faveur des sieurs Comans &amp; Delaplanche, Flamans. Sa Majesté fit venir de Flandres les sieurs Comans &amp; Delaplanche pour aprendre le métier aux Parisiens, qui n'en ont pas profité ; on ne voit sortir de leurs mains aucun Ouvrage neuf ; ils courent les Inventaires où ils cabalent, achetent de vieilles Tapisseries pour <hi rend="italic">les rentrayer</hi>. Ils ont été obligés de s'unir avec les Courtepointiers &amp; Couverturiers pour former un Corps &amp; de donner l'être.</p>
<p><hi rend="italic">Vide la Sentence du 7 Mars 1682 cy-après.</hi> Ces mêmes Patentes prouvent que longtems auparavant il y avoit des Manufactures dans la Marche, par ces mots : <hi rend="italic">déclarans néanmoins que nous n'entendons préjudicier en aucune chose à l'établissement des Tapisseries de Hautelisse de Feletin, Beauvais ou autres établies dans le Royaume, lesquelles nous voulons avoir &amp; continuer leurs cours ainsi que par cy-devant.</hi></p>
<p><hi rend="italic">1625.</hi> Nouveaux articles afin d'autant mieux exciter lesdits de Comans &amp; Delaplanche à instruire les Parisiens, preuve qu'ils ne se perfectionnoient pas en cet Art.</p>
<p><hi rend="italic">7 Decembre 1624.</hi> Sentence du Châtelet, fait mainlevée à un Fabriquant d'Aubusson de Marchandises saisies à la Requête des Jurez Tapissiers de Paris.</p>
<p><hi rend="italic">Nota,</hi> que pour lors la Visite &amp; la marque étoient permises, mais sans pouvoir prétendre aucun salaire ni amende. Et par les Patentes de 1665 &amp; 1688, la Visite &amp; la marque sont absolument défenduës.</p>
<p><hi rend="italic">28 Avril 1640.</hi> Sentence deboute les Tapissiers de Paris qui vouloient empêcher ceux d'Aubusson &amp; de Feletin de tenir Magazin à Paris hors le tems des Foires ; mainlevée etc., avec dépens.</p>
<p><hi rend="italic">17 May 1646.</hi> Arrest fait mainlevée d'un soubassement servant d'étalage &amp; de deux pièces de Tapisserie d'Aubusson.</p>
<p><hi rend="italic">1665, 1688.</hi> Patentes pour Aubusson &amp; Feletin, affranchis de toute Visite ; permission de vendre &amp; débiter à Paris &amp; ailleurs.</p>
<p><hi rend="italic">2 Avril 1676.</hi> Sentence, mainlevée de quatre pièces de Tapisseries d'Aubusson que les Tapissiers de Paris vouloient assujettir à la marque, avec dépens.</p>
<p><hi rend="italic">1 Mars 1678.</hi> Sentence, déboute les Tapissiers de Paris qui vouloient restraindre à quinzaine la faculté de vendre les Tapisseries de la Marche, sans pouvoir demeurer toute l'année, ni les tenir en Magasin.</p>
<p><hi rend="italic">21 Janvier, 4 Juin 1678.</hi> Avis du Procureur du Roy, &amp; Sentence, mainlevée des Tapisseries d'Aubusson qui étoient marquées d'un plomb aux Armes du Roy &amp; de la Ville.</p>
<p><pb n="7"/> <hi rend="italic">16 Juin 1682.</hi> Sentence, déboute les Tapissiers de Paris qui vouloient visiter les Tapisseries d'Aubusson qui étoient plombées ; ordonne l'exécution des précedentes Sentences, &amp; que les Tapissiers de Paris &amp; d'Aubusson se porteront respect, respectivement ; condamne les Tapissiers de Paris avec dépens.</p>
<p><hi rend="italic">4 Septembre 1682.</hi> Sentence, mainlevée des soubassemens mis aux portes des Magasins des Tapisseries de la Marche, qui avoient été saisis par les Tapissiers de Paris.</p>
<p><hi rend="italic">7 Mars 1692.</hi> Sentence, condamne Jean Bussiere, Tapissier rentrayeur à Paris, d'ôter les Tableaux, Enseignes &amp; Inscriptions &amp; autres marques de Magasin Royal des Tapisseries d'Aubusson qu'il avoit fait mettre au devant de sa Maison, ruë de la Huchette ; défenses etc., amende de dix livres &amp; aux dépens.</p>
<p><hi rend="italic">24 Octobre 1711.</hi> Sentence renduë par Mr de Baudry, Lieutenant de Police, fait mainlevée de Tapisseries saisies au Bureau de Rencontre, attendu que les Marchandises étoient marquées du plomb d'Aubusson &amp; Feletin.</p>
<p><hi rend="italic">Nota.</hi> Monsieur de Baudry étoit pour lors Conseiller au Conseil de Commerce, &amp; il étoit saisi de l'affaire du Conseil.</p>
<p><hi rend="italic">5 Fevrier 1717.</hi> Nouveaux Statuts pour Aubusson, dont on demande l'homologation pour obvier à certains abus sur les Lieux, &amp; la plus grande perfection des Ouvrages.</p>
<p><hi rend="italic">1719.</hi> Nouveaux Statuts pour Paris. Art. 14, assujettit les Tapisseries d'Aubusson à la Visite, enjoint aux Tapissiers d'Aubusson de porter honneur &amp; respect à ceux de Paris.</p>
<p>Art. 37, assujettit les Marchandises d'Aubusson à être portées au Bureau de Paris pour être visitées &amp; marquées.</p>
<p>Art. 38, assujettit chaque pièce au droit de cinq sols de Marques.</p>
<p><hi rend="italic">Contraires aux Statuts d'Aubusson &amp; Feletin, qui exemptent de toute Veue, Visite &amp; Marque.</hi></p>
<p>Opposition par les Marchands Tapissiers de la Marche.</p>
<p>Ceux de Paris, voulans pallier leur mauvais dessein, ont 1° dressé dix-sept Articles, qui paroissent plus doux, dont ils ont demandé l'homologation au Conseil, 2° un Mémoire, ensuite duquel ils se sont restraits à sept Articles, qui paroissent encore plus mitigés ; mais tout cela aboutit toujours à assujetir les Tapisseries de la Marche à la Visite &amp; Marque de Paris, &amp; à l'usage du grand &amp; fin teint, &amp; des Laines finces, ce qui est impratiquable par les raisons déduites dans le Mémoire des Marchands &amp; Fabriquans Tapissiers de la Marche, en faveur desquels Mr l'Intendant de Moulins s'est déterminé, par son avis envoyé en Cour.</p>
<p>De l'Imprimerie de Jacques-François Grou, ruë de la Huchette, au Soleil d'Or.</p>
</div>
</body>
</text>
</TEI>