<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">011. "Repliques des quatre premiers corps des Marchands de Paris, au memoire servant de réponses aux Objections faites contre le Memoire concernant le Consulat." (1725)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>Arch. Seine, D1B6-36, 1725, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Ms Joly de Fleury 60 (Fol.128)</idno></msIdentifier><head><title>n° 0189 Arch. Seine, D1B6-36 [1725] [imprimé]</title><origDate when="1724">1724</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>accès au Consulat — codification coutumière — augmentation du nombre des consuls</term>
</keywords>
<keywords ana="parties">
<term>drapiers, épiciers, merciers, pelletiers contre bonnetiers, orfèvres</term>
</keywords>
<keywords ana="juridiction">
<term>conseil de commerce</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0189" type="factum">
<head>Arch. Seine, D1B6-36 [1725] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Repliques des quatre premiers corps des Marchands de Paris, <hi rend="italic">au memoire servant de réponses aux Objections faites contre le Memoire concernant le Consulat.</hi></hi></p>
<p>La liberté de donner des Memoires dans des affaires interessantes ne doit jamais être blâmée, mais il ne doit jamais y avoir de surprise. La bonne foy &amp; la vérité sont des caracteres inséparables du Marchand ; ces caracteres ne se découvrent point dans les démarches des Marchands Bonnetiers &amp; Orfevres par rapport à leurs idées sur le Consulat, puisqu'ils ont presenté une Réponse aux Objections faites à leur Memoire, &amp; qu'ils y comprennent les Marchands Pelletiers, sous le nom desquels ils agissent.</p>
<p>C'est vouloir surprendre la religion des Magistrats que de se servir, comme font les Marchands Bonnetiers &amp; Orfévres, du nom des Pelletiers, qui ont abandonné ce projet depuis trois mois, en faisant signifier un acte aux Marchands Bonnetiers &amp; Orfévres, portant que leur interêt &amp; celui du Public étoit de demander la continuation du Consulat conjointement avec les trois premiers Corps.</p>
<p>Il est triste que des vûës particulieres &amp; des interêts personnels déterminent des démarches que le bien public ne demande point, &amp; que ces mêmes interêts troublent l'union des Six Corps en les divisant, &amp; répandant la confusion par les nouveautez que les deux derniers Corps veulent introduire au préjudice du bon ordre où l'on a toujours vû le Consulat depuis son établissement.</p>
<p>Les presentes Repliques se termineront à differentes Observations.</p>
<p><hi rend="italic">Primò.</hi> Quelque soit ou quelque puisse être l'étenduë de la Ville de Paris, soit par la nouveauté de ses édifices, soit par l'augmentation de ses Habitans, cela ne mettra jamais de proportion avec les temps où les Billets de l'Etat, ceux des monnoyes &amp; ceux de la Banque ont eu cours. Jamais le Consulat se trouva plus surchargé d'affaires ; cependant le nombre des Consuls a suffi, &amp; le public ne s'en est jamais plaint. Où est donc la nécessité d'apporter des changemens dans cette Jurisdiction, &amp; de vouloir en augmenter les Chefs dans un temps où l'application du Gouvernement ne tend qu'à faire rentrer toutes choses dans leur ordre naturel ?</p>
<p><hi rend="italic">Secundò.</hi> L'élection des Consuls s'est toûjours faite de personnes consommées dans le commerce, &amp; d'un âge qui donne seul l'experience <pb n="2"/> necessaire à la Justice distributive de cette Jurisdiction ; &amp; quelqu'ait été l'attention des Six Corps à ne point admettre dans le Consulat des hommes infirmes ou courbez sous le faix des années, comme l'on n'y peut passer qu'à un certain âge, il n'est pas étonnant d'avoir vû ou de voir mourir des Consuls au milieu de leur carriere. La destinée des hommes est de mourir à tout âge, la mort n'épargne qui que ce soit, &amp; son heure estant incertaine, puisqu'elle attaque les jeunes comme les vieux ; c'est une fort mauvaise raison que d'attribuer la mort d'un Consul en place au poids du grand travail qui l'occupe &amp; peut l'accabler. Les changemens que l'on propose dans le Consulat ne préviendront jamais la mort ; &amp; malgré la vûë des deux derniers Corps, qui prétendent que dans une élection libre on n'y admettroit que la capacité &amp; le merite, ils seroient assez embarrassez à trouver cette capacité &amp; ce vrai merite dans les Corps qu'ils voudroient faire entrer au Consulat.</p>
<p><hi rend="italic">Tertiò.</hi> Le public ne souhaite ni ne demande l'augmentation du nombre des Consuls ; mais les deux derniers Corps, Auteurs des differens Memoires concernans cette augmentation, &amp; jaloux de voir le Siege Consulaire rempli de Sujets des trois premiers Corps, &amp; de se trouver trop éloignez de cet honneur, proposent cette augmentation ; c'est donc moins l'interest public qui gouverne ces démarches que des interests personnels.</p>
<p>Les deux derniers Corps ne doivent jamais se figurer que les quatres autres les croyent indignes de l'honneur du Consulat ; à Dieu ne plaise que l'on puisse concevoir l'idée d'une telle pensée, puisque les deux derniers Corps composent le Consulat comme les premiers. Il est vrai qu'ils ne peuvent pas esperer d'y être admis tous les deux ; le nombre n'estant que de quatre Consuls, la quatrième place ne peut tomber sur les derniers Corps qu'alternativement d'année en année. Le Corps de la Pelleterie ne demande à y être admis qu'à son tour ; c'est un ordre où l'on a joûjours vêcu ; &amp; l'on ne prévoit pas que pour épouser la passion ou l'entêtement de quelques Particuliers remuans, Sa Majesté se porte à changer cet ordre.</p>
<p><hi rend="italic">Quartò.</hi> La maladie d'un Consul ou sa recusation ne peut affoiblir le Siege, qui se trouve remplacé par un ancien Consul ; c'est une mauvaise raison que celle d'attaquer ou de vouloir innover dans une Jurisdiction par de tels motifs. Nos Anciens étoient-ils moins sages que nous ? Et pourroit-on ne pas suivre leurs traces ?</p>
<p><hi rend="italic">Quintò.</hi> Quand le Consulat seroit rempli de Six Consuls, il est, moralement parlant, impossible de se dispenser de passer sur la voye d'Arbitres, parce qu'il est impossible d'apporter à l'Audience les marchandises sujettes à contestation pour juger de leur qualité &amp; défectuosité. Le Marchand Orfevre même, tout habile qu'il puisse être, ne peut décider du titre, d'aloy, d'alliage, des grains, de Karats &amp;</p>
<p>trente-deuxième de fin sur le champ à l'Audience, quand on lui representeroit même les matieres, attendu que cela demande un examen qu'il est impossible de faire sur le champ ; au lieu que le Drapier, qui sçait le lainage, est en état de décider de la validité des Lettres de Change, des Billets au porteur, ou à ordre &amp; valeur en marchandises, ce que <pb n="3"/> d'autres ne peuvent faire sur le champ par le défaut d'usage.</p>
<p><hi rend="italic">Sextò.</hi> La Réponse à la sixième objection est des plus affectées ; elle porte avec elle son inutilité, puisque l'on ne s'apperçoit jamais d'aucun vuide à l'avenement du Siege. Tout le public sçait que les Sentences renduës au Consulat sont presque toutes confirmées au Parlement.</p>
<p><hi rend="italic">Septimò.</hi> L'on a dit avec raison qu'il n'y avoit eu aucun changement dans le Consulat de Roüen, &amp; que ce qui s'y est toûjours pratiqué, s'y pratique encore. Au reste, ce qui pourroit être avantageux pour la Ville de Roüen ne conviendroit nullement à la Ville de Paris. Il est des occasions où l'exemple peut décider ; mais l'application ne s'en doit pas faire ici.</p>
<p><hi rend="italic">Octavò.</hi> L'exemple de ceux qui passent deux ans Marguilliers, Gardes ou Echevins, ne peut servir de decision dans la proposition des deux derniers Corps ; tout le monde peut remplir ces places les deux années qu'elles durent, le travail n'y étant pas continuel, ni si accablant que celui du Consulat, qui demande trois jours chaque semaine avec une assiduité &amp; une application sans interruption, les autres jours s'employant à écouter les differends des Parties. Les nouveautez ne servient à rien, &amp; il convient beaucoup mieux de laisser les choses comme elles sont. Si les sentimens du plus grand nombre doivent estre écoutez, comment les deux derniers Corps voudroient-ils faire seuls la loy aux quatre autres, &amp; demander des changemens qui ne sont pas convenables, &amp; que les autres Corps ne souhaitent pas.</p>
<p><hi rend="bold">Conclusions.</hi></p>
<p>L'on a toûjours respecté a l'on respecera toûjours le Regne de Charles IX auquel nous sommes redevables des sages dispositions avec lesquelles il a érigé la Jurisdiction Consulaire. La memoire des Anciens doit toûjours estre respectable ; &amp; les éminens personnages qui vivoient du tems de ce Roy, &amp; qui composoient son Conseil, en prescrivant le nombre des Consuls, n'avoient pour objet que l'acceleration de la Justice dont les operations doivent estre simples, &amp; qu'un trop grand nombre de Juges, surtout en matiere de commerce, embarrasseroit.</p>
<p>Les avantages que cause cet établissement au Public, en l'estat où il a plû à Charles IX de l'établir, ne demandent pas que des vûës interessées &amp; particulieres de quelques personnes y apportent aucun changement ; il est de la justice &amp; de la grandeur du Prince de conserver ce que ses Prédecesseurs ont si sagement établi.</p>
<p>Ainsi les quatre premiers Corps de Paris représentent très humblement à Nosseigneurs du Conseil de Sa Majesté,</p>
<p>1° Que le Siege Consulaire est suffisamment rempli de quatre Consuls avec le Juge sans qu'il soit nécessaire ni avantageux d'en augmenter le nombre.</p>
<p>2° Qu'on ne peut tirer que du nombre des Six Corps des anciens Marchands pour en remplir les place &amp; composer le Consulat, cet ordre étant aussi ancien que l'établissement de cette Justice.</p>
<p>3° Que comme le Juge ne vient qu'à son tour à cette place, &amp; <pb n="4"/> longtemps après avoir passé son année de Consulat, il y aura un Consul de son même Corps ; c'est l'usage qui s'est toûjours pratiqué, il n'y a point de raison qui puisse le changer.</p>
<p>4° Les six Corps ne se trouvent point divisez pour le choix puisque chaque Corps le fournit à son tour &amp; à son rang, ce qui doit toûjours continuer de même.</p>
<p>5° Il y auroit des inconveniens à changer ou à multiplier les élections ; il n'est besoin que de l'élection ordinaire qui se fait tous les ans au mois de Fevrier, conformément au titre de la Declaration de la Jurisdiction, au lieu que deux élections par an jetteroient le Consulat dans la confusion &amp; dans une dépense surnumeraire qui l'incommoderoit beaucoup ; l'on sçait que les pertes qu'il a souffert n'ont pas rendu sa situation aisée.</p>
<p>Enfin les changemens proposez par les deux derniers Corps n'ayant pour objet que celui de satisfaire la passion qu'ils ont de se rendre indépendans &amp; de faire la loy aux autres, on ne doit pas les écouter ; les nouveautez sont préjudiciables. Il est notoire que le Public ne trouveroit jamais d'avantage dans les changemens que les deux derniers Corps demandent &amp; qu'ils prétendent être du bien public, pendant que leurs interêts personnels gouvernent seuls toutes leurs démarches.</p>
<p>Lorsqu'il s'agit de l'administration de la Justice &amp; que, comme dans la Jurisdiction Consulaire, tout s'y passe dans un bon ordre &amp; à la satisfaction du Public, il ne convient jamais d'apporter aucun changement, surtout quand tout s'y trouve si sagement établi, &amp; que le Public est content du progrès de la Jurisdiction Consulaire en l'état où elle est &amp; où elle doit toûjours rester. L'on ne voit dans les deux derniers Corps qu'une ambition démesurée, qui les fait troubler une union qu'ils devroient être jaloux de conserver. Les quatre premiers Corps ne demandent qu'à vivre fraternellement avec eux &amp; à ne point sortir d'un ordre aussi sagement établi que celui où se trouve le Consulat</p>
<p>De l'Imprimerie d'André Knapen, au bout du Pont S. Michel.</p>
</div>
</body>
</text>
</TEI>