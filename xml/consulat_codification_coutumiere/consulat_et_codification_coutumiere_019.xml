<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">019. "Memoire a Monseigneur le Procureur General, en execution de l'arrest de la Cour du 5 fevrier 1727." (1727)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Ms Joly de Fleury 60 (Fol.197), 1727, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Ms Joly de Fleury 60 (Fol.204)</idno></msIdentifier><head><title>n° 0415 BNF, Ms Joly de Fleury 60 (Fol.197) [1727] [manuscrit]</title><origDate when="1727">1727</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>accès au Consulat — codification coutumière</term>
</keywords>
<keywords ana="parties">
<term>bonnetiers, orfèvres contre Six Corps</term>
</keywords>
<keywords ana="juridiction">
<term>Parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0415" type="factum">
<head>BNF, Ms Joly de Fleury 60 (Fol.197) [1727] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Memoire a Monseigneur le Procureur General, en execution de l'arrest de la Cour du 5 fevrier 1727.</hi></p>
<p>Par lequel il est dit que les six corps des marchands remettront incessamment à Monseigneur le Procureur General chacun leur memoire sur la maniere dont il convient proceder aux elections des juge et consuls.</p>
<p>Aucuns corps ni communauté de marchands et negocians à Paris n'est aussy interessé sur le fait des elections des juge et consuls que le corps des marchands bonnetiers et celuy des orfevres joüailliers, soit par raport à l'etendüe de leur commerce, soit par raport à l'utilité que le public en retire. Les marchands drapiers, epiciers et merciers ayant insensiblement et par succession de tems usurpé une possession dans le Consulat, à l'exclusion des derniers corps, quoique les marchands bonnetiers et orfevres soient en tres grand nombre, et que l'étendüe de leur commerce soit aussy considerable que le leur.</p>
<p>Dans le fait, par l'edit du Roy Charles 9, portant creation de la jurisdiction des consuls à Paris du mois de 9bre 1563, enregistré au Parlement le 18 janvier suivant de la même année, il est permis et enjoint aux prevost des marchands et echevins de Paris en l'assemblée des cent notables bourgeois de nommer et elire cinq marchands du nombre desdits cent, ou autres absents, natifs et originaires du royaume de France, marchands demeurans à Paris, le premier desquels sa Majesté nomme juge des marchands, la charge desquels cinq ne durera qu'un an.</p>
<p><pb n="2"/> Cet edit ne fait aucune distinction entre les six corps des marchands de Paris, et ne porte point que les sujets d'un des six corps seront elus juges et consuls preferablement ni plus frequemment que les autres.</p>
<p>Cependant les trois corps de la draperie, de l'epicerie et de la mercerie, qui se disent les premiers quoiqu'il y ait une parfaite egalité entre les six corps, se sont par suite de tems arrogé le droit de remplir à chaque election quelques cinq places dont le Consulat est composé ; l'un desdits trois corps fournissant presque toujours et le juge et un consul en mesme tems, ce qui les a rendu les maitres des elections, avec lors d'icelles les trois quarts des voix du siege et du corps des anciens consuls.</p>
<p>Cet usage est abusif. Il est prejudiciable au public et blesse le commerce des marchands bonnetiers.</p>
<p>Il est prejudiciable au public en ce qu'il se trouve au siege moins de sujets capables de juger les contestations qui vont au commerce des 3 derniers corps, du moins aussy important que celuy des 3 premiers.</p>
<p>Et il blesse le commerce des marchands bonnetiers ce que n'y ayant aucune préemninence entre les six corps, il n'est pas juste que les premiers, qui ne le sont que par ordre numeraire, se rendent perpetuellement les maitres des autres, sans droit et sans titre, pendant que les trois derniers viennent aux places de juge et de consuls que de loin quoiqu'il se trouve parmy ceux cy des sujets egalement à meme de remplir les fonctions de juge et de consuls, comme il peut s'en trouver dans les trois premiers corps.</p>
<p><pb n="3"/> L'election d'un juge et de 4 consuls a esté faite le 30 janvier dernier suivant les usages precedents. Mais les marchands libraires et les marchands de vin, qui ont déja été admis au Consulat, se sont opposés à cette election. Et ils ont porté leur opposition au Parlement par une requeste qu'ils y ont presentée le 30 janvier 1727, par laquelle ils ont demandé qu'elle soit declarée nulle et qu'il soit procedé à une nouvelle election.</p>
<p>Sur cette requeste, deux arrêts sont intervenus.</p>
<p>Le 1<hi rend="sup">er</hi> du 3 fevrier 1727 portant sursis à la prestation de serment des nouveaux juge et consuls, et cependant que les anciens continüeront de faire leurs fonctions.</p>
<p>Le 2<hi rend="sup">e</hi> du 5 fevrier audit an portant que les sindics des 6 corps des marchands seront tenus de vous remettre incessamment, Monseigneur, chacun leur memoire sur la maniere dont il convient proceder aux elections des juge et consuls, pour estre par vous pris telles conclusions que vous aviserez, et par la cour estre ordonné ce qu'il appartiendra, et que par provision les anciens juge et consuls continüeront l'exercice de leurs fonctions.</p>
<p>De la part des teinturiers du bon teint, ils sont intervenus par requeste du [blanc] fevrier audit an, et ils demandent que la forme prescrite par l'edit de creation pour les elections soit suivie et executée.</p>
<p>Et est l'etat de la contestation.</p>
<p>Les marchands bonnetiers et orfevres joüailliers soutiennent avec raison qu'il y a parité de droit entre eux et les trois premiers corps pour venir egalement et concurremment avec eux à l'election du Consulat.</p>
<p><pb n="4"/> Il faudroit premierement que les 3 premiers corps, pour soutenir leur pretention contraire, eussent un droit, des reglemens à opposer aux trois autres corps.</p>
<p>Or il est certain qu'ils n'en ont aucun. C'est un fait certain et qui ne peut estre contesté par la moindre piece.</p>
<p>Mais les corps des marchands bonnetiers et orfevres joüailliers vont plus loin. Car ils soutiennent et ils prouvent que le droit qu'ils ont de rouller egalement et concurremment avec les trois premiers corps aux elections des juge et consuls a esté conservé dans tous les tems avant l'usurpation des 3 premiers corps.</p>
<p>Pour s'en convaincre, il n'y a qu'à consulter le catalogue des elections imprimé avec le Recueil des edits et declarations du Roy sur l'etablissement de la jurisdiction des consuls de Paris. L'on y verra que dès l'origine de la jurisdiction consulaire, et depuis dans tous les tems, les marchands bonnetiers et orfevres ont toujours fourny des sujets pour remplir les places du siege plus frequemment qu'ils ne font presentement, quoique leurs corps soient considerablement augmenté depuis ces tems là.</p>
<p>D'ailleurs il n'y a nulle distinction d'un drapier ou epicier à un bonnetier ou un orfevre. On nomme là les premiers par un ordre nécessaire. Mais aucun drapier ne se peut dire d'un ordre plus distingué qu'un marchand orfevre ni qu'un marchand bonnetier. Toute la difference qui s'y remarque est que chacun des corps de ceux cy sont au moins cinq fois plus nombreux que celuy du drapier.</p>
<p>Non seulement l'edit de creation de la jurisdiction <pb n="5"/> des consuls, non seulement tous les reglemens intervenus sur le fait de cette jurisdiction n'accordent aucun privilege aux trois premiers corps au prejudice des trois autres pour raison des elections des juge et consuls, ni pour aucun autre chose, mais au contraire les merciers ayant surpris sur requeste un arrest du Conseil le 14 janvier 1689 portant qu'il seroit tous les ans eleu et choisy au moins un marchand du corps de la mercerie pour remplir la place de l'un des consuls, avec deffenses aux autres corps des marchands electeurs et tous autres d'y contrevenir, les cinq autres corps des marchands s'y opposerent, et sur leur opposition il intervint arrest du Conseil contradictoire le 2 juillet 1697, qui reçoit lesdits cinq corps des marchands opposans à celuy du 14 janvier 1689, ordonne qu'il sera doresnavant procedé à l'election des juge et consuls comme auparavant ledit arrest.</p>
<p>Par cet arrest, suivy de lettres patentes du 30 juillet 1697 enregistrées au Parlement le 19 aoust audit an, il n'a pas été permis aux merciers plutôt qu'aux drapiers, ny à ceux cy plutôt qu'aux bonnetiers et aux orfevres de s'aproprier annuellement une place fixe et assurée dans le siege au prejudice des autres.</p>
<p>Rien n'a changé l'ordre qui doit estre observé dans la justice entre les six corps que l'usurpation des trois premiers, lesquels, en se reunissant ensemble, ont trouvé le secret de laisser les trois derniers après eux et de remplir continüellement le Consulat des sujets de leurs corps, ayant esté toujours plus forts en nombre de sufrages, et ayant par là exclus les autres des places qui leur etoient communes avec eux.</p>
<p><pb n="6"/> Les marchands bonnetiers et orfevres demandent donc un concours egal et alternatif entre les six corps pour remplir les places aux elections des juge et consuls. Leur pretention est fondée sur le droit commun des six corps des marchands, du nombre desquels ils sont incontestablement, sur l'egalité qu'il y a entre eux, sur la possession de cette egalité qui a esté dans tous les tems et qui doit toujours subsister, sur l'objet des contestations qui arrivent tous les jours relativement à leurs differens commerces qui demandent un nombre egal de chacun des six corps pour juger des contestations. Car il est du bien de la justice et de l'interest du public que lorsqu'il s'agit de decider un differend à l'occasion du commerce d'un orfevre ou d'un bonnetier, il se trouve dans le nombre des consuls un bonnetier ou un orfevre qui puisse l'entendre et le juger en connoissance de cause.</p>
<p>Si l'on entroit dans le detail du commerce d'un orfevre pour le comparer à ceux des autres corps, l'on seroit surpris de la difference notable qu'il y a de l'un à l'autre pour l'objet de leurs commerces, difference que l'on sentira bien mieux en donnant une notion du commerce de l'orfevre et des details qui en font une partie.</p>
<p>L'on demande donc si les marchands des cinq autres corps pourroient entendre les contestations qui naissent tous les jours entre des fourbisseurs, des fondeurs, des horlogers, des graveurs, des lapidaires, des doreurs, des boutonniers et des batteurs et tireurs d'or, tous artisans qui tous les jours ont des differends pour raison des matieres d'or et d'argent et pour leur commerce et leur travail. Ils <pb n="7"/> scauroient ce que c'est que titre, alloy, alliage, deniers, grains, karats et trente deuxiemes de fin. Si l'on joint à cela tous les ouvriers qui ont trait à l'orfevrerie, les cizeleurs, les planeurs, et le reste de ceux qui travaillent aux matieres d'or et d'argent, dont le nombre est infiny tant pour les travaux differents qui se font sur ces metaux precieux que pour la connoissance du degré de bonté interieure des matieres, où en seroient tant le drapier que les marchands des autres corps qui se trouveroient en place ? A la premiere veue, de ce que l'on vient de détailler, ils se trouveroient dans la nécessité indispensable d'appeller un marchand orfevre, au lieu que ce même marchand orfevre qui se trouveroit au siege du Consulat leur eclairciroit leurs doutes et les mettroit en etat de statuer sur le champ sur la contestation qui se presenteroit, par la connoissance qu'il auroit du fait qu'il y auroit à decider.</p>
<p>Ce qui justiffie la justice qu'il y a d'entretenir une juste egalité et un concours mutuel entre les trois premiers corps et les trois derniers.</p>
<p>Le reglement que les marchands bonnetiers et orfevres demandent à la Cour est par raport aux elections qui se feront à l'avenir. A l'egard de l'election qui vient d'estre faite, ils s'en rapportent à vous, Monseigneur, et à la Cour pour en ordonner.</p>
<p>Et quant à la pretention des marchands libraires et des marchands de vin, les orfevres et bonnetiers s'en raportent aussy à la Cour pour déterminer à cet egard ce qu'elle jugera estre du bien public, en quoy ils sont bien persuadés qu'elle ne prejudiciera en rien aux droits des six corps des marchands.</p>
<p><pb n="8"/> Reste la pretention des teinturiers. Quelque consideration qu'ils ayent voulu s'attirer par leur requeste, les supplians n'estiment pas, sauf le respect deu à la Cour, que leur communauté soit en droit de demander d'estre admise aux elections de la maniere qu'ils le proposent. Selon eux ils n'ont, en tout depuis l'erection des juge et consuls, eu que trois sujets. Encore l'election a-t-elle été faite dans des tems si reculés que leur droit est prescrit et aneanty dans l'espace immense qui s'est ecoulé depuis ces elections. D'ailleurs ils donnent à entendre qu'elles ont été faits par raport aux grands richesses dont ils décorent leurs predecesseurs, et l'on sçait que ce n'est jamais sur un tel titre que l'on peut se fonder pour aspirer à un honneur qui ne doit estre accordé qu'au merite, à la probité et à l'etendüe des connoissances de ceux qui sont elus.</p>
<p>Sur le fondement, de ce que les bonnetiers et les orfevres viennent d'observer, ils demandent</p>
<p>Premierement l'execution de l'edit de creation de la jurisdiction des consuls du mois de 9bre 1563, enregistré en la Cour le 18 janvier de la même année.</p>
<p>2ement que les marchands bonnetiers et les marchands orfevres seront maintenus dans le droit qu'ils ont d'estre elus aux places de juge et consuls, et ce egalement et concurremment avec les marchands drapiers, epiciers, merciers et pelletiers, et que dans les elections des juge et consuls qui se feront à l'avenir, les cinq places du juge et de 4 consuls seront remplies doresnavant par cinq sujets des six corps des marchands, qui seront pris et choisis en chacun d'iceux alternativement.</p>
</div>
</body>
</text>
</TEI>