<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">037. "A Monseigneur le chancelier." (1749)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Ms Joly de Fleury 60 (Fol.241), 1749, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Ms Joly de Fleury 60 (Fol.230)</idno></msIdentifier><head><title>n° 0426 BNF, Ms Joly de Fleury 60 (Fol.241) [1749] [manuscrit]</title><origDate when="1749">1749</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>accès au Consulat — codification coutumière</term>
</keywords>
<keywords ana="parties">
<term>marchands de vin contre Consulat</term>
</keywords>
<keywords ana="juridiction">
<term>parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0426" type="factum">
<head>BNF, Ms Joly de Fleury 60 (Fol.241) [1749] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">A Monseigneur le chancelier.</hi></p>
<p>Monseigneur, les maitres et gardes du corps des marchands de vins ont l'honneur de representer tres respectueusement à vôtre Grandeur, au nom de leur corps, qu'ils ne peuvent garder le silence sur la contestation qui vient de s'elever entre les six corps et les sindics de la librairie. Il y a deux ans que les suplians ont été dans le même cas, ils eurent alors l'honneur de presenter leurs memoires à M. le premier president, à M. le procureur general, où, en exposant les justes motifs de leurs plaintes, ils ont rendu compte des manœuvres qui se pratiquoient annuellement par les six corps pour se perpetuer dans les places du Consulat, et en eloigner les suplians et les libraires, <pb n="2"/> en sorte qu'ils n'obtiennent qu'une place dans trois, quatre ou cinq ans, au lieu que les drapiers, epiciers et merciers y ont tous les ans des sujets, en sorte que depuis 12 ans les drapiers et epiciers ont eu chacun 12 sujets et les merciers 11, au lieu que les suplians n'en ont eu que trois.</p>
<p>C'est contre la disposition expresse de la declaration du Roy du 18 mars 1728, enregistrée au Parlement, sur l'election des consuls et sur les sujets sur lesquels cette election doit tomber, que les six corps se sont arrogés le droit de disposer des places de ce tribunal, en sorte qu'ils n'y admettent que ceux que bon leur semble.</p>
<p>Les drapiers, les epiciers et les merciers particulierement s'emparent tous les ans, presque sans interruption, des trois premieres places du Consulat, et n'admettent à la quatrieme que les sujets qu'il leur plait, et toujours par preference ceux qui leur son presentés par les trois autres corps. Par cette usurpation, ils sont toujours les maitres des suffrages, de façon que la convocation qui se fait tous les ans pour elire les quatre nouveaux consuls <pb n="3"/> devient inutile, et la pretendüe election qui s'y fait n'est qu'une election fictive où l'on ne fait que nommer les sujets qui ont été indiqués et choisis préalablement dans l'assemblée des six corps.</p>
<p>Ce qui vient de se pratiquer à l'egard des libraires, et ce qui se pratiqua en 1747 à l'egard des suplians, sont des preuves de l'injuste domination des six corps qui se sont arrogés l'autorité de disposer des places, et obligent les libraires et les marchands de vins à faire plusieurs années consécutives des démarches auprès d'eux avant de consentir à leur ceder une des places. Ils poussent même la domination jusqu'à vouloir indiquer les sujets qu'il leur plait, comme s'ils étoient plus en etat que les gardes des marchands de vins ou que les sindics des libraires de connoître la capacité et les talens de ceux de ces deux corps qui sont proposés pour être admis au Consulat.</p>
<p>Dès l'année 1564, au commencement de l'etablissement de la jurisdiction consulaire, le corps des marchands de vins a fourni au siege un consul pendant les années 1565, 1567 et 1569. Il a fourni trois juges en ce même siege jusques en l'année 1610. Il y a eu presque sans discontinuation un marchand de vin en qualité de consul ou de juge.</p>
<p>Il paroît par l'extrait de la jurisdiction consulaire que depuis 1645, les six corps se sont apliqués à n'y admettre des marchands de vins <pb n="4"/> que tres rarement, et même depuis 15 ou 16 ans ils n'accordent que la 4<hi rend="sup">e</hi> place de consul, et ne jettent plus au sort comme il a toujours été d'usage le rang des places.</p>
<p>Les six corps ne peuvent cependant s'excuser sur l'incapacité des sujets de ces deux compagnies, puisqu'ils ne peuvent disconvenir que ceux qui ont passé depuis plusieurs années, et celui qui est actuellement en place, ont donné des preuves de leurs talens dans le commerce, et se sont signalés par leur intelligence dans les affaires les plus difficiles de la negociation.</p>
<p>Les six corps ne peuvent encore disconvenir que le corps des marchands de vins est un des plus nombreux de Paris, et celui dont les operations sont les plus étendües, qu'il est par consequent celui de tous les corps auquel il est le plus necessaire de trouver à ce tribunal un sujet qui, ayant été à la teste de son corps, soit le plus en état de connoitre les usages de ce commerce et les discussions nombreuses qu'il entraine.</p>
<p>L'interpretation la plus naturelle que l'on puisse donner à la déclaration du Roy est que chacun des corps donne alternativement des sujets aux consuls, mais plus particulierement encore ceux de ces corps qui, étant les plus étendus, fournissent plus d'occasions aux décisions de ce tribunal.</p>
<p>Les drapiers, qui se sont etablis une succession injuste et continuelle dans ce tribunal, ne <pb n="5"/> forment entr'eux qu'un corps peu nombreux, celui des pelletiers et celui des orfevres joints à celui de la draperie ne fournissent pas entr'eux trois une seule cause au Consulat contre dix qui y sont portées par le corps de la marchandise de vin, soit pour les achats et livraisons des vins par les commissionnaires, les voitures par eau ou par terre, les marcs la livre, les jauges des vaisseaux à vin des differentes provinces, les correspondances et associations des marchands, les negociations et achats en commun de differentes especes de vins, les variations des prix suivant les années et les qualités des vins ou le temps des</p>
<p>livraisons, l'etat et la qualité des vins lors des achats comparé à celui des arrivées, sans parler des discussions ordinaires de ce commerce qui sont infinies. Ces raisons y rendent la présence d'un sujet du corps des marchands de vins infiniment plus nécessaire que celle des trois autres corps, qui, avec toute l'experience qu'ils pourroient avoir, ne peuvent décider des questions qui dependent totalement de l'usage d'un commerce qui leur est absolument inconnu.</p>
<p>Si l'alternative n'est pas ordonnée, il ne sera jamais possible, quelque précaution que puissent prendre Nosseigneurs les magistrats, d'empêcher les six corps de continuer leurs manœuvres pour se perpetuer au siege à la pluralité des voix. La raison en est sensible.</p>
<p>On a observé qu'ils convenoient dans l'assemblée <pb n="6"/> des six corps, faite avant celle du Consulat, des sujets qu'ils y placeroient.</p>
<p>On appelle à l'election du Consulat cinq des gardes en charge de chaque corps. C'est pour les six corps trente personnes. Les suplians et les libraires ne forment que dix voix. Le surplus des autres marchands au nombre de 20 que l'on appelle à cette election sont choisis à la volonté du juge et des consuls, et ne manquent jamais de nommer les personnes qui leur sont indiquées par un bulletin qui leur est remis par le siege, en sorte qu'il y a 50 voix contre dix, en retranchant la moitié des soixante qui composent la totalité des voteurs, il restera trente. Le juge et les quatre consuls qui sont au siege, et les deux scrutateurs qui sont choisis par le juge et par le premier consul, font sept personnes, qui sont toujours du party des six corps. Cela forme trente sept voix. Et quand, dans les trente restans, les dix marchands de vins et libraires auroient été par hazard conservez, ce qui n'arrive pas quand les billets ont été mêlez, il reste toujours vingt sept voix contre dix, compris le juge, les quatre consuls et les deux scrutateurs par eux choisis. Ainsy les marchands de vins et les libraires ne peuvent jamais être nommez que du consentement des <pb n="7"/> six corps, au lieu que si les huit corps fournissoient à tour de rolle des sujets, chacun en presenteroit regulierement et l'on n'auroit à choisir que dans le nombre des sujets que chaque corps auquel il appartiendroit de fournir proposeroit. Le tout se feroit de bonne foy.</p>
<p>C'est surtout ce que dessus que les suplians joignent leurs remontrances à celles des sindics des libraires, ont recours à vôtre justice, Monseigneur, pour qu'il vous plaise accorder un arrêt du Conseil qui, en interpretant la déclaration du Roy du 18 mars 1728, reforme l'abus que les six corps ont introduit en ce qui regarde le Consulat et les sujets qui doivent y être appellés, et les obliger à remplir l'esprit de la déclaration du Roy en admettant au Consulat alternativement et sans préference des sujets de tous les huit corps qui ont roit d'y être admis, et ordonner qu'apres l'election faite on jettera au sort, comme on a fait dans tous les temps depuis l'etablissement du Consulat, les places des consuls, affin qu'elles puissent être remplies sans préference par ceux des corps auxquels il appartiendra de fournir des sujets circulairement. Les suplians continueront leurs vœux au ciel pour la conservation de vôtre Grandeur.</p>
</div>
</body>
</text>
</TEI>