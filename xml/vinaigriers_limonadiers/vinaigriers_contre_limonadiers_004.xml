<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">004. "Memoire pour les Jurez &amp; Gardes de la Communauté des Marchands d'Eau de Vie, Limonadiers, Distillateurs de toutes sortes d'Eaux, de cette Ville de Paris. Contre les Jutez Vinaigriers de cette Ville." (1694)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, FOL-FM-12417, 1694, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>FOL-FM-12620</idno></msIdentifier><head><title>n° 0110 BNF, FOL-FM-12417 [1694] [imprimé]</title><origDate when="1692">1692</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>eau-de-vie — gros et détail — petite mesure</term>
</keywords>
<keywords ana="parties">
<term>limonadiers contre vinaigriers</term>
</keywords>
<keywords ana="juridiction">
<term>parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0110" type="factum">
<head>BNF, FOL-FM-12417 [1694] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Memoire pour les Jurez &amp; Gardes de la Communauté des Marchands d'Eau de Vie, Limonadiers, Distillateurs de toutes sortes d'Eaux, de cette Ville de Paris.</hi></p>
<p><hi rend="italic"><hi rend="bold">Contre les Jurez Vinaigriers de cette Ville.</hi></hi></p>
<p>Toute la contestation du Procez se reduit à sçavoir si l'Arrest de la Cour de l'année 1694 intervenu entre les parties, qui maintient les Vinaigriers dans la possession du gros &amp; du détail de l'Eau de Vie, le petit détail, c'est à dire la faculté d'en donner à boire chez eux sur le comptoir aux passans, leur est donné de la mesme maniere qu'aux Marchands d'Eau de Vie.</p>
<p>Les Vinaigriers tiennent l'affirmative, que l'Arrest de la Cour a jugé.</p>
<p>Les Marchands d'Eau de Vie au contraire soutiennent la negative, &amp; que la Sentence de Reglement renduë à la Police dont est appel n'a point touché aux veritables dispositions de l'Arrest, &amp; qu'elle doit subsister.</p>
<p><hi rend="italic"><hi rend="bold">Fait.</hi></hi></p>
<p>Les parties ont eu un Procez en la Cour pour sçavoir si les Vinaigriers auroient le gros &amp; le détail de l'Eau de Vie dans Paris, soit qu'ils la fissent venir à Paris, ou qu'ils l'acheptassent des Marchands forains &amp; autres negocians de cette Ville.</p>
<p>Les Marchands d'Eau de Vie, qui vouloient les exclure de cette faculté &amp; les recevoir à vendre seulement par pintes &amp; petites mesures celles qu'ils distilleroient, se pourveurent d'abord au Conseil qui les renvoya à la Cour où, les parties ayant procedé, intervint d'abord un Arrest interlocutoire qui ordonna que l'Instance &amp; Procez seroit communiquée aux Officiers de Police pour avoir leur avis.</p>
<p>Inutilement rapporteroit-on tout ce qui se passa dans la suite de ce Procez ; l'avis fut donné &amp; rapporté en la Cour, favorable aux Vinaigriers ; il porte en substance que l'on estime qu'ils doivent estre maintenus dans la possession du gros &amp; du détail des Eaux de Vie de telle qualité qu'elles soient, &amp; ajoûte que l'on doit les empescher neanmoins d'en donner à boire chez eux &amp; sur le comptoir comme font les Limonadiers.</p>
<p>Les Vinaigriers raporterent cet avis en la Cour, en demanderent l'omologation ; &amp; après l'Instance reveuë avec l'avis par Monsieur le Procureur <pb n="2"/> General, qui donna ses conclusions en faveur des Marchands d'Eau de Vie à la veille du jugement du Procez, l'on donna à la verité une Requeste de la part des Marchands d'Eau de Vie qui tendoit à faire défenses aux Vinaigriers d'en donner à boire chez eux ; &amp; peu de temps après, l'Instance rapportée en la Grande Chambre par Mr l'Abbé Robert y fut partagée ; le partage porté en la premiere des Enquestes, elle le fut une seconde fois, enfin départagée en la seconde en faveur des Vinaigriers par l'Arrest de 1694, qui maintient les Marchands d'Eau de Vie dans le droit &amp; possession du gros &amp; du détail de l'Eau de Vie, fait défenses aux Vinaigriers de vendre aucunes liqueurs, &amp; maintient pareillement les Vinaigriers en la possession du gros &amp; du détail de l'Eau de Vie, &amp; sur le surplus des demandes met les parties hors de cours.</p>
<p>En execution de cet Arrest, les Vinaigriers ont pretendu que la permission d'en donner à boire chez eux leur estoit permise, qu'ils pouvoient en user de la mesme maniere que les Marchands d'Eau de Vie, ce qui a donné lieu à une Sentence de Reglement renduë sur la requisition du Substitut de Monsieur le Procureur General à la Police, qui leur fait défenses, &amp; à toutes personnes ayant qualité d'en vendre, d'en donner à boire chez eux comme font les Marchands d'Eau de Vie.</p>
<p>Les Vinaigriers qui pouvoient venir à la Police faire valoir leurs raisons parce que cette Sentence n'estoit point renduë avec eux, assignez dans la suite devant le sieur Lieutenant de Police, ils n'ont pas voulu y comparoistre parce qu'ils sont penetrez que l'on sçait parfaitement les distinctions qu'il y a à faire des facultez de ces deux Communautez ; ils ont interjetté appel de la Sentence de Reglement &amp; des Ordonnances qui l'on suivie. Voilà l'estat de la contestation.</p>
<p><hi rend="italic"><hi rend="bold">Moyens des Vinaigriers pour détruire la Sentence de Reglement.</hi></hi></p>
<p>1<hi rend="sup">e</hi> Les Vinaigriers ont droit &amp; possession de vendre de l'Eau de Vie tant en gros qu'en détail, ils l'ont fait voir lors de l'Arrest de 1694.</p>
<p>2<hi rend="sup">e</hi> Le droit &amp; leur possession leur ont esté confirmez par le mesme Arrest.</p>
<p>3<hi rend="sup">e</hi> L'Arrest de Police ne leur ostoit point ce détail &amp; cette faculté d'en donner à boire chez eux, ce n'estoit que par une simple remontrance ; enfin quand cela seroit, l'Arrest a jugé au contraire.</p>
<p>4<hi rend="sup">e</hi> Il est si vray que cela est de la sorte, que les Marchands d'Eau de Vie ont demandé que défenses leur fussent faites d'en donner à boire chez eux, ce mesme Arrest a mis sur cette demande hors de cours ; ainsi chose jugée, la Sentence a jugé le contraire, &amp; par conséquent doit estre infirmée.</p>
<p><hi rend="italic"><hi rend="bold">Réponses des Marchands d'Eau de Vie.</hi></hi></p>
<p>L'on convient qu'il n'y a personne qui d'abord ne soit prevenu contre les Marchands d'Eau de Vie ; mais faisant attention sur les circonstances <pb n="3"/> du Procez, on decouvrira que la raison &amp; l'équité est de ce costé.</p>
<p>Premierement, il n'est point vray que les Vinaigriers ayent le droit &amp; la possession du gros &amp; du détail des Eaux de Vie. C'est aux Marchands à qui il apartient depuis plus d'un siecle, la Communauté qui en avoit le droit primitif ayant esté unie à leur corps, l'Arrest de la Cour le distingue ; car il ne maintient les Vinaigriers que dans la possession, donc ce n'est que la possession qui leur a acquis ce droit aux termes de l'Arrest.</p>
<p>2<hi rend="sup">e</hi> L'on soûtient que l'Arrest les ayant maintenus dans la possession du gros &amp; du détail de l'Eau de Vie, c'est-à-dire comme ils avoient accoûtumé de faire. Or l'on soûtient, &amp; les Vinaigriers n'ont osé mettre le fait contraire, que ce n'est que depuis peu de temps qu'ils se sont meslez d'en donner à boire chez eux &amp; de s'attribuer ce petit détail, que ce temps n'a pas pû leur acquerir de possession, &amp; par consequent.</p>
<p>L'on a fait voir dans les écritures que le détail parmi les Marchands &amp; corps de Communauté se partage ; c'est-à-dire qu'une telle Communauté a la faculté de vendre de telle marchandise en gros &amp; l'autre en détail, que ce détail est borné, comme en cette rencontre l'on convient que les Vinaigriers peuvent en vendre par pintes &amp; petites mesures, mais qu'ils ne sçauroient en donner à boire chez eux. Les Vinaigriers en ont eux-memes produit la preuve, &amp; cela est de notorieté dans cette ville. Les Vinaigriers ont incontestablement le gros &amp; détail de toutes manieres du vinaigre, comme les Marchands d'Eau de Vie l'ont des Eaux de Vies. Cependant il y a des Communautez qui participent de leurs droits : les Espiciers vendent du Vinaigre en détail, les Chandeliers, les Fruitieres n'en peuvent vendre en gros ny que certaine quantité de pintes, dont il est vray que le détail se partage. Si cela est, peut-on croire que la Cour ait pretendu oster un droit aux Marchands d'Eau de Vie, &amp; confirmer une possession aux Vinaigriers d'en donner à boire chez eux, qu'ils n'ont jamais euë ? Or c'est dans cette distinction que les Officiers de Police <hi rend="italic">proprio motu</hi> instruits des fonctions de ces deux Communautez, entre lesquelles ils venoient de donner leur avis rapporté à la Cour, penetrez que l'Arrest n'a point jugé ce petit détail en faveur des Vinaigriers, ont entré en prononçant la Sentence de Reglement dont est appel.</p>
<p>3<hi rend="sup">e</hi> L'on soûtient qu'il n'a jamais esté question lors de l'Arrest de sçavoir si l'on donneroit ce petit détail aux Vinaigriers, on n'y a pas voulu entrer.</p>
<p>En effet, ce n'estoit que le gros &amp; le détail par pintes qui a esté renvoyé en la Cour ; cela si vray que, quand elle a desiré d'avoir l'avis de Police, ces Officiers n'y ont pas voulu entrer ; &amp; en faisant cette remontrance qu'il estoit à propos d'exclure les Vinaigriers d'en donner à boire chez eux, cela marque que s'il y avoit eu une demande formée, ils auroient donné leur avis precisement. Ces officiers, s'estans renfermez dans la matiere des veritables contestations, ont conduit la Cour à en faire de <pb n="4"/> mesme ; les Vinaigriers, en ayant demandé l'homologation par leurs écritures, se seroient voulu insinuer qu'on leur a accordé <hi rend="italic">ultra petita</hi>.</p>
<p>4<hi rend="sup">e</hi> Si la Cour a prononcé un hors de cours sur la demande des Marchands d'Eau de Vie pour ce petit détail, ce n'est pas à dire que l'on ait entendu par l'Arrest leur donner ; il ne faut que quelques reflexions pour se persuader.</p>
<p>La seconde est qu'en matiere de Reglement entre Communautez, il estoit de necessité de la part des Vinaigriers d'en former une demande precise afin de mettre la Cour en estat d'y prononcer, ce qui n'ayant esté fait, il s'ensuit que l'on ne peut pas dire que l'Arrest leur ait attribué un droit nouveau qu'ils n'avoient point, qu'ils ne demandoient pas, &amp; dont ils n'avoient aucune possession.</p>
<p>La troisième, qu'il faut bien faire de la difference entre le hors de cours sur demande &amp; le débouté au premier cas, ce terme marque bien qu'on n'a pas voulu écouter les Marchands d'Eau de Vie qui auroient dû former leur demande <hi rend="italic">ab initio</hi>, ou du moins lorsque le Procez fût renvoyé en la Cour avant l'avis rapporté, &amp; ne pas attendre quatre ou cinq jours avant le Procez rapporté. Ce qui a conduit la Cour à les mettre hors de cours ; au second cas l'on convient que le débouté feroit presumer quelque chose de plus fort, &amp; authoriseroit davantage les Vinaigriers.</p>
<p>En un mot, si l'Arrest avoit entendu leur donner ce petit détail, il n'auroit pas manqué de s'expliquer par son Arrest, &amp; de les y maintenir, comme il l'a fait, dans le negoce &amp; le détail par pintes, ou du moins de dire qu'ils feroient le negoce &amp; le détail de la mesme maniere que les Marchands d'Eau de Vie. Cela n'estant pas, on a lieu de soutenir la Sentence de Reglement qui se trouve tellement juridique qu'on a osé venir à la Police, où l'on est instruit parfaitement des fonctions de ces deux Communautez.</p>
<p>Enfin les Marchands d'Eau de Vie soutiennent l'ouvrage des Officiers de Police qui venoient tout recemment de donner leur avis en la Cour ; ils ont eu leurs motifs dont la Cour peut s'instruire en le demendant ; l'Arrest leur est connu ; &amp; si les Vinaigriers n'estoient pas persuadez que ce petit détail est un droit nouveau qu'ils vouloient s'attribuer, ils devoient y comparoistre &amp; faire valoir l'Arrest ; il estoit mesme des regles d'y proceder, la Sentence n'estant pas renduë à la poursuite des Marchands d'Eau de Vie ; leur appel marque leur defiance &amp; fait esperer que la Cour n'entrera pas dans les fausses couleurs qu'ils proposent.</p>
<p>Orry, Proc.</p>
</div>
</body>
</text>
</TEI>