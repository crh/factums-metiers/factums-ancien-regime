<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">001. "Factum pour les Jurez &amp; Gardes des Maistres Distillateurs d'Eau de Vie, &amp; de toutes autres Eaux &amp; Marchands d'Eau de Vie, &amp; de toutes sortes de Liqueurs, en la Ville, Fauxbourgs &amp; Banlieuë de Paris. Contre les Jurez &amp; Vinaigriers de cette Ville de Paris." (1692)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Z-Thoisy-387 (Fol.331), 1692, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Z-Thoisy-387 (Fol.331)</idno></msIdentifier><head><title>n° 0113 BNF, Z-Thoisy-387 (Fol.331) [1692] [imprimé]</title><origDate when="1692">1692</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>eau-de-vie — gros et détail — qualité de marchand — Paris et province</term>
</keywords>
<keywords ana="parties">
<term>limonadiers contre vinaigriers</term>
</keywords>
<keywords ana="juridiction">
<term>parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0113" type="factum">
<head>BNF, Z-Thoisy-387 (Fol.331) [1692] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Factum pour les Jurez &amp; Gardes des Maistres Distillateurs d'Eau de Vie, &amp; de toutes autres Eaux &amp; Marchands d'Eau de Vie, &amp; de toutes sortes de Liqueurs, en la Ville, Fauxbourgs &amp; Banlieuë de Paris.</hi></p>
<p><hi rend="italic"><hi rend="bold">Contre les Jurez &amp; Vinaigriers de cette Ville de Paris.</hi></hi></p>
<p>La Cour a trois demandes ou questions principales à juger, qui sont tellement importantes aux Marchands d'Eau de Vie que, si ils n'y réussissoient pas (comme ils l'esperent), leur Corps de Marchands qui en faisoit deux avant l'année 1676 deviendroit absolument à rien.</p>
<p>La premiere est de sçavoir si les Vinaigriers ont droit de pretendre au negoce de l'Eau de Vie, c'est-à-dire si par Lettres Patentes, Statuts, Titre ou Possession legitime &amp; paisible, l'achat des Eaux de Vie des Provinces, l'envoy &amp; le debit tant en gros qu'en détail leur est permis.</p>
<p>La seconde, si ils ont titre, possession ou autres pieces qui puissent les autoriser à la vente des fruits confits avec de l'Eau de Vie, &amp; de toutes sortes de liqueurs.</p>
<p>La troisième, si les Vinaigriers qui n'ont effectivement point de titre constitutif ny de possession legitime pour le negoce de l'Eau de Vie &amp; la vente des fruits confits &amp; liqueurs, qui au contraire en sont exclus par Arrests du Conseil d'Estat, registrez en la Cour, ne doivent pas estre condamnez en des dommages, interests considerables pour lesquels on s'est restraint à dix mil livres.</p>
<p><hi rend="italic"><hi rend="bold">Fait du procez.</hi></hi></p>
<p>Le fait est constant que les Distillateurs unis aux Marchands d'Eau <pb n="2"/> de Vie estoient ceux qui faisoient au commencement du siecle cette Eau de Vie merveilleuse, qui fut jugée si salutaire pour la santé des peuples qu'après une épreuve publique faite en presence de Medecins &amp; autres personnes experimentez, en consequence d'Arrest de la Cour de l'année 1634, ils furent érigez en Corps de Maistrise par Sa Majesté, &amp; leurs Statuts redigez par les sieurs Lieutenant Civil &amp; Procureur du Roy du Chastelet de Paris, &amp; ensuite registrez en la Cour des Monnoyes en consequence d'une Declaration du Roy.</p>
<p>Il est encore certain que les Vinaigriers n'avoient point pour lors la qualité de Distillateurs d'Eau de Vie &amp; n'en faisoient aucun debit ny negoce ; leurs anciens Statuts, l'Arrest de 1634 &amp; autres precedens, rendus avec eux &amp; les Faiseurs d'Eau de Vie de ce temps, le prouvent trop manifestement pour qu'ils ozent en disconvenir.</p>
<p>Cette qualité de Distillateur d'Eau de Vie ne leur fût donnée qu'en l'année 1658 dans une concession de nouveaux Statuts qu'ils demanderent à sa Majesté, dans laquelle ils ont glissé cette qualité, afin de s'attribuer la fabrication de l'Eau de Vie qu'ils n'avoient point auparavant.</p>
<p>Cela est vi veritable qu'après l'enregistrement de leurs Statuts, qui ne leur donne que la fabrication de l'Eau de Vie, il y eut procez entr'eux &amp; les veritables Distillateurs, qui est encore pendant en la Cour, ainsi qu'il est justifié par les Arrests de la Cour produits de l'année 1666, qui reçoit les Vinaigriers Appellans des Jugemens de la Cour des Monnoyes qui les excluoient de cette fabrication &amp; distillation d'Eau de Vie.</p>
<p>En 1673, Sa Majesté crea un Corps de Marchands d'Eau de Vie &amp; de toutes sortes de Liqueurs, auquel il en attribua principalement le negoce &amp; fit deffenses aux Vinaigriers d'en vendre &amp; de les troubler.</p>
<p>En 1675, les Vinaigriers s'opposerent à l'enregistrement des Lettres Patentes par un acte volant, sans dire les moyens de leur opposition.</p>
<p>Mais en 1676, Sa Majesté ayant sçeu le rapport qu'il y avoit entre le Corps ancien des Distillateurs &amp; les Marchands d'Eau de Vie, par un Arrest solemnel d'Estat, il les réunit ensemble sous le titre exprimé au commencement du Factum, &amp; reïtera les deffenses aux Vinaigriers de s'immiscer dans leurs fonctions.</p>
<p>Cet Arrest dut lû, publié, affiché, registré, &amp; les Statuts redigez &amp; pareillement registrez purement &amp; simplement sans aucune modification ny opposition le 27 Mars 1676.</p>
<p>Depuis cet Arrest, les Marchands d'Eau-de-Vie ont eu toutes les peines du monde à s'establir, &amp; les Vinaigriers ont profité de leur <pb n="3"/> impuissance pour tâcher de s'attribuer non seulement un droit qui ne leur appartient point, qui consiste en la Fabrication &amp; Distillation de l'Eau de Vie, mais encore l'ont porté jusques à en negocier ouvertement &amp; en acheter de celle des Provinces, &amp; mesme par une entreprise sans bornes se sont ingerez dans le debit des fruits confits avec l'Eau de Vie &amp; des Liqueurs.</p>
<p>Ces entreprises ont contraint les Marchands d'Eau de Vie de se pourvoir au Conseil, qui s'estoit reservé la connoissance du trouble qui leur pouvoit estre fait ; &amp; sur une Requeste par eux presentée, Sa Majesté a trouvé à propos de renvoyer en la Cour pour faire ce Reglement si important aux Marchands d'Eau de Vie, &amp; qui consiste dans le Jugement de trois questions cy devant proposées.</p>
<p>Voila le fait dans sa pureté ; il faut presentement examiner les raisons rapportées par les Vinaigriers pour maintenir le droit du negoce &amp; de l'achapt des Eaux de Vie des Provinces ou Païs estrangers, &amp; en même temps par les réponses faire connoistre que jamais pretention ne fut plus temeraire, puisqu'elle n'est soutenuë d'aucun Titre.</p>
<p><hi rend="bold">Moyens des Vinaigriers.</hi></p>
<p><hi rend="italic">Premier.</hi> Qu'on ne peut les exclure du negoce de l'Eau de Vie, parce qu'ils en sont en possession de tout temps, mesme de la vente des fruits confits &amp; Liqueurs.</p>
<p><hi rend="italic">Deuxième.</hi> Que les Espiciers ayant voulu disputer &amp; saisir chez eux de l'Eau de Vie, la saisie a esté declarée injurieuse par un Arrest de la Cour du 10 Avril 1666 confirmatif d'une Sentence du 9 Février 1664, &amp; qu'ainsi on peut interdire ce negoce.</p>
<p><hi rend="italic">Troisième.</hi> Que les deffenses portées par l'Arrest du Conseil d'Estat ne sont considerables non plus que les Statuts des Marchands d'Eau de Vie, parce qu'ils sont Opposans à l'enregistrement, que ces oppositions subsistent, &amp; par consequent.</p>
<p><hi rend="italic">Quatrième.</hi> Que les Statuts des Marchands d'Eau de Vie sont à leur <pb n="4"/> avantage, puisqu'ils n'interdisent le negoce qu'à ceux qui n'ont point de qualité &amp; possession d'en acheter &amp; vendre ; &amp; consequemment qu'estant dans le cas de cette exception, on ne peut pas les en priver.</p>
<p><hi rend="italic"><hi rend="bold">Réponses des Marchands d'Eau de Vie au premier Moyen.</hi></hi></p>
<p>Que cette possession des Vinaigriers est une pure illusion, puisqu'il est prouvé au procès par leurs propres titres qu'avant l'année 1658, ils ne se mesloient de vendre que du Vinaigre &amp; Moutarde, &amp; n'estoient appellez autrement que Vinaigriers Moutardiers, que par aucun article des Statuts procedans à cette année, il n'est parlé d'Eau de Vie ; mais pour rendre cette preuve asseurée, on rapporte l'Arrest de 1634 rendu avec les Vinaigriers &amp; Faiseurs d'Eau de Vie aux Marchands, à qui les Vinaigriers voulans empescher l'achapt des lies fraiches de vin pour en faire de l'Eau de Vie, il leur fut neanmoins après une épreuve permis d'en acheter pour en distiller, ce qui manifeste bien qu'ils ne se mesloient point en ce temps de la fabrication &amp; du negoce d'Eau de Vie.</p>
<p>Qu'il est impossible de faire remonter leur droit au delà de l'année 1658, en laquelle ils ont surpris la qualité de Distillateurs ou Ouvriers d'Eau de Vie purement &amp; simplement, &amp; non pas le negoce qui a esté le fondement de l'établissement du Corps des Marchands d'Eau de Vie ; la Cour sçait les differences qu'il y a entre le Marchand &amp; l'Ouvrier quand ils sont Corps distincts &amp; separez dans une Ville comme Paris.</p>
<p>Que cette possession toute imaginaire &amp; imparfaite qu'elle est, estant relative à un titre moderne, ne leur peut pas attribuer des droits nouveaux sans auparavant obtenir des Lettres Patentes de Sa Majesté.</p>
<p>Qu'ils peuvent si peu s'en prévaloir qu'en l'année 1673 &amp; 1676, l'interdiction leur a esté non seulement prononcée pour le negoce, mais mesme pour la distillation d'Eau de Vie, &amp; qu'ils ont même esté jugez sans qualité de Distillateurs puisque ceux que Sa Majesté a reconnu pour tels ont esté unis aux Marchands d'Eau de Vie.</p>
<p>Et ainsi ce n'est que par une pure tollerance qu'on leur laisse la qualité de Distillateurs, à condition qu'ils ne pourront l'estendre au delà de ses veritables bornes écrites dans leurs Statuts.</p>
<p><hi rend="italic"><hi rend="bold">Réponse au second Moyen.</hi></hi></p>
<p>1<hi rend="sup">e</hi> Il ne s'agissoit pas dans le procès des Espiciers d'une contestation <pb n="5"/> pour le negoce de l'Eau de Vie, mais simplement d'un droit de visite que les Espiciers pretendoient ; &amp; pour peu d'attention qu'on fasse sur l'Arrest de 1666, qu'oppose les Vinaigriers, on découvrira aisément que la saisie faite fut declarée nulle sur le fondement que le droit de visite n'appartenoit point aux Espiciers, à qui on fit mesme des deffenses d'y aller.</p>
<p>2<hi rend="sup">e</hi> Cet Arrest n'est pas rendu avec les Marchands d'Eau de Vie, pas mesme avec les Distillateurs qui estoient pour lors, ainsi ne peut faire de loy, quand principalement il s'agit de faire un Reglement aussi important que celuy en question pour lequel on s'en rapporte toûjours aux titres constitutifs.</p>
<p>3<hi rend="sup">e</hi> Cet Arrest mesme dont les Vinaigriers se veulent prévaloir, &amp; qui ne juge rien, ne pourroit pas subsister quand il leur attribuoit le negoce (que non), parce qu'il se trouveroit contraire aux deux Arrests du Conseil d'Estat qui leur interdisent ce negoce précisement, &amp; qui établissent l'état d'un Corps considerable dans Paris qui en renferme deux.</p>
<p>4<hi rend="sup">e</hi> Que cet Arrest se trouveroit mesme contraire à un autre de la Cour, posterieur de l'année 1675 rendu sur l'appel d'une Sentence du Lieutenant de Police en forme de Reglement du 19 Avril 1662, entre les Distillateurs, les Espiciers, Joüailliers &amp; Merciers, qui reglent les fonctions des uns &amp; des autres, &amp; n'accorde que la simple distillation &amp; fabrication de l'Eau de Vie aux Distillateurs, &amp; non pas le negoce qu'il donne aux Espiciers, Joüailliers &amp; Merciers ; &amp; par consequent, quand on a fait voir que les Vinaigriers n'ont la distillation de l'Eau de Vie que par pure tollerance, ils doivent se renfermer dans leur fonction &amp; suivre la Loy écrite bien précisement dans cet Arrest de Reglement.</p>
<p><hi rend="italic"><hi rend="bold">Réponse au troisième Moyen.</hi></hi></p>
<p>Ces oppositions ne sont considerables par plusieurs raisons.</p>
<p>La premiere, elles ont esté rejetéées, &amp; l'enregistrement fait sans y avoir aucun égard &amp; sans aucune modification.</p>
<p>La seconde, elles n'ont point esté poursuivies dans le temps ; ce ne sont que simples actes signifiez à Monsieur le Procureur General, aneantis, perimez &amp; hors d'état de pouvoir servir.</p>
<p>La troisième, les Vinaigriers n'y concluent mesme point, &amp; ne s'en servent que pour dire qu'ils sont opposans, sans en avoir dit les moyens &amp; sans les dire encore presentement, &amp; par consequent.</p>
<p>La quatrième, que ces oppositions, quand on leur voudroit donner tout l'avantage qu'en peuvent induire les Vinaigriers, elles n'ont <pb n="6"/> pas la force de leur attribuer un droit nouveau du negoce de l'Eau de Vie, pourquoy ils n'ont ny titre ny possession qui l'établisse ; ainsi c'est leur accorder ce qu'ils demandent, lorsqu'on n'empêche point (si la Cour le trouve à propos) que les Vinaigriers ne fassent de l'Eau de Vie &amp; ne debitent icelle conformement à leurs Statuts.</p>
<p><hi rend="italic"><hi rend="bold">Réponse au quatrième Moyen.</hi></hi></p>
<p>C'est mal argumenter sur l'article des Statuts des Marchands d'Eau de Vie, pour en tirer la consequence que les Vinaigriers sont dans le cas de l'exception, ce qui ne peut estre par plusieurs raisons.</p>
<p>La premiere qu'il n'est pas croyable que par des Statuts qui ont esté redigez en consequence d'Arrests du Conseil qui interdisent aux Vinaigriers le negoce, mesme la vente de l'Eau de Vie, on ait entendu leur en conserver le droit qui leur est deffendu, &amp; qu'ils n'ont par aucun titre.</p>
<p>La seconde, qu'il est impossible aux Vinaigriers d'établir qu'ils ayent qualité &amp; qu'ils soient Marchands pour s'attribuer le negoce de l'Eau de Vie au préjudice d'un Corps étably <hi rend="italic">ad hoc</hi>, puisque par leurs Statuts ils ne sont que simples Ouvriers d'Eau de Vie &amp; non Marchands, qu'ils n'ont point de possession paisible qui puisse faire presumer ce droit de negoce contre leur titre rapporté, &amp; par consequent ils ne sont point dans le cas de l'exception qui n'a esté mise dans cet article qu'en faveur des Espiciers, Joüailliers, Merciers, conformément au Reglement de 1675.</p>
<p>La troisième, que par la confrontation du premier art. des Statuts des Marchands, avec l'art. 42 des Statuts des Vinaigriers qui les autorise à faire l'Eau de Vie, on ne trouvera pas qu'il soit exprimé de maniere qui puisse être entendu pour le negoce de cette Eau de Vie, ainsi que l'art. des Statuts des Marchands s'en explique formellement.</p>
<p>La quatrième, &amp; qui est une raison d'équité, consiste en ce qu'on ne presumera pas que S. M. ait érigé un Corps de Marchands pour le negoce de l'Eau de Vie, à qui il a uni le Corps des Distillateurs veritables, afin de ne leur attribuer autre chose que ce que les Vinaigriers veulent avoir, &amp; que ces deux Corps unis ensemble ne renferment pas des privileges &amp; des fonctions separés de celles des Vinaigriers.</p>
<p>Ces reponses établissent assez nettement que jamais le commerce &amp; le negoce de l'Eau de Vie n'a esté attribué aux Vinaigriers, &amp; qu'ils ont tres grand tort de pretendre étendre leur qualité litieuse au delà de ses veritables bornes. Reste à dire deux mots sur leurs deux autres questions.</p>
<p>Pour la question des Fruits confits &amp; Liqueurs, les Vinaigriers <pb n="7"/> sont d'un grand silence, ils ne laissent pas actuellement d'en vendre &amp; debiter, &amp; croyent que la Cour passera doucement sur toutes leurs entreprises ; mais les Marchands d'Eau de Vie ont fait connoître à la Cour qu'ils ne peuvent établir par aucun titre ou possession qu'ils ayent droit d'en vendre, que c'est une entreprise violente &amp; qui a extrêmement fait préjudice auxdits Marchands, parce que depuis plusieurs années, ils remplissent toutes leurs fonctions, ce que la Cour n'autorizera apparament pas, &amp; assurera par un Arrest solemnel l'estat &amp; les fonctions des uns &amp; des autres, &amp; en mesme temps par les dommages &amp; interests proportionnez reparera le tort qu'ils ont receus des Vinaigriers, ce qu'ils esperent de la Justice de la Cour.</p>
<p><hi rend="italic">Monsieur l'Abbé Robert, Rapporteur.</hi></p>
<p>Orry, Proc.</p>
</div>
</body>
</text>
</TEI>