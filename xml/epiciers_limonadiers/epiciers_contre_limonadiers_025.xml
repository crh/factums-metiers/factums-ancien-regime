<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">025. "Memoire pour les maitres distilateurs limonadiers vinaigriers." (1776)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>Arch. Seine, 2ETP/10/2/00, 1776, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>Arch. Seine</repository><idno>2ETP/10/2/00</idno></msIdentifier><head><title>n° 0240 Arch. Seine, 2ETP/10/2/00 n° 6 [1776] [manuscrit]</title><origDate when="1776">1776</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>eau-de-vie — gros et détail</term>
</keywords>
<keywords ana="parties">
<term>limonadiers-vinaigriers contre épiciers</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0240" type="factum">
<head>Arch. Seine, 2ETP/10/2/00 n° 6 [1776] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Memoire pour les maitres distilateurs limonadiers vinaigriers.</hi></p>
<p>Par l'Edit du mois d'août dernier, les maitres limonadiers et les maitres vinaigriers ont été reunis en un même corps de communauté.</p>
<p>Et par l'article 1<hi rend="sup">er</hi> de cet Edit, le Roi en créant et érigeant de nouveau en tant que de besoin six corps de marchands, et quarante quatre communautés d'arts et metiers, veut que lesdits corps et communautés jouissent exclusivement à tous autres du droit et faculté d'exercer les commerces, metiers et professions qui leur sont attribués et denommés en l'etat arrêté au Conseil de Sa Majesté.</p>
<p>Suivant cet état annexé à l'Edit, les droits attribués aux limonadiers et aux vinaigriers consistent :</p>
<p>1° dans la profession de confiseur en concurrence avec l'epicier et le patissier,</p>
<p>2° dans le commerce d'eau de vie et de liqueur en gros et en détail, en concurrence pour la vente en gros avec l'epicier,</p>
<p>3° dans le détail de la bierre en concurrence avec les brasseurs, et les cidres exclusivement, ainsi que le droit de servir et donner à boire dans leurs boutiques l'eau de vie et les liqueurs.</p>
<p>Non seulement ce droit exclusif leur est attribué par l'etat annexé à l'Edit, mais l'art. 45 de l'Edit même en renferme encore une disposition très expresse ; car en supprimant les lettres domaniales ci-devant accordées pour la vente en regrat de la marchandise de la fruiterie, de la bierre, de l'eau de vie et autres menues marchandises, le législateur veut pour cet article que lesdites marchandises en regrat soient vendues librement, à l'exception neantmoins de la bierre, du cidre, de l'eau de vie, dont la vente en boutique appartiendra, savoir celle de la bierre aux limonadiers et vinaigriers en concurrence avec les brasseurs, le cidre et l'eau de vie auxdits limonadiers et vinaigriers exclusivement.</p>
<p>Ce droit exclusif de vendre le cidre, de servir et donner à boire dans leurs boutiques l'eau de vie et les liqueurs ne dédommage pas à beaucoup près les limonadiers de la vente du caffé brulé en grain et en poudre attribué par le même etat annexé aux epiciers, <pb n="2"/> en concurrence avec les limonadiers, ni de la réunion des vinaigriers à eux.</p>
<p>Cependant les limonadiers et les vinaigriers apprennent que les epiciers se donnent des mouvements pour leur enlever le droit exclusif qui leur est si expressement attribué tant par l'etat annexé à l'Edit que par les articles 1<hi rend="sup">er</hi> et 45 de l'Edit même, et pour partager avec eux le droit de servir et donner à boire dans leurs boutiques l'eau de vie et les liqueurs.</p>
<p>S'ils reussissoient dans leur projet, les limonadiers n'auroient aucun dédommagement du droit exclusif qui leur appartenoit avant la suppression des maitrises, tant pour la vente des liqueurs à petite mesure que pour la vente du caffé brulé en grain et en poudre, vente qui, par rapport aux liqueurs, étoit si severement deffendue aux epiciers qu'il ne leur étoit pas même permis d'en avoir dans des vaisseaux en vuidange, soit dans leurs boutiques, soit dans leur arrieres boutiques, et qu'ils ne pouvoient en tenir dans leurs arrieres boutiques comme dans leurs boutiques autrement qu'en bouteilles entieres, bouchées d'un bouchon de liège coeffé d'un morceau de parchemin et ficelées. Mrs les vinaigriers observent aussi que leur perte est très considerable relativement à ce que l'on donne aux epiciers le commerce indéfiny du vinaigre, qui fait un objet considerable pour la partie de leur commerce et qu'il n'y a que la vente de la bierre et du cidre qui puisse les indemniser en partie de cette perte, et qu'avant l'Edit les epiciers n'avoient droit que d'avoir 30 pintes de vinaigre chez eux, et ne pouvoient point en faire venir d'Orléans ny d'autres provinces, au lieu qu'à présent ils en font venir des quantités considerables.</p>
<p>Ainsi par leur demarche, dont les limonadiers et les vinaigriers ont été informés, les epiciers entreprennent de violer tout à la fois tous les règlemens qui subsistoient avant la suppression des maitrises, et ceux mêmes qui y ont été substitués, tant par l'Edit du mois d'août que par l'etat arrêté au Conseil du Roi et annexé à cet Edit.</p>
<p>Mais les limonadiers et les vinaigriers osent esperer que leurs prétentions seront rejettées et que Sa Majesté voudra bien ordonner que son Edit du mois d'août dernier sera exécuté selon sa forme et sa teneur.</p>
<p>Il en est de même des mouvemens que les limonadiers et les vinaigriers apprennent encore que se donnent les fermiers du Roi pour faire réformer l'article 45 du même Edit, dont on a déja parlé, et par lequel Sa Majesté a supprimé les lettres domaniales voulant que les menues marchandises dont la vente en regrat étoit permise par ces <pb n="3"/> lettres soient vendues librement, à l'exception néantmoins de la bierre, du cidre et de l'eau de vie, dont cet article porte que la vente en boutique appartiendra, savoir celle de la bierre aux limonadiers et vinaigriers en concurrence avec les brasseurs, et celle du cidre et de l'eau de vie auxdits limonadiers et vinaigriers exclusivement. La démarche des fermiers pour faire reformer la disposition de</p>
<p>cet article est d'autant plus déplacée que Sa Majesté par ce même article déclare expressement qu'elle se reserve de pourvoir à cet egard à l'indemnité de qui il appartiendra, et l'on voit bien d'ailleurs qu'en laissant subsister ces lettres domaniales qu'il est si facile d'acquerir et à si peu de frais, il ne seroit pas juste de priver les limonadiers, dont les maitrises ont couté des sommes considerables, d'un droit exclusif qui n'est qu'un foible dédommagement de tous ceux qui leur étoient attribués par les reglements qui subsistoient avant la suppression des maitrises, et de la pluspart desquels ils ont été privés par l'Edit du mois d'août dernier.</p>
<p>Ainsi, sur ce second objet, de même que sur le premier, les limonadiers et les vinaigriers esperent que Sa Majesté aura la bonté d'ordonner l'execution de son Edit.</p>
</div>
</body>
</text>
</TEI>