<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">002. "Pour les Maistres &amp; Gardes Lapidaires, demandeurs. Contre les Maîtres &amp; Gardes Orfèvres, deffendeurs." (1666)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Ms français 17652 (Fol.77), 1666, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Nouv. acq. française 2450 (Fol.84)</idno></msIdentifier><head><title>n° 0220 BNF, Ms français 17652 (Fol.77) [1666] [imprimé]</title><origDate when="1666">1666</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>conflit de juridictions</term>
</keywords>
<keywords ana="parties">
<term>lapidaires contre orfèvres</term>
</keywords>
<keywords ana="juridiction">
<term>parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0220" type="factum">
<head>BNF, Ms français 17652 (Fol.77) [1666] [imprimé]</head>
<p><pb n="1"/> <hi rend="italic">Plaise à Monseigneur [blanc] Conseiller du Roy en ses Conseils, Maître des Requestes ordinaire de son Hostel, avoir pour recommandé en Justice le bon droict,</hi></p>
<p><hi rend="bold">Pour les Maistres &amp; Gardes Lapidaires, demandeurs.</hi></p>
<p><hi rend="italic"><hi rend="bold">Contre les Maîtres &amp; Gardes Orfèvres, deffendeurs.</hi></hi></p>
<p>Il s'agist d'un Reglement de Juges en la grande Chambre du Parlement de Paris &amp; celle de l'Edict.</p>
<p>Les Lapidaires demandent la grande Chambre, et les Orfèvres celle de l'Edict.</p>
<p>Les raisons des Lapidaires sont que les Orfèvres plaidans en la grande Chambre avec Caillon particulier Lapidaire, &amp; la cause y estant attachée par plusieurs demandes desdits Orfèvres, sur lesquelles les parties ont esté reglées par Arrest contradictoire du 2 Mars 1666, &amp; s'agissant de faire cesser le trouble que lesdits Orfèvres donnent indeuëment aux Lapidaires en voulant leur oster la faculté de vendre les pierreries mises en œuvre qui leur appartient par leurs Statuts, &amp; en laquelle ils ont esté maintenus par plusieurs Arrests du Conseil &amp; du Parlement de Paris, il s'agit de faire un Reglement enntre lesdites Communautez ; les Maistres &amp; Gardes Lapidaires sont intervenus pour empescher que cette mauvaise pretention desdits Orfèvres ne fust jugée sans eux.</p>
<p>Et comme ils ont veu apres cette intervention que lesdits Orfèvres se servoient d'un Arrest de la Chambre de l'Edict &amp; en demandoient l'execution en ladite grande Chambre, que les parties sur cette demande estoient reglées &amp; joint, ils ont pour respect de cet Arrest insoustenable obtenu incidemment Requeste civile.</p>
<p>Et delà les Orfèvres ont pris pretexte de faire retenir à l'Edict la cause sur laquelle les parties estoient reglées en la grande Chambre, &amp; ont par ce moyen forcé les Lapidaires d'obtenir le Reglement de Juges.</p>
<p><pb n="2"/> Les lapidaires, pour estre renvoyez en la grande Chambre, disent que la cause a esté portée en la grande Chambre par les Orfèvres mesmes, &amp; que par ledit Arrest du 2 Mars 1666 donné en la poursuitte desdits Orfèvres, &amp; contradictoirement entr'eux &amp; ledit Caillon auparavant l'intervention desdits Lapidaires, il a esté ordonné que les parties auroient audiance sur la demande des Orfèvres afin d'execution dudit Arrest de l'Edict.</p>
<p>Que l'intervention desdits Maistres &amp; Jurez Lapidaires &amp; leur demande en Requeste civille sont incidentes &amp; posterieures à cet Arrest du 2 Mars 1666, ainsi elles doivent necessairement suivre le principal.</p>
<p>Que comme la demande des Orfèvres tend à faire executer ledit Arrest de l'Edict, &amp; comme ils l'alleguent comme un moyen, la deffense des Lapidaires opposée à cette demande, &amp; la Requeste civille qu'ils opposent pour moyen, ne fait qu'un seul &amp; mesme pas ; et puisque la demande est attachée à la grande Chambre, il faut que la deffense y demeure, parce qu'il seroit ridicule que les Orfèvres voulussent faire demeurer leur demande en la grande Chambre &amp; neantmoins porter la deffense des Lapidaires à l'Edict.</p>
<p>En troisiesme lieu, qu'il s'agit au fonds d'un Reglement à faire entre les deux Communautez, dont la Chambre de l'Edict n'est point competante, mais seulement la grande Chambre.</p>
<p><hi rend="italic">Quartò</hi>, la question se devant decider par l'inspection des Statuts &amp; des Arrests de verification intervenus en la grande Chambre, il n'appartient point à l'Edict d'en connoistre, les Statuts ne luy ayans point esté addressez.</p>
<p><hi rend="italic">Quintò</hi>, l'Edict n'a jamais esté saisi d'aucune demande d'entre les parties sur la matiere dont est question, &amp; il n'y a personne des concluans qui puisse fonder la Jurisdiction de l'Edict, le privilege de la Religion pretenduë Reformée ne pouvant estre appliqué aux Communautez collectives.</p>
<p><hi rend="italic">Sextò</hi>, par plusieurs Arrest contradictoires du Conseil produits par les Lapidaires, il a esté ordonné que tous les differends meus &amp; à mouvoir concernant les Reglemens desdites Communautez seroient traittez en premiere instance au Chastelet, &amp; par appel au Parlement, c'est à dire en la grande Chambre.</p>
<p><hi rend="italic">Septimò</hi>, les Lapidaires ont encore produit plusieurs Arrests donnez en la grande Chambre dudit Parlement entre les Orfèvres &amp; les Communautez des Fourbisseurs, Horlogeurs &amp; Graveurs, par lesquels sur le fondement des Statuts &amp; de leur addresse, &amp; des Arrests de verification &amp; des Arrests du Conseil portans renvoy, lesdits Orfèvres ont plaidé en la grande Chambre &amp; perdu leur procez.</p>
<p>Lesdits Orfèvres font deux objections frivoles :</p>
<p>L'une qu'il n'y a point de conflict entre le Parlement &amp; la Chambre de l'Edict, &amp; consequemment qu'il faut renvoyer au Parquet.</p>
<p>Cette supposition est si contraire à la verité qu'elle se détruit d'elle-mesme, &amp; n'est pas besoin d'autre contredit.</p>
<p>La seconde, que s'agissant d'une Requeste civille contre un Arrest de l'Edict, il faut qu'elle soit plaidée en la mesme Chambre.</p>
<p>A quoy les Lapidaires répondent que cette Requeste civille estant un incident, &amp; pour mieux dire un moyen servant dans l'instance pendante en la grande Chambre, &amp; qui y a esté portée par les Orfèvres, cet incident est inseparable du principal, que cela est un usage trivial, exempt de contredit, &amp; que depuis peu le Conseil l'a ainsi jugé à l'égard d'une Requeste civille contre un Arrest de l'Edict de Castres, obtenuë incidemment dans un procez pendant à Paris, laquelle le Conseil a renvoyé à Paris &amp; le juge ainsi tous les jours.</p>
<p>Et si l'Edict devant estre consideré comme une Chambre du Parlement, ainsi que lesdits Orfèvres le supposent, il est encore des regles que les Requestes civilles soient plaidées en la grande Chambre &amp; qu'elles y demeurent jusques à l'appointement.</p>
<p>A quoy lesdits Lapidaires joignent que l'Arrest de l'Edict n'est pas donné avec eux, &amp; une infinité de moyens qui prouvent qu'il est nul.</p>
<p><hi rend="italic">Monsieur de Novion, Rapporteur.</hi></p>
<p>Boctois, Advocat.</p>
</div>
</body>
</text>
</TEI>