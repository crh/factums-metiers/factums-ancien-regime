<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">009. "Factum pour les Marchands Maistres Lapidaires Joüailliers de la Ville de Paris. Contre les Marchands Orfèvres de la mesme Ville." (1672)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Ms français 21795 (Fol.405), 1672, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>BNF</repository><idno>Nouv. acq. française 2450 (Fol.153)</idno></msIdentifier><head><title>n° 0489 BNF, Ms français 21795 (Fol.405) [1672] [imprimé]</title><origDate when="1670">1670</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>diamants — enchâssement des pierres précieuses — vente de l'objet fini</term>
</keywords>
<keywords ana="parties">
<term>lapidaires contre orfèvres</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0489" type="factum">
<head>BNF, Ms français 21795 (Fol.405) [1672] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Factum pour les Marchands Maistres Lapidaires Joüailliers de la Ville de Paris.</hi></p>
<p><hi rend="italic"><hi rend="bold">Contre les Marchands Orfèvres de la mesme Ville.</hi></hi></p>
<p>Deux professions qui ne sont employées qu'à travailler sur les Chef-d'œuvres de la nature, c'est à dire sur les Pierreries &amp; sur les plus precieux des Metaux, attendent de la Justice du Conseil un Reglement sur le sujet de leurs Ouvrages.</p>
<p>Les Orfèvres pretendent qu'il n'appartient qu'à eux seuls d'employer de l'or &amp; de l'argent, &amp; que les Lapidaires ne peuvent pas le faire pour mettre leurs Pierreries en œuvre, qu'ils ne peuvent que les tailler &amp; polir, mais qu'aussitost après ils sont obligez de les leur remettre pour les garnir suivant les differens usages auxquels elles sont destinées.</p>
<p>Les Lapidaires soûtiennent au contraire que leur profession seroit imparfaite, &amp; le public mal servy, si pour des ouvrages de cette qualité, où il entre si peu d'or &amp; d'argent, il falloit que deux Ouvriers differens fussent employez, &amp; qu'il leur doit estre permis de faire la Joüaillerie comme il est permis aux Orfèvres de faire les grands ouvrages d'Orfèvrerie.</p>
<p>Ce sont les differentes conclusions des parties, dont le Conseil, par un mouvement de sa Justice accoûtumée, &amp; par la raison de l'importance du Reglement, s'est reservé la connoissance par Arrest contradictoire du 13 Février 1671, nonobstant la resistance des Orfèvres.</p>
<p>Les Lapidaires établissent leurs conclusions <hi rend="italic">sur le bien public</hi>, <hi rend="italic">les prejugez du Conseil</hi>, &amp; <hi rend="italic">l'ancien usage</hi>.</p>
<p>C'est le bien public qui est l'arbitre des differens qui surviennent entre les Corps et Communautez pour raison de leurs ouvrages, <pb n="2"/> &amp; comme il doit prevaloir aux interests des particuliers, il doit aussi estre le premier consulté sur les Reglemens generaux qui sont à faire en ces sortes d'occasions.</p>
<p><hi rend="bold">Interest Public.</hi></p>
<p>Voicy donc l'interest que le public peut avoir en la cause, &amp; qui la decide en faveur des Lapidaires.</p>
<p>1° Il est plus commode &amp; plus advantageux à ceux qui ont des pierreries à faire tailler &amp; à mettre en œuvre de pouvoir faire faire l'un &amp; l'autre par les Lapidaires, que d'estre obligez, après que le Lapidaire les aura taillé &amp; poly, de les porter à un Orfèvre pour les mettre en œuvre, parce que plus il y a d'Ouvriers &amp; de mains employez à un travail, il y a aussi plus de risque à essuyer, plus de temps à attendre, &amp; de plus de frais à faire, outre qu'il est d'une bonne police que les Marchandises &amp; les Ouvrages soient vendus aux particuliers par la premiere main.</p>
<p>C'est par ces considerations que les Horlogers peuvent faire les boëtes de leurs Montres, les Fourbisseurs les Gardes de leurs Espées, les Esperonniers les Estriers &amp; les Esperons, quoyque tout cela soit d'or ou d'argent.</p>
<p>Par cette mesme raison de la commodité publique, il a esté permis aux Chapeliers de garnir leurs Chapeaux, aux Gantiers de parfumer leurs gants &amp; d'y mettre des rubans &amp; des franges, aux Ferreurs d'Esguillettes de ferrer leurs esguillettes d'or &amp; d'argent ; les Carrossiers garnissent leurs Carrosses &amp; y mettent des cloux &amp; des plaques de cuivre, quoyque les Tapissiers &amp; les Fondeurs ayent pretendu de le faire ; les Charrons mettent &amp; forgent des bandes de fer aux rouës de leurs carrosses &amp; de leurs charrettes ; les Tailleurs font aujourd'huy eux seuls ce que les Chaussetiers, Pourpointiers &amp; Drappiers faisoient autrefois ; en un mot, chaque ouvrier a obtenu la liberté d'achever son ouvrage.</p>
<p>2° Si les Lapidaires ne pouvoient garnir les Pierreries ny les vendre après qu'elles sont garnies, le commere en demeureroit tout entier aux Orfèvres, au prejudice du public, car ils seroient les seuls qui les acheteroient des Lapidaires, parce qu'ils ne voudroient pas mettre en œuvre celles qui leur seroient apportées par les particuliers. Ils seroient encore les seuls qui pourroient acheter aux Inventaires, &amp; les Lapidaires en seroient exclus, puisqu'ils ne pourroient vendre ce qu'ils y auroient achepté ; &amp; ainsi <pb n="3"/> il se formera indubitablement un monopole pour les Pierreries.</p>
<p>3° L'émulation a toûjours esté la veritable cause de la perfection des ouvrages. Si la faculté de mettre les Pierreries en œuvre estoit accordée aux Orfèvres à l'exclusion des Lapidaires, il n'y auroit plus d'émulation entr'eux ; &amp; cependant c'est par ce moyen que les Lapidaires ont porté l'art de garnir les Pierreries jusques à une si grande perfection, que mesme des païs où elles se forment, on les envoye en France pour y estre mises en œuvre ; &amp; ces nations si éloignées que la nature a rendu comme les depositaires de ses plus grandes richesses ont recours aux François pour achever ce qu'elle a commencé.</p>
<p>Les Orfèvres taschent à mettre aussi l'interest public de leur costé, car ils pretendent qu'il n'y a point de mains plus fidelles que les leurs pour manier l'or &amp; l'argent, &amp; que de laisser la disposition de ces riches metaux à plusieurs sortes de professions, c'est exposer le public à de grandes tromperies.</p>
<p>Mais quel peut estre le sujet de soupçonner la fidelité d'un Lapidaire dans l'employ de quelques onces d'or ou d'argent qu'il fournit pour metttre des diamans en œuvre, tandis qu'on luy confie la vente &amp; la taille de ces mesmes diamans, qui sont pour l'ordinaire d'un prix si au dessus de la matiere dans lequelle ils sont enchassez qu'elle n'entre pas mesme dans son estimation.</p>
<p><hi rend="bold">Prejugez.</hi></p>
<p>Par Arrest du Conseil contradictoirement rendu entre les Orfèvres &amp; les Horlogers de Paris, le 8 May 1643, les Horlogers sont maintenus au pouvoir &amp; faculté de faire, vendre &amp; debiter toutes sortes de boëtes d'or &amp; d'argent, émaillées &amp; gravées, pour leurs Montres &amp; Horloges.</p>
<p>Par autre Arrest aussi contradictoirement rendu entre les mesmes parties le 11 Septembre 1671, le precedent a esté confirmé.</p>
<p>La mesme chose a esté jugée à l'égard des Fourbisseurs, des Graveurs, des Cousteliers, des Esperonniers &amp; des Lunetiers, &amp; il leur a esté permis d'employer de l'or &amp; de l'argent à faire les Gardes des Espées les Cahets, les Manches des Cousteaux, les Estriers, les Esperons, les Mords &amp; les Cercles de Lunettes.</p>
<p>La raison de ces Prejugez est que l'or ou l'argent n'est pas proprement le fonds de tous ces Ouvrages ; il n'en est que l'accessoire, <pb n="4"/> &amp; ce n'est presque que par occasion &amp; par accident qu'il y est employé.</p>
<p>Cette raison doit avoir bien plus de lieu à l'égard des Lapidaires ; car au lieu que la Boëte d'or ou d'argent d'une Montre vaut presque toûjours plus que le mouvement, &amp; que les Lames des Espées &amp; des Cousteaux n'égalent jamais le prix de leurs Gardes ou de leurs Manches, que la graveure d'un Cachet d'or, &amp; moins encore les verres des Lunettes d'argent n'approchent point de ce que les Cachets &amp; les Lunettes valent.</p>
<p>Tout au contraire, dans les Ouvrages de Joüaillerie, l'or ou l'argent qui y est employé n'a aucune proportion de valeur avec les Pierreries.</p>
<p>D'ailleurs le mouvement d'une montre a bien moins de rapport avec sa boëtte, la garde avec l'épée, la graveure avec le cachet, la lame du cousteau avec le manche, que la Pierrerie avec l'or &amp; l'argent dans lequel elle est enchassée ; &amp; l'addresse de l'Ouvrier est bien moins necessaire à adjuster tous ces ouvrages qu'à enchasser une Pierrerie ; car c'est en quoy consiste en partie la beauté du Joyau, &amp; c'est ce qui en releve particulierement l'éclat que d'estre bien mis en œuvre.</p>
<p><hi rend="bold">La Possession et l'Usage.</hi></p>
<p>Ce sera bien assez de remonter jusques au temps de Saint Loüis pour faire voir que les Lapidaires ont toûjours esté en la possession de mettre leurs Pierreries en œuvre.</p>
<p>Les plus anciens Statuts que l'on trouve des Lapidaires (qui portoient alors le nom de Cristaliers) sont de l'année 1290.</p>
<p>Il y a plusieurs articles qui justifient cette possession autant que la simplicité du siecle le pouvoit permettre.</p>
<p>Mais il suffit de dire qu'ils y sont déchargez de tous peages &amp; droits qui se levoient alors à la vente ou à l'achapt, &amp; la raison de ce privilege est conceu en ces termes : <hi rend="italic">c'est que leur Mestier n'appartient fors à la honorance de la Saint Eglise &amp; des Hauts Hommes</hi>.</p>
<p>Ils travailloient donc en ce temps aux Joyaux &amp; aux Pierreries, dont les Autels &amp; les personnes de grande qualité estoient ornées.</p>
<p>En 1331, quelques particuliers ayant voulu imiter les Pierreries <pb n="5"/> naturelles, ces mesmes Cristalliers s'y opposerent, &amp; après plusieurs formalitez, firent deffendre l'usage de ces fausses Pierreries.</p>
<p>S'ils n'avoient eü que la taille &amp; le poliment des pierres precieuses, comme les Orfèvres le supposent, &amp; qu'ils n'eussent pas eü le pouvoir de les mettre en œuvre &amp; de faire de la Joüaillerie, il leur eust esté fort indifferent que d'autres les eussent imitez, &amp; les Orfèvres auroient pris ce soin.</p>
<p>En 1549, ils furent compris sous leur nom de Lapidaires dans l'Ordonnance qui fut faite au sujet des Ouvriers qui employoient de l'or &amp; de l'argent ; comme les autres ils sont soûmis à la Cour des Monnoyes, &amp; depuis ils ont toûjours esté nommez dans les Ordonnances &amp; les Reglemens generaux qui ont esté faits au sujet desdits Ouvriers.</p>
<p>Ce seroit une étrange erreur, &amp; dont les compilateurs de ces Ordonnances ne peuvent pas estre crus capables, si l'on avoit meslé de simples Tailleurs &amp; Pollisseurs de pierres precieuses avec des Orfèvres &amp; autres Ouvriers en or &amp; en argent, &amp; qu'ils eussent esté renvoyez à la Cour des Monnoyes s'ils n'avoient pas travaillé sur le fin.</p>
<p>En 1583, l'usage des Pierreries estant beaucoup changé, ils obtinrent du Roy de nouveaux Statuts, dans l'article 19 desquels il est porté que les pierres brutes &amp; taillées ne seront venduës que par les Lapidaires, Joüailliers &amp; Orfèvres. Ce mot de " pierres taillées " s'entend des pierres garnies &amp; mises en œuvres ; car encore que pour ce qui est du travail, il y ait une difference entre la taille &amp; la garniture, neantmoins à l'égard de la vente, c'est la mesme chose, n'estant pas fort ordinaire que l'on achette des pierres taillées &amp; qu'ensuite on les fasse garnir ; tout au contraire ceux qui achettent des Joyaux, ou les achettent brutes pour les faire tailler &amp; garnir à leur fantaisie, ou les achettent tout taillez &amp; tout garnis.</p>
<p>Le 20<hi rend="sup">e</hi> article explique plus nettement cet usage ; car il y est dit que sans l'adveu des Jurez, aucunes Bagues ou Pierreries ne seront portées sous cappe, d'où il s'ensuit que les Lapidaires avoient droit dès ce temps-là de vendre les Pierreries mises en œuvre, le mot de " Bagues " estant alors un terme general qui s'appliquoit à toute sorte de Joyaux.</p>
<p>En 1613, sur la Requeste des Lapidaires, deffenses furent faites <pb n="6"/> aux Marchands Forains d'apporter hors du temps des Foires des Pierreries taillées &amp; façonnées ; et il est ordonné que quand elles seront exposées en vente en autre temps, elles seroient visitées par les Jurez Lapidaires, d'où il resulte qu'ils estoient alors en possession de vendre les Pierreries taillées &amp; facçonnées, c'est à dire taillées &amp; mises en œuvre.</p>
<p>En 1614, par Arrest contradictoire du Conseil, le commerce des Pierreries fut declaré commun entre les Orfèvres &amp; les Lapidaires.</p>
<p>En 1615, les Orfevres ayant fait arrester prisonnier, d'auctorité de la Cour des Monnoyes, le nommé Bacler Lapidaire, les Lapidaires prirent son fait &amp; cause, &amp; demanderent le renvoy de la cause au Parlement, comme s'agissant d'un fait de police, sur quoy conflict, &amp; ensuitte instance en reglement de Juges au Conseil, sur laquelle intervint Arrest contradictoire par lequel les parties furent renvoyées au Prevost de Paris en premiere instance, &amp; par appel au Parlement, pour les fautes qui se commettoient en leurs Manufactures &amp; Ouvrages ; mais qu'en tout ce qui concerneroit le fin, l'alliage &amp; la bonté des metaux, les parties se pourvoiroient en la Cour des Monnoyes. Les Orfèvres oseroient-ils dire après cet Arrest que les Lapidaires ne missent pas en œuvre les Pierreries, &amp; qu'ils n'y employassent pas de l'or &amp; de l'argent, puisqu'ils sont renvoyez sur ce sujet en la Cour des Monnoyes ?</p>
<p>Mais l'on passe plus avant, car on soûtient que les anciens Statuts des Orfèvres ne parlent pas mesme des Pierreries &amp; de la façon de les mettre en œuvre ; ils ont esté sommez plusieurs fois de les representer pour ce sujet, mais ils en ont toûjours fait un secret.</p>
<p>Ils ont opposé seulement que, dans quelques escritures fournies sous le nom des Lapidaires, ils sont demeurez d'accord de laisser mettre les Pierreries en œuvre par les Orfèvres, &amp; de s'en reserver seulement la Taille &amp; le Poliment.</p>
<p>Mais on leur a répondu que ces Escritures ne sont point signées des Jurez qui estoient alors en charge, qu'elles n'ont point esté faites par déliberation de la Communauté, que l'Usage a toûjours esté contraire, que d'ailleurs la prudence Politique ne traçoit aucun Reglement aux Communautez sur leurs consentemens, ny mesme sur leurs interests particuliers, mais sur le bien seul du public, que c'estoit par cette raison que les Causes des Communautez <pb n="7"/> estoient toutes publiques, &amp; que dans les Parlemens les Procureurs generaux de Sa Majesté avoient droict d'y prendre des Conclusions, que les Communautez sont toûjours presumées Mineures, et que par cette raison les consentemens que ceux qui les conduisent ont donné à leur prejudice, ou par ignorance, ou par infidelité, ne sont jamais d'aucune consideration.</p>
<p>A quoy l'on adjoûte aujourd'huy que la cause ayant esté reservée au Conseil, nonobstant ces pretendus consentemens, ils n'ont point esté considerez par le Conseil, &amp; ils le seront bien moins à present qu'il plaira à Sa Majesté prendre une entiere connoissance de la contestation principale.</p>
</div>
</body>
</text>
</TEI>