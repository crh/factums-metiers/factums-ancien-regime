<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">013. "Mémoire pour les Gardes des Marchands-Orfèvres-Joyailliers à Paris, Demandeurs &amp; Défendeurs. Contre Etienne Domergue, Louis Goupy, Jean Masoyer dit Charmoy &amp; Charles Tulmay, Maîtres Lapidaires, Défendeurs. Et contre les Jurez de la Communauté des Maîtres Lapidaires, Intervenans." (1735)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>BNF, Ms français 1426 (Fol.186), 1735, imprimé</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>A.N.</repository><idno>K 1045</idno></msIdentifier><head><title>n° 0165 BNF, Ms français 1426 (Fol.186) [1735] [imprimé]</title><origDate when="1730">1730</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>diamants — enchâssement des pierres précieuses — vente de l'objet fini</term>
</keywords>
<keywords ana="parties">
<term>orfèvres contre lapidaires</term>
</keywords>
<keywords ana="juridiction">
<term>parlement</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0165" type="factum">
<head>BNF, Ms français 1426 (Fol.186) [1735] [imprimé]</head>
<p><pb n="1"/> <hi rend="bold">Mémoire pour les Gardes des Marchands-Orfèvres-Joyailliers à Paris, Demandeurs &amp; Défendeurs.</hi></p>
<p><hi rend="italic"><hi rend="bold">Contre Etienne Domergue, Louis Goupy, Jean Masoyer dit Charmoy &amp; Charles Tulmay, Maîtres Lapidaires, Défendeurs.</hi></hi></p>
<p><hi rend="italic"><hi rend="bold">Et contre les Jurez de la Communauté des Maîtres Lapidaires, Intervenans.</hi></hi></p>
<p>Les Lapidaires, dans un Mémoire imprimé qu'ils viennent de répandre, s'efforcent de se faire accorder la provision sur les appointez à mettre dont il s'agit, par differentes suppositions.</p>
<p><hi rend="italic">Leurs prétenduës raisons sont :</hi></p>
<p>1° Le droit commun.</p>
<p><pb n="2"/> 2° Les titres.</p>
<p>3° Leur ancienneté qu'ils prétendent tirer de 1290.</p>
<p>Il ne sera pas difficile aux Marchands Orfèvres de détruire ces prétendus moyens par une simple observation qui sera suivie du parallele des titres rapportez par les Parties.</p>
<p>Il faut commencer par établir ce que c'est que le droit commun par rapport aux Ouvriers &amp; aux Marchands.</p>
<p>Il n'y a que les Marchands qui puissent vendre la marchandise qu'ils n'ont pas faite ; l'Ouvrier au contraire ne peut vendre que ce qu'il a travaillé.</p>
<p>Les Orfèvres sont Marchands &amp; sont un des six Corps des Marchands dans Paris.</p>
<p>Le Lapidaire n'est qu'un Ouvrier &amp; un Artisan, &amp; ainsi il ne peut vendre que ce qu'il a droit de faire.</p>
<p>Examinons à présent s'il a droit de monter les Pierreries &amp; de les mettre en œuvre, &amp; à qui ce droit appartient.</p>
<p>Le droit commun, pour en revenir encore à ce point, le donne à l'Orfèvre privativement à tout autre parce que le seul Orfèvre est Ouvrier en or &amp; en argent, mais les titres sont bien plus précis.</p>
<p>Les Statuts des Orfèvres de l'année 1345 aux articles 4, 5, 6, 7, 8, 9, 10, 11 ne parlent que de la maniere de monter &amp; mettre en œuvre<note><p>Page 4 du Recueil.</p></note>.</p>
<p>L'article 10 prouve que les Orfèvres avoient la taille des Pierres.</p>
<p>Les Statuts des Lapidaies au contraire prouvent que les Lapidaires ne sont que des Tailleurs de Pierres, que leur profession y est bornée, il n'est pas dit un mot de la monture<note><p>Page 584 du Recueil.</p></note>.</p>
<p>L'article premier designe leur qualité : <hi rend="italic">Tailleurs, Graveurs, Œuvrans en toutes sortes de Pierres fines &amp; naturelles</hi>.</p>
<p><pb n="3"/> L'article 12 designe leurs Outils, qui <hi rend="italic">sont des Rouës &amp; des Moulins</hi>.</p>
<p>Après ces Statuts, les Lapidaires prétendirent vendre les Pierreries montées, ils ne prétendoient pas les monter. On voit par un extrait de leurs écritures produit en l'Instance qu'ils reconnoissoient que la monture appartenoit à l'Orfèvre.</p>
<p>Sur cette contestation qui n'étoit qu'entre les Lapidaires &amp; les Merciers, ils avoient obtenu Sentence au Châtelet le 18 Juin 1614, conforme à leurs prétentions.</p>
<p>Mais sur l'appel des Orfèvres, Arrêt contradictoire en la Cour du 6 Septembre 1631, qui <hi rend="italic">fait défenses aux Lapidaires de vendre aucunes Pierreries montées &amp; mises en œuvre</hi>. Cet Arrêt est conforme, comme la Cour voit, aux titres &amp; au droit commun<note><p>Page 625 du Recueil.</p></note>.</p>
<p>Les Lapidaires, voulant s'attribuer dans la suite la monture des Pierres, surprirent au Conseil du roi un Arrêt en 1670 portant qu'ils seroient reçûs en la Cour des Monnoyes.</p>
<p>Sur l'opposition des Orfèvres à cet Arrêt, les Lapidaires demanderent d'être maintenus dans la possession de monter la Pierre. Ils se pourvurent en cassation contre l'Arrêt de 1631, contre lequel ils avoient déja obtenu une Requête Civile.</p>
<p>Le vû de cet Arrêt fait connoître qu'ils n'oublierent rien pour leur défense.</p>
<p>Cependant par Arrêt du 28 Janvier 1673, il <hi rend="italic">leur fut fait défense &amp; à tous autres qu'aux Orfèvres de monter &amp; garnir les Pierres, à peine de 3 000 liv. d'amende </hi>; l'exécution de l'Arrêt de 1631 fut ordonnée, sauf à eux à poursuivre en la Cour le Jugement de la Requête Civile<note><p>Page 639 du Recueil.</p></note>.</p>
<p>Tels sont les titres des Orfèvres. Quels sont ceux qu'opposent les Lapidaires ?</p>
<p><pb n="4"/> 1° Leurs Statuts, article 20, où, disent-ils, il est défendu à tous autres qu'à eux de vendre des Bagues.</p>
<p>Cet article ne s'exprime point ainsi ; il dit seulement que sans l'aveu des Jurez on ne pourra colporter sous le manteau aucunes Bagues, Pierreries, etc.</p>
<p>Mais ils ne peuvent tirer aucun avantage de cet article parce que ces Statuts ayant été surpris, les Orfèvres avoient obtenu au Conseil des Lettres Patentes du roi, de 1586, qui les révoquoient<note><p>Page 615 du Recueil.</p></note>.</p>
<p>L'opposition des Lapidaires à ces Lettres a fait une partie des contestations jugées par l'Arrêt du Parlement de 1631, qui a reglé les droits des Parties, qui a donné aux Lapidaires le privilege de tailler seulement &amp; aux Orfèvres le droit de vendre, à l'exclusion des Lapidaires, les Pierreries montées.</p>
<p>2° La Sentence du 18 Juin 1614.</p>
<p>Cette Sentence est celle qui est infirmée par l'Arrêt du 6 Septembre 1631 ; si l'appel suspend l'execution d'une Sentence &amp; empêche qu'elle ne puisse passer pour un titre tant que l'appel n'est pas jugé, à plus forte raison ne peut-elle en faire un lorsqu'elle est anéantie.</p>
<p>Une pareille citation mérite reprehension.</p>
<p>3° Une Sentence du 9 Janvier 1666.</p>
<p>Elle n'est pas rapportée, mais d'ailleurs l'on soutient qu'elle ne prononce pas comme les Lapidaires le disent, puisque Caillon, au profit duquel elle étoit renduë, en interjetta appel, &amp; sur l'appel demandoit mainlevée provisoire ; il n'auroit pas eu besoin de la demander si la Sentence l'avoit prononcée.</p>
<p>4° L'Arrêt obtenu par Paul Caillon le 2 Mars 1666, qui en fait mainlevée provisoire.</p>
<p>Cet Arrêt n'est que sur le provisoire, mais d'ailleurs il est anterieur à l'Arrêt contradictoire de 1673, sur le fonds, <pb n="5"/> qui a jugé la question <hi rend="italic">in terminis</hi>, &amp; ainsi cet Arrêt ne pourroit jamais servir de regle aujourd'hui.</p>
<p>5° Enfin, les differens Arrêts sur Requête par eux obtenus, pareils à ceux auxquels les Orfèvres ont formé opposition.</p>
<p>Des Arrêts sur Requête ne sont pas des titres, &amp; si les Gardes de ce tems n'y ont pas formé d'opposition, cela ne peut empêcher ceux d'à present de le faire &amp; d'user du droit qu'ils ont.</p>
<p>Ainsi les titres des Lapidaires se réduisent donc à des Sentences infirmées &amp; à des Arrêts sur Requête.</p>
<p>Au contraire, ceux des Orfèvres sont les Statuts respectifs des Communautez, des Arrêts qui ont jugé la question <hi rend="italic">in terminis</hi> avec les Lapidaires.</p>
<p>A quoi ils ajoutent le Reglement general de l'Orfèvrerie du 30 Décembre 1679, enregistré en la Cour, qui leur donne à l'exclusion de tous autres Marchands &amp; Artisans l'emploi de l'or &amp; de l'argent.</p>
<p>Après cela, sied-il bien aux Lapidaires de dire, comme ils font, qu'ils sont dans une possession immémoriale de monter les Pierres, &amp; peut-on traiter de possession une contravention qui se trouve reprimée toutes les fois qu'elle paroît ? Si les Lapidaires avoient cette possession prétenduë, pourquoi ne pas travailler en Boutique comme leurs Statuts les y obligent, &amp; comme les Orfèvres y sont pareillement obligez ? Leur retraite dans les Chambres particulieres prouve bien qu'ils reconnoissent eux-mêmes que, quand ils veulent faire ce travail, ils sont en contravention.</p>
<p>Par rapport à l'érection des Lapidaires, pour prouver qu'ils ne sont que de 1585, il suffit d'avoir recours aux Lettres Patentes qui furent accordées aux Orfèvres par le Roi, le 4 Novembre 1581, &amp; qui furent enregistrées en <pb n="6"/> la Cour par Arrêt contradictoire du 29 Novembre 1582, par lequel il fut dit que les Orfèvres continueroient de tailler, faire tailler les Pierres, comme de toute ancienneté, sans qu'il soit besoin d'ériger le mêtier de Lapidaire en mêtier Juré<note><p>Page 613 du Recueil.</p></note>.</p>
<p>Les Lapidaires n'ont rien à objecter contre des titres aussi autentiques, sinon de dire que l'Arrêt de 1631 est contraire à leurs Statuts, &amp; que celui de 1673 n'en est qu'une suite.</p>
<p>Mais la Cour vient de voir qu'au contraire l'un &amp; l'autre y sont conformes, qu'ils ont toujours été executez, il n'en faut d'autre preuve que les saisies faites en differens tems sur les Lapidaires.</p>
<p>Des titres pareils ne se prescrivent point, ainsi les Orfèvres ont lieu d'esperer que la Cour, toujours attentive à conserver l'ordre &amp; la police dans le Royaume, &amp; entre les Corps &amp; Communautés qui y sont établis, &amp; surtout dans la Ville de Paris, ne permettra pas que par provision des Ouvriers tels que les Lapidaires continuent le Commerce &amp; la profession des Orfèvres, au préjudice de titres aussi autentiques<note><p>Les saisies sont faites d'outils d'Orfèvrerie, de limaille d'argent, &amp; d'or &amp; d'argent brut &amp; à moitié employé, de croix &amp; bagues d'or massives &amp; à bas titre, ce qui a obligé les Orfèvres de les porter à la Cour des Monnoyes.</p></note> ; ce seroit ruïner entierement les Orfèvres, &amp; les Lapidaires n'ont point à se plaindre qu'on les renferme dans les bornes de leur profession, qui est la taille des Pierres, dont ils n'auroient jamais dû s'écarter. Le bon ordre &amp; la police le demandent &amp; militent pour les Orfèvres, aussi bien que les titres.</p>
<p><hi rend="italic">Monsieur de La Guillaumie, Rapporteur.</hi></p>
<p>Delaguette, Proc.</p>
<p>De l'Imprimerie de Paulus-du-Mesnil, 1735.</p>
</div>
</body>
</text>
</TEI>