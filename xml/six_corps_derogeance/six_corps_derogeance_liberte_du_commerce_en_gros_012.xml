<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">012. "Réfléxions du corps de l'orphèvrerie sur un mémoire dont l'objet est de supplier le Roy qu'il luy plaise d'oter au commerce en détail, tel que le font les Six Corps des Marchands de la ville et fauxbourgs de Paris, la tache de dérogeance qui paroit avoir été implicitement attachée à ce commerce par différentes déclarations qui permettent aux Nobles le seul commerce en gros sans déroger." (1762)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>Arch. Seine, 2ETP/10/3/00, 1762, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>Arch. Seine 2ETP</repository><idno>/10/3/00</idno></msIdentifier><head><title>n° 0130 Arch. Seine, 2ETP/10/3/00 n° 2 [1762] [manuscrit]</title><origDate when="1762">1762</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>gros et détail — suppression de la dérogeance</term>
</keywords>
<keywords ana="parties">
<term>orfèvres contre législation du Conseil</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0130" type="factum">
<head>Arch. Seine, 2ETP/10/3/00 n° 2 [1762] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Réfléxions du corps de l'orphèvrerie sur un mémoire dont l'objet est de supplier le Roy qu'il luy plaise d'oter au commerce en détail, tel que le font les Six Corps des Marchands de la ville et fauxbourgs de Paris, la tache de dérogeance qui paroit avoir été implicitement attachée à ce commerce par différentes déclarations qui permettent aux Nobles le seul commerce en gros sans déroger.</hi></p>
<p>Il paroit que l'auteur du mémoire dont est question n'a eu en vüe qu'une très petite partie de cette societé de marchands qui forment les Six Corps.</p>
<p>L'on croit qu'entreprendre de détruire un préjugé accrédité pendant plusieurs siecles, adopté dans plusieurs grandes monarchies, soutenu par des causes d'Etat et favorable même à la plus grande partie des marchands qui en solliciteroient l'extinction, seroit une démarche légere qui entraineroit nombre d'inconveniens, et qui ne pourroit manquer d'etre desavoué par le plus grand nombre de marchands qui, toujours éclairés sur leurs intérets, ne regarderont point comme un bien une idée qui leur paroitroit chimérique, dont très peu d'entr'eux seroient à portée de profiter.</p>
<p>Que la plus grande partie des marchands de ce corps, loin de penser comme l'auteur du mémoire que cette prérogative distinguée de non dérogeance seroit pour eux un aiguillon d'honneur qui les encourageroit à continuer leur commerce et à y faire succéder leurs enfans, et par ce moyen augmenter les forces de l'Etat en le peuplant de riches marchands, pensent au contraire que cette idée, qui fait honneur au zèle patriotique de l'auteur, n'a de réel que cette spéculation satisfaisante, <pb n="2"/> que l'effet de ce projet actuel des Six Corps seroit diamétralement opposé à l'idée qu'ils s'en font.</p>
<p>En effet, en supposant pour quelques instants que cette faveur fut accordée au commerce en détail des Six Corps des marchands, ce seroit renverser en un seul instant le plus ferme appuy de leur commerce, cette dérogeance soit réelle ou idéalle.</p>
<p>Pour mettre cette vérité dans son jour, il est besoin de mettre sous les yeux l'etat médiocre ou etoit le commerce avant que cette monarchie fut montée à ce degré de splendeur où nous la voyons présentement.</p>
<p>Dans ces tems reculés, l'etat environné de voisins puissans etoit attaqué de toutes parts ; il falloit des défenseurs. Les distinctions et les dignités furent les appanages glorieux de ceux qui embrasserent le party des armes.</p>
<p>Le commerce foible alors fut méprisé. Le labour des terres, le débit des denrées, l'exercice de quelques métiers formoient alors tout son etendüe. Le commerce etoit un, ces distinctions survenües depuis etoient ignorées.</p>
<p>La magistrature même ne fut pas à couvert du mépris des Nobles. Mais depuis la découverte de l'Amérique, le commerce s'est étendu. De simples marchands firent succéder l'abondance à la pauvreté, les besoins furent multipliés, les impots furent augmentés considérablement, les denrées et la main d'œuvre le furent en proportion, le luxe survint, les arts naquirent, tous aspirerent à ce bien être. Les Nobles désirerent la fortune ; mais accoutumés à peu estimer les marchands, ils ne voulurent point la devoir au commerce ; et relativement à leur idée, c'eut été dégénérer. Ils s'attacherent aux Souverains, les Rois devenus plus puissans rendirent leurs bienfaits plus fréquens et plus abondans, et insensiblement l'aisance se répandit partout.</p>
<p>Ces heureuses révolutions firent changer le systême politique, et l'on commence à comprendre que comme les Nobles sont les <pb n="3"/> défenseurs de l'Etat, les commerçans en sont le soutien. Et par une suite de ces raisonnemens, on crut devoir accorder à ces derniers des dignités comme à des citoyens estimables, et permettre aux premiers de faire la portion la plus éclatante du commerce sans déroger. Mais les préjugés sont indépendans des loix. Les détruire est l'ouvrage du tems et des circonstances. Le peu de succès qu'eurent les Edits de 1669 et de 1701 en est la preuve. Nos efforts aujourd'huy seront-ils plus heureux ?</p>
<p>Lorsque les Six Corps se déterminerent de demander au Roy l'abolition de ce préjugé, avoient-ils bien examiné cette matiere susceptible de tant de détail et de difficultés ?</p>
<p>Qu'obtiendroit-on si l'on venoit à renverser une fois ce rampart qui divise le peuple d'avec la noblesse ? Un surcroit de marchands qui, grace au préjugé de la dérogeance, sont maintenant les tributaires de notre industrie, et qui cesseroient de l'etre.</p>
<p>Ces malheurs tout sensibles qu'ils seroient dans les grandes villes deviendroient bien plus considérables dans les campagnes. Le gentilhomme commerçant accableroit tout de son autorité et de sa fortune. Bientot ses fermiers deviendroient ses vassaux, et l'on verroit renaitre sous un autre nom le gouvernement féodal, cette honte de l'humanité.</p>
<p>Les Romains, ces sages législateurs, avoient sans doute prévu ces desordres ainsy qu'il paroit par cette loy faite en faveur des commerçans, et citée par l'auteur du mémoire qui s'explique ainsy : sunt enim non nulli qui jus comercii exercere prohibentur, ut nobiles et potentiores personae, non quiae indecorum, sed quiae difficile esset plebi cum eis commercium.</p>
<p>On est persuadé que l'intéret public est le seul motif qui a dirigé l'intention des Six Corps à former l'idée de la non dérogeance pour leur commerce en détail. Mais ne pourroit-on pas penser qu'eblouis par le brillant de cette proposition, ils ont perdu de vuë pour quelques instants cet intérêt public qui est toujours la boussolle de leur conduite, si l'on considere qu'il est des moyens ouverts à ceux des marchands dont la <pb n="4"/> fortune est considérable pour s'illustrer, soit par l'acquisition des charges de secretaire du Roy, soit par celles de trésorier de France, et autres. Or, après cette premiere démarche, le premier soin de ces nouveaux illustrés est d'acquérir pour leurs enfans les charges honorables de la magistrature. Le moins fortuné hors d'etat de renoncer à son commerce voudroit aussy faire pour ses enfants ce que vient de faire le favori de la fortune pour les siens. Mais icy le préjugé de la dérogeance l'arrête, met un frein à son ambition, et la raison le force de faire de son fils un marchand qui enrichit l'Etat par son industrie. Ce préjugé de la dérogeance, envisagé sous ce point de vuë, est donc favorable au commerce puisqu'il prévient bien plutot l'emigration des marchands qu'il ne l'occasionne.</p>
<p>On est obligé de convenir que la vanité et la légereté sont les péchés originels de notre nation. Sera-ce en brisant les seuls liens qui les retiennent encore que l'on les forcera à subir le joug de la raison ?</p>
<p>Un jeune homme dans cette effervescence de vanité et d'ambition, dupe de son amour propre, sera-t-il bien docile aux avis de son pere qui, dépouillé par la loy du reste de son autorité, verra son fils abandonner le commerce et, de bon marchand qu'il auroit dû être, devenir souvent un très ignorant Magistrat ? Mais avant que d'en être à ce terme, si cette loy est demandée au nom des Six Corps pour l'etat actuel de leur commerce, que de dégouts, de contrarietés et d'obstacles insurmontables n'auront-ils pas à eprouver, tant de la part de la noblesse d'épée que de celle de la robe ! Et le moindre des malheurs qui résulteroit d'une conduite aussy peu réfléchie seroit d'affoiblir aux yeux du Roy le mérite de leur don.</p>
<p>D'ailleurs les marchands ne se trompent-ils point en attanchant eux mêmes à leur commerce une idée d'avilissement qui n'y est point en effet ?</p>
<p>Le commerce en détail est une suite nécessaire du commerce en gros, aussi indispensable pour l'Etat.</p>
<p>Que deviendroient sur nos ports ces amas considérables <pb n="5"/> de marchandises si l'intérêt, ce mobile des hommes qui les y a assemblé, ne prenoit soin d'en régler la distribution et le détail ?</p>
<p>De ces deux genres de commerce, l'un a pour lui l'eclat de l'abondance, l'autre l'avantage inestimable d'une nécessaire distribution. Si des loix émanées de nos Souverains pour le bien de ce Royaume ont divisé l'unité du commerce et en ont formé deux branches, toujours la raison ramenera les êtres pensans à ce principe que ces deux branches se soutiennent l'une par l'autre, et que c'est leur heureuse harmonie qui forme le commerce.</p>
<p>Est-on réellement bien fondé à mépriser des citoyens utiles qui remplissent leurs fonctions avec honneur et probité ?</p>
<p>Le commerce, dans le corps moral de l'Etat, doit être regardé comme le sang dans le corps phisique. C'est sa circulation réglée jusques dans les plus petits vaisseaux qui est le principe de la santé et du bien être. Or tout ce qui concourt à ce bien être ne doit-il pas être précieux au corps ?</p>
<p>Cette conséquence naturelle ne devroit-elle pas faire disparoitre toutes ces idées d'avilissement que l'on attache au commerce en détail ?</p>
<p>Dans la Magistrature comme dans les autres ordres de l'Etat, est-on méprisable quand on en occupe point les premiers rangs ?</p>
<p>Partant de ces principes, ne peut-on pas appliquer au commerce en détail cette idée de l'Empereur Justinien, citée par l'auteur du mémoire qui, en interdisant le commerce aux nobles comme aux riches, n'entendoit point l'avilir ny le flétrir : non quia indecorum, sed quia difficile esset plebi cum cis commercium. Par cette interprétation, l'on peut juger avec quelle attention cet Empereur protegeroit le commerce. A-t-on lieu de penser que dans un Etat florissant comme ce Royaume où l'on reconnoit ces maximes incontestables que le commerce est le plus ferme appuy de la puissance et de la grandeur de nos Rois, on voulut l'avilir et le deshonorer ?</p>
<p>Il est à souhaiter que les Six Corps des marchands, écartant <pb n="6"/> desormais ces idées inquietes, se bornent à joüir en paix d'une loy qui, loin de flétrir leur etat, en assure la durée et le bonheur.</p>
<p>Nous estimons qu'il seroit plus avantageux de se réunir pour supplier le Roy qu'il luy plaise retrancher ce grand nombre de lieux privilégiés qui portent au commerce des Six Corps un préjudice si considérable.</p>
</div>
</body>
</text>
</TEI>