<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fr">
<teiHeader><fileDesc>
<titleStmt>
<title level="m" type="main">008. "Memoire pour les Six Corps des Marchands de Paris." (1762)</title>
<respStmt><resp>Fichier XML-TEI encodé en 2023</resp><orgName>Centre de recherches historiques (UMR 8558/ CNRS-EHESS)</orgName></respStmt></titleStmt><editionStmt><edition>Arch. Seine 2ETP, /10/3/00, 1762, manuscrit</edition></editionStmt>
<publicationStmt>
<publisher>Centre de recherches historiques</publisher>
<availability status="free">
<licence target="https://github.com/etalab/licence-ouverte/blob/master/open-licence.md">Distributed under an Open License 2.0</licence>
</availability>
</publicationStmt>
<sourceDesc>
<msDesc><msIdentifier><repository>Arch. Seine 2ETP</repository><idno>/10/3/00</idno></msIdentifier><head><title>n° 0134 Arch. Seine 2ETP/10/3/00 n° 2 [1762] [manuscrit]</title><origDate when="1762">1762</origDate></head></msDesc></sourceDesc>
</fileDesc><profileDesc>
<creation>
<date when="2023-04"/>
</creation>
<langUsage>
<language ident="fr"/>
</langUsage>
<textClass>
<keywords ana="litiges">
<term>gros et détail — suppression de la dérogeance</term>
</keywords>
<keywords ana="parties">
<term>six corps contre législation du Conseil</term>
</keywords>
<keywords ana="juridiction">
<term>conseil</term>
</keywords>
</textClass>
</profileDesc></teiHeader>
<text>
<body>
<div n="0134" type="factum">
<head>Arch. Seine 2ETP/10/3/00 n° 2 [1762] [manuscrit]</head>
<p><pb n="1"/> <hi rend="bold">Memoire pour les Six Corps des Marchands de Paris.</hi></p>
<p>Les Six Corps des Marchands de Paris se flattent d'avoir donné dans tous les tems des preuves de leur attachement à la gloire de leur Souverain et aux interêts de l'Etat.</p>
<p>Si malgré leurs efforts, ils ne sont pas parvenus à exprimer toute l'etendue de leurs sentimens, le desir qu'ils ont de se rendre dignes de la bienveillance de Sa Majesté la leur fera certainement mériter.</p>
<p>Le Pere de son Peuple protegera des sujets fidels qui ne reclament aujourd'hui que les droits accordés à tous citoiens que le crime ou la servilité n'ont pas dégradé de l'etat de leur naissance.</p>
<p>Par le préjugé le plus inconcevable, on est parvenu jusqu'à infliger une tache au commerce ; les mêmes moiens qu'on a emploiés pour l'encourager servent aujourd'hui à l'aneantir.</p>
<p>Les Marchands de Paris vont develloper ces verités, en présenter les funestes effets et le veritable moien d'en arreter le cours.</p>
<p>Il seroit bien difficile de rendre raison des préjugés qui ont affecté successivement toutes les nations. Les Juifs avoient en horreur la peinture et la sculpture. Les Babiloniens decrioient la medecine. Platon bannit la poesie de sa république. Rome, sous Domitien, chassa les mathématiques. Le commerce <pb n="2"/> paroit humilié en France.</p>
<p>Cette opinion si défavorable au commerce est née au milieu de nous. On en découvre l'origine dans notre propre histoire, au milieu de ces tems barbares d'un gouvernement feodal qui representoit moins une monarchie qu'un grand fief. On ne connoissoit alors ni vües générales ni interêt public. Cette foule de souverains ne pensa longtemps qu'à s'attaquer et à se deffendre.</p>
<p>Tout ce qui ne portoit pas l'image de la féodalité, ce qui ne rapprochoit pas des idées de chevalerie, en un mot tout ce qui pouvoit faire sortir d'une grave oisiveté, paroissoit ignoble et indigne de l'attention. Les lettres mêmes ne furent pas exemptes du mépris, à plus forte raison le commerce qui auroit rendu suspect la communication des vassaux.</p>
<p>Les professions les plus honorables aujourd'hui ont donc été dans leur principe couvertes du même mepris. Elles seroient encore dans cet état si un gouvernement plus naturel et plus éclairé ne les en ont successivement tirées par des loix d'encouragement.</p>
<p>Le commerce ne pouvoit manquer d'obtenir quelque distinction. On s'aperçut qu'il pouvoit devenir le germe de la puissance et de la grandeur. Charles IX, par lettres patentes de 1556, fit voir qu'il en connoissoit l'utilité, il permit le commerce de mer aux nobles de Marseille, de Roüen et de Bretagne.</p>
<p>La justice de Louis XIII ne put souffrir que le commerce demeura méprisé. Par son ordonnance de 1629, il s'exprima ainsi : " pour convier nos sujets de quelque qualité et condition qu'ils soient de s'adonner au commerce et trafic de mer, et faire connoitre que notre intention est de relever et de faire honnorer ceux qui s'en occuperont, nous ordonnons que tous gentilshommes qui par eux ou par personnes interposées <pb n="3"/> entreront en part et société des vaisseaux, denrées et marchandises d'iceux, ne dérogeront point à la noblesse ".</p>
<p>Louis XIV renouvella en 1669 l'invitation aux nobles de se livrer au commerce de mer et en gros. Il y joignit le commerce en gros sur terre.</p>
<p>Ces différentes loix n'ont produit aucuns des effets que l'on se proposoit.</p>
<p>Le commerce en s'éloignant de ces tems de chevalerie acqueroit luy même, et par sa propre utilité, une considération qui n'auroit fait que croitre ; une expression générale de la part du Souverain l'auroit porté tout d'un coup au premier degré ; des expressions insuffisantes l'ont laissé dans le même état de préjugé où il etoit, si elles ne l'ont pas abaissé davantage.</p>
<p>On a remarqué dans ces différentes loix une distinction dans l'exercice du commerce. En indiquant premierement aux nobles le commerce de mer, et longtems après celui de terre, en gros seulement, ces loix ont fait renaitre le préjugé que tout le corps du commerce avoit été jusqu'alors un état ignoble ; ces distinctions particulieres parurent autant d'exceptions au principe général.</p>
<p>Il en est résulté 1<hi rend="sup">er</hi> que les nobles reprirent le préjugé que le tems et les connoissances avoient presque eteint ; convaincus que pour arriver au grand il faut souvent passer par le moindre, et voiant que l'un n'étoit séparé de l'autre que par des points imperceptibles, les nobles aimerent mieux renoncer à la liberté qu'on leur conservoit que d'en étudier les bornes, et courir les dangers de la dérogeance en les excédant.</p>
<p>2<hi rend="sup">e</hi> Le commerce de détail ou d'economie, qui s'elevoit également avec tout le commerce, parut à l'epoque de ces loix recevoir la tache la plus humiliante. L'indication <pb n="4"/> aux nobles de s'occuper du commerce en gros de terre sans déroger sembla prononcer la dérogeance et consequemment la servilité dans le commerce au détail.</p>
<p>Cependant le commerce est un tout, dont on ne peut honorer une partie et mepriser l'autre sans que le mepris ne couvre tout entier. Le commerce en gros et celui en détail se reunissent et tendent au même but d'utilité publique. Les opérations d'economie dirigent toujours celles du commerce en gros ; c'est dans le premier que l'on connoit de plus près les besoins pour y pourvoir ; c'est par les connoissances particulieres du gout et du genie du consommateur que le commerçant d'economie dirige les manufactures et les fabriques, qu'il les anime et les soutient ; il est souvent inventeur des objets les plus utiles tant à la consommation intérieure qu'à l'exportation. L'intention du gouvernement n'a jamais pu être d'attacher une idée de servilité ou d'infamie à des occupations de ce genre.</p>
<p>En vain toutes ces réflexions se sont-elles reunies avec l'interêt personnel pour vaincre la répugnance des citoiens ou simples bourgeois. Elle s'autorise de l'expression des loix, elle ne peut être détruite que par la disposition précise d'une autre loy.</p>
<p>Il ne faut que connoitre le genie françois pour decouvrir le principe de cette répugnance. L'honneur est son sentiment naturel. Le citoien ou simple bourgeois respecte la noblesse, mais il croit avec raison qu'il doit y avoir pour luy un etat mitoien entre celui de la noblesse et celui de la servilité. Ce citoien ou simple bourgeois répugne à entreprendre ou à continuer le commerce de détail parce qu'il n'y trouve pas ce juste milieu, et qu'il craint que la loy <pb n="5"/> n'ait prononcé la servilité pour ce même commerce, et par consequent contre la liberté de son etat.</p>
<p>Il est du pouvoir du Souverain de vaincre cette répugnance et de faire connoitre dans tout son roiaume cette vérité qu'il est aussi honnête de s'occuper de tout commerce que de faire le verre, peindre ou dessiner.</p>
<p>Cette justice est d'autant plus nécessaire au commerce de Paris que la situation de cette ville, presque au centre du roiaume, l'empeche d'être une ville d'entrepot et de commerce en gros. Tout ce qu'elle attire de marchandises y arrive chargé de tous les droits possibles, de traittes, peages, poids le roy, etc. Les marchandises une fois entrées dans cette ville ne peuvent être transportées ailleurs. Cette ville n'est donc proprement qu'une ville d'importation et de consommation. Tout le commerce prodigieux qui s'y fait est pour son approvisionnement particulier. Ce n'est donc, en plus grande partie, qu'un commerce de détail.</p>
<p>Cependant ceux qui s'occupent journellement d'opérations aussi grandes et d'objets aussi interessans à cette capitale ne sont point affranchis de cet etat de servilité. Il leur est même en quelque façon particulier, puisqu'ils ne peuvent pas exécuter les distinctions faittes dans la loy. Les Six Corps des Marchands de Paris presentent donc un exemple frapant de l'injustice de ces distinctions du commerce en gros et du commerce en détail.</p>
<p>Ils ne prétendent à aucune préeminence, ils desirent seulement qu'on ne leur fasse pas un crime de leur profession. Ils ne cherchent point à acquérir dans le commerce une distinction étrangere à leur naissance, mais à ne pas perdre dans le commerce celle que leur naissance peut leur avoir acquise.</p>
<p>En effet, chacun de ceux qui s'occupent du commerce <pb n="6"/> reunit en luy deux qualités, celle de citoien et celle de marchand. Si dans la premiere qualité, une pere de famille peut aspirer, soit pour luy soit pour ses enfans, à quelque charge distinguée dont l'entrée luy soit ouverte par sa fortune et sa naissance honorable, l'autre qualité, celle de marchand, luy ferme cette même entrée.</p>
<p>Il se trouve dans une maison de commerce plusieurs enfans, ils ne peuvent pas tous perdre le même état ; les uns continuent celui de leur pere, les autres ont des dispositions pour quelqu'autres professions, soit dans la robe soit dans l'épée, ils y sont admis à la verité. Mais l'on exige comme une condition nécessaire que le pere quitte son commerce, et les enfans qui pourroient luy succeder sont pareillement obligés d'y renoncer. Il faut donc que le pere sacrifie à l'etablissement d'une partie de ses enfants, le sien propre et celui de ses autres enfans.</p>
<p>Enfin il ne sçauroit recouvrer les droits de sa qualité de citoien qu'en abdiquant celle de marchand, comme si elles etoient incompatibles.</p>
<p>Cet avilissement nécessite d'abord l'ambition, source de tous les malheurs dans le commerce.</p>
<p>Le premier effet de cette passion est l'emigration continuelle des marchands de l'etat du commerce.</p>
<p>Les uns, calculant le peu d'années dont leur vie est mesurée, se laissent aveugler par le desir de parfaire en peu de tems une fortune qui devroit naturellement être le fruit de plusieurs générations.</p>
<p>D'autres, s'abandonnant à l'impatience de se retirer, eux et leurs enfans, d'un etat qui paroit meprisé, se permettent peut être du relachement dans les occasions où ils entrevoient du secret ou de l'impunité.</p>
<p><pb n="7"/> Ceux qui restent constamment attachés aux vrais principes des négocians, la modération et la probité, ne trouvent d'autres ressources que dans l'economie la plus excessive, ils languissent et demeurent étouffés par les entreprises téméraires de ceux qui n'ont d'autre regle que leur ambition.</p>
<p>Enfin s'il s'en trouve quelques uns assez heureux pour echapper aux dangers d'un travail aussi penible que desagréable, ils sont encore plus empressés de s'en retirer et de faire prendre à leurs enfans un état où ils puissent acquerir une considération qu'ils chercheroient en vain dans le commerce.</p>
<p>De tous les maux qui résultent nécessairement de cette emigration, on en distingue deux qui affligent dangereusement le commerce. L'un est le défaut d'expérience, et l'autre l'appauvrissement du commerce.</p>
<p>Toujours novices, les marchands commencent de nouveaux établissemens, ils travaillent sans expérience et sans guide, dans un âge où le feu de la vivacité les expose à mille fautes. Ils sont même privés de la ressource des conseils, puisque toute la surface de l'etat commerçant ne leur présente que des confreres aussi novices qu'eux. Ils ne trouvent plus dans ces anciennes maisons les enfans dépositaires de l'expérience et de la prudence de leurs peres. Abandonnés à leur insuffisance naturelle, ce ne sont pas eux qui conduisent le commerce, mais c'est le commerce qui les conduit et les entraine souvent dans des circonstances malheureuses, dont on ne voit que trop d'exemples depuis quelques années.</p>
<p>Quant à l'appauvrissement du commerce, rien n'est plus palpable. Il faut regarder comme un fait constant que dès qu'un marchand a acquis une fortune suffisante <pb n="8"/> pour vivre et établir sa famille hors du commerce, il l'abandonne. S'il en est quelques uns assez courageux pour resister au préjugé et vouloir en preserver leurs enfans, ceux cy ne restent dans cet etat qu'avec une impatience qui se manifeste au même moment que la mort de leurs peres les rend maîtres de leurs fortunes.</p>
<p>Ce sont donc des fortunes perdües pour la masse du commerce. L'emigration étant continuelle, ce sont des sommes considérables qui sortent continuellement du commerce ; il s'appauvrit donc nécessairement ; on le reduit sans cesse à la foiblesse d'un premier âge ; loin de s'accroitre, il perd tous les jours sa substance et ses forces.</p>
<p>Enfin on ne peut mieux comparer l'etat actuel du commerce qu'à un héritage entre les mains d'usufruitiers impatiens de jouir. Ils épuisent la nature du sol et le laissent ensuitte à l'autres qui sont hors d'etat même de l'ensemencer.</p>
<p>Les Six Corps des Marchands de Paris l'avancent avec confiance : tous ces malheurs disparoitront aussitot qu'une loy positive aura établi qu'il est possible d'exercer tout commerce sans la tache affligeante de la dérogeance, aussitot qu'on aura rendu compatibles en tous pays la qualité de citoien et celle de marchand, sans distinction.</p>
<p>L'ambition n'aura plus d'objet, elle se convertira en une noble émulation pour se distinguer et se perpétuer dans un etat considéré ; loin de rougir de leur profession, les peres en inspireront le gout à leurs enfans, et ceux cy le prendront d'autant plus volontiers qu'ils auront été témoins de la considération dont jouissoient leurs peres.</p>
<p>Aux deux maux destructifs succederont deux <pb n="9"/> principes vivifians.</p>
<p>Les enfans d'un pere dont les ancêtres auront commencé un établissement jouiront presque en naissant de l'experience de plusieurs siecles. Les fortunes se perpetueront dans les familles commerçantes, répandront l'aisance dans les affaires et les multiplieront.</p>
<p>Les maisons anciennes, deja riches et élevées sur les fondemens de la modération et de la probité, seront le conseil et l'exemple de celles qui naitront. On ne verra plus cette précipitation dans les projets des fortunes, ni ces jouissances anticipées. Les peres laisseront leurs enfans achever ce qui ne peut l'être avec honneur dans le court espace de la vie d'un seul homme.</p>
<p>L'abondance de l'argent fera perir l'usure, ce monstre detestable qui se nourrit de la peine et du travail du négociant laborieux. Le commerce subsistera de ses propres forces et le consommateur ne s'appercevra plus de la nécessité que se faisoit un marchand de gagner en une seule fois ce qu'il se contentera de gagner en deux ou plusieurs fois lorsque l'argent sera abondant dans le commerce.</p>
<p>Il ne s'agit que d'abattre une foible separation ou de rendre au commerce son unité ; comme Louis XIV a cru devoir ajouter aux vues de ses prédécesseurs, Sa Majesté actuellement regnante verra qu'il luy etoit reservé de les porter à leur perfection.</p>
<p>Par toutes ces considérations, les Six Corps des Marchands de Paris osent se flatter que Sa Majesté voudra bien en interprettant en tant que de besoin les edits de 1629, 1669 et 1701, declarer que si ces edits ont invité les nobles à quelque genre particulier de commerce, l'intention n'a pas été qu'il put en résulter la peine de dérogeance contre les autres genres de commerce, et encore moins contre ceux <pb n="10"/> dont les Six Corps des Marchands de Paris s'occupent journellement, tant en gros qu'en détail, en conséquence déclarer que tous ceux qui s'en sont occupés et qui s'en occuperont à l'avenir ne cesseront de jouir eux et leurs descendans, en toutes occasions, des droits et prérogatives à eux appartenans par leur naissance.</p>
<p>Goulleau avocat.</p>
</div>
</body>
</text>
</TEI>