# TEIPublisher

- Les documents XML sont chargés dans la visionneuse avec ce code :

```html
<section class="" style="">
	<paper-card class="doclist" data-i18n="[Factums]browse.documents" heading="Factums">
	<paper-card class="doclist">
	    <div class="card-content">
	        <div class="browse" slot="page">
	            <pb-custom-form id="facets" url="api/search/facets" event="pb-results-received" subscribe="docs" emit="docs"/>
	            <pb-browse-docs id="document-list" url="api/collection/<DOSSIER AVEC LES HTML>" sort-options="[{&#34;label&#34;: &#34;browse.title&#34;, &#34;value&#34;: &#34;title&#34;},{&#34;label&#34;: &#34;browse.author&#34;, &#34;value&#34;: &#34;author&#34;}]" sort-by="file" filter-options="[{&#34;label&#34;: &#34;browse.title&#34;, &#34;value&#34;: &#34;title&#34;},{&#34;label&#34;: &#34;browse.author&#34;, &#34;value&#34;: &#34;author&#34;},{&#34;label&#34;: &#34;browse.file&#34;, &#34;value&#34;: &#34;file&#34;}]" filter-by="file" auto="auto" history="history" login="login" emit="docs" subscribe="docs">
	                <pb-paginate slot="toolbar" per-page="10" id="paginate" range="5" subscribe="docs" emit="docs"/>
	                <pb-restricted login="login" slot="toolbar">
	                    <pb-ajax emit="docs" url="api/odd" method="post" data-template="lib:parse-params" event="pb-search-resubmit" title="Recompile ODD" data-i18n="[title]odd.manage.regenerate-all">
	                        <h3 slot="title">
	                            <pb-i18n key="odd.manage.regenerate-all"/>
	                        </h3>
	                        <paper-icon-button icon="icons:update"/>
	                    </pb-ajax>
	                </pb-restricted>
	            </pb-browse-docs>

	        </div>
	    </div>
    </paper-card>
	<pb-restricted class="upload-panel" login="login">
	    <aside>
	        <paper-card class="upload" data-i18n="[heading]upload.upload" heading="Upload">
	            <div class="card-content">
	                <pb-upload id="upload" emit="docs" accept=".xml, .tei, .odd, .docx, .mei"/>
	            </div>
	        </paper-card>
	    </aside>
    </pb-restricted>
</section>
```

- Le chemin vers le dossier qui contient les XML doit être mis dans ` /db/apps/factums/modules/config.xqm`, ligne `declare variable $config:data-root :=$config:app-root || "/<DOSSIER>";`.

- Les documents XML peuvent être déposés dans la collection à laquelle ils appartiennent
  - avec un glisser déposer dans la partie "Télécharger" de la page de la collection ;
  - dans le navigateur de fichiers de l'IDE (`file>manage`).

- Pour modiffier les informations qui apparaissent dans la visioneuse, sous le titre du HTML : ` 
/apps/<APP>/transform/<APP>-web.xql `.

	Pour les mots clefs :
	```xquery
	case element(profileDesc) return
      (html:block($config, ., ("tei-w", css:map-rend-to-class(.)), ("Litige : ", textClass/keywords[@ana='litiges']/term))     => model:map($node, $trackIds),
      html:block($config, ., ("tei-w", css:map-rend-to-class(.)), ("Parties : ", textClass/keywords[@ana='parties']/term))     => model:map($node, $trackIds),
      html:block($config, ., ("tei-w", css:map-rend-to-class(.)), ("Juridiction : ", textClass/keywords[@ana='juridiction']/term))     => model:map($node, $trackIds))
    ```

    Pour le document, modifier :
    ```xquery
    case element(edition) return
       if (ancestor::teiHeader) then
       	html:block($config, ., ("tei-edition", css:map-rend-to-class(.)), ("Doctument :", .))                            => model:map($node, $trackIds)
      else
      	$config?apply($config, ./node())
     ```
- Gestion des utilisateurs dans la page d'accueil exist en étant connecté.

- Sécurité des dossiers déjà implantée.