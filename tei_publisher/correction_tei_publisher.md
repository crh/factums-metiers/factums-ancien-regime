% Correction sur le site _Factums des métiers_
% Mars 2023

## Processus de correction

1. Sur la page d'un factum, cliquer sur le "burger" en haut à droite.

![Cliquer sur le "burger".](./factums_1_burger.png "burger")

2. Un menu apparaît à droite de l'écran. Cliquer sur "Default single text layout".

![Cliquer sur "Default single text layout".](./factums_2_default_text_layout.png "Default single text layout")

3. Dans la liste déroulante, sélectionner "Annotation Editing".

![Sélectionner "Annotation Editing".](./factums_3_annotation_editing.png "Annotation Editing")

4. L'écran de correction s'affiche.
  - Sélectionner le mot ou le groupe de mots à corriger avec le curseur, puis cliquer sur le crayon en haut à gauche.
  - Entrer la correction en dessous de "Correction", à gauche.
  - Enregistrer en cliquant sur la disquette qui se trouve en dessous.

![Correction de "à Sa Majesté" par "au Roi".](./factums_4_correction.png "Correction 1")

5. Dans un premier temps, la correction n'apparaît pas sur la partie centrale de l'écran mais dans la partie de prévisualisation à droite.
  - Le passage corrigé est souligné en vert, mais c'est toujours l'ancienne version qui s'affiche.
  - Pour enregistrer définitivement la correction, cliquer sur la disquette de la partie de prévisualition (en haut à droite).

![Enregistrement définitif de la correction.](./factums_5_correction_faite.png "Correction 2")
