<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs" version="1.0">
    <xsl:output method="text" indent="yes" encoding="UTF-8"/>
    <!-- XSL stylesheet by Jean-Damien Généro, CNRS, Centre de recherches historiques (UMR 8558)-->
    <!-- TEX CANVAS -->
    <xsl:template match="/"><!--<xsl:result-document href="./essai.tex">-->\documentclass{report}
        \usepackage[utf8]{inputenc}
        \usepackage[T1]{fontenc}
        \usepackage[paperwidth =170mm, paperheight= 260mm,left=2.5cm, right=2.5cm, top=2.3cm, bottom=3.7cm]{geometry}
        \usepackage[french]{babel}
        \usepackage[hidelinks, pdfstartview=FitH, plainpages=false]{hyperref}
        \usepackage{xcolor}
        \usepackage{tabularx}
        \usepackage{ltablex}
        \usepackage{booktabs}
        \usepackage{chngcntr}% http://ctan.org/pkg/chngcntr
        \usepackage{graphicx}
        \usepackage[clearempty]{titlesec}
        \usepackage{float}
        % \usepackage[a4,cam,center]{crop}

        % espacement des paragraphes
        \usepackage[skip=5pt plus1pt]{parskip}
        
        % sommaire court
        \usepackage{shorttoc}
        
        % numérotation des sections et table des matières
        \usepackage{titlesec}
        \titleformat{\chapter}{\normalfont\huge\bfseries}{}{0pt}{}
        \titleformat{\section}{\normalfont\Large\bfseries}{}{0pt}{}
        
        \usepackage{titletoc}
        \makeatletter
        \let\latexl@section\l@section
        \def\l@section#1#2{\begingroup\let\numberline\@gobble\latexl@section{#1}{#2}\endgroup}
        \makeatother
        \makeatletter
        \let\latexl@chapter\l@chapter
        \def\l@chapter#1#2{\begingroup\let\numberline\@gobble\latexl@chapter{#1}{#2}\endgroup}
        \makeatother
        
        % indent
        \newenvironment{sommpara}{%
        \begin{list}{}%
        {\setlength{\leftmargin}{2em}%
        \setlength{\itemindent}{2em}%
        \setlength{\listparindent}{2em}%
        }%
        \item[]}
        {\end{list}}
        
        \newenvironment{sommpara2}{%
        \begin{list}{}%
        {\setlength{\leftmargin}{4em}%
        \setlength{\itemindent}{4em}%
        \setlength{\listparindent}{4em}%
        }%
        \item[]}
        {\end{list}}
        
        % TOC
        \makeatletter
        \renewcommand*\l@chapter[2]{%
        \ifnum \c@tocdepth >\m@ne
        \addpenalty{-\@highpenalty}%
        \vskip 1.0em \@plus\p@
        \setlength\@tempdima{1.5em}%
        \begingroup
        \parindent \z@ \rightskip \@pnumwidth
        \parfillskip -\@pnumwidth
        \leavevmode \bfseries
        \advance\leftskip\@tempdima
        \hskip -\leftskip
        #1\nobreak\hskip .5em \@plus 70fil %
        \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
        \penalty\@highpenalty
        \endgroup
        \fi}
        
        \def\@dottedtocline#1#2#3#4#5{%
        \ifnum #1>\c@tocdepth \else
        \vskip \z@ \@plus.2\p@
        {\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
        \parindent #2\relax\@afterindenttrue
        \interlinepenalty\@M
        \leavevmode
        \@tempdima #3\relax
        \advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
        {#4}\nobreak
        \leaders\hbox{$\m@th
        \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
        mu$}\hskip .5em \@plus 70fil %
        \nobreak
        \hb@xt@\@pnumwidth{\hfil\normalfont \normalcolor #5}%
        \par}%
        \fi}
        
        \makeatother
        % TOC
        
        \title{\textbf{Factums des métiers}}
        \author{\textit{\textsc{xvii}\textsuperscript{e} siècle -- \textsc{xviii}\textsuperscript{e} siècle}}
        \date{}
        
        \begin{document}
        \maketitle
        
        \newpage
        \thispagestyle{empty}
        \newpage
        
        \addcontentsline{toc}{chapter}{Sommaire}
        \shorttoc{Sommaire}{0}
        
        \newpage
        
        \vspace*{3cm}
        
        \begin{Large}\textbf{Avant-propos}\end{Large}
        \addcontentsline{toc}{chapter}{Avant-propos}
        
        \bigskip
        \bigskip
        
        Le corpus réuni ici sous le titre de \og Factums des métiers \fg{} est le fruit d'une double intention : celle d'étudier le corporatisme d'Ancien Régime, tout en offrant à la lecture les sources qu'il a laissées. Ces sources sont issues de l'activité rédactionnelle et banale de ce que nous appelons, aujourd'hui, les \og corporations \fg{}. Prenant place parmi les innombrables procès que ces groupes professionnels ont diligentés entre eux, ou à l'encontre d'une juridiction, d'un texte de loi, voire d'un simple particulier, elles reflètent en effet leur quotidien, et, par conséquent, le quotidien commercial d'une ville française des \textsc{xvii}\textsuperscript{e} siècle et \textsc{xviii}\textsuperscript{e} siècles.
        
        Le corporatisme peut s'apparenter à un litige quasi permanent agité par ses propres acteurs, d'où la possibilité de réunir la documentation disponible sous l'intitulé de \og factums \fg{}, à savoir des textes de contestation, revendication, accusation, défense, rédigés dans le cadre d'une procédure (devant le Conseil du roi, le Parlement, un tribunal de première instance...). Pour la plupart, les textes présentés s'inscrivent dans une suite argumentative, d'attaques et contre-attaques impliquant deux parties principales (par ex. épiciers contre apothicaires), dans un lieu donné. Pour l'essentiel, et sauf indication contraire, ce lieu est ici Paris.
        
        Si l'intitulé de \og factum \fg{} semble alors quelque peu usurpé, eu égard à sa ou ses définitions académiques, il n'en recoupe pas moins la production immense, et parfois difficile d'accès, que les agents du commerce, de l'artisanat, ont tiré de leur existence collective. On n'entrera pas ici dans le débat portant sur les auteurs \og réels \fg{} de ces textes. Les corporations ? Ou les auxiliaires de justice, avocats et procureurs, qu'elles engageaient ? Ces textes peuvent-ils réfléchir l'image courante des gestes de vente et de confection, en usage avant la Révolution ? Sans doute pas. Bien des développements sont consacrés à des aspects purement formels, juridiques, jurisprudentiels, protocolaires, hiérarchiques.
        
        Toutefois, on ne peut ignorer, d'une part, que ces aspects étaient à la base de toute expérience commerciale sous la période. Le rapport au droit était omniprésent. D'autre part, la définition juridique des techniques ou des marchandises impliquait toujours leur définition pratique. Du reste, les \og factums \fg{} sont souvent les seuls vestiges permettant de reconstituer la réalité autant que les aléas, les rivalités ordinaires, que traverse cette expérience commerciale, collective mais aussi individuelle, dans son lien à la société, à la monarchie, aux normes techniques. Ils sont le témoin, volontiers unique, d'un flot d'événements liés à une économie incorporée quasi impénétrable autrement.
        
        Enfin, la préférence portée ici à l'argumentation, aux allégations des métiers, plutôt qu'aux jugements auxquels les factums répondaient bien souvent, est au coeur de ce projet. Ce dernier veut mettre l'accent non sur l'état de la réglementation en rapport à l'exercice commercial ou de artisanal, mais plutôt sur la manière dont les métiers travaillaient cette réglementation, et l'intégraient économiquement, socialement et politiquement.
        
        \bigskip
        
        \textbf{Équipe}
        
        \medskip
        
        \begin{itemize}
        \item \textit{Direction du projet :} Centre de recherches historiques (\textsc{umr} 8558/ \textsc{cnrs}-\textsc{ehess}).
        \item \textit{Direction scientifique :} Mathieu Marraud, chargé de recherches au \textsc{cnrs}.
        \item \textit{Direction technique :} Jean-Damien Généro, ingénieur d'études du \textsc{cnrs}.
        \item \textit{Collecte photographique :} Valérie Gratsac-Legendre, ingénieure de recherche de l'\textsc{ehess}.
        \item \textit{Participation à la saisie des textes :} Geneviève Morin.
        \item \textit{Structuration \textsc{xml}-\textsc{tei} des textes, transformation en \LaTeX, compilation en \textsc{pdf} :} Jean-Damien Généro.
        \item Site internet : \og \href{https://sourcesetdonnees.huma-num.fr/exist/apps/factums/index.html}{Factums des métiers} \fg.
        \item \textit{Date de création du PDF :} mars 2023.
        \item \textit{Contact :} \href{mailto:gestion.sourcesetdonnees@ehess.fr}{gestion.sourcesetdonnees@ehess.fr}
        \end{itemize}
        
        \newpage<xsl:apply-templates select="//text"/>
        \newpage
        
        \tableofcontents
        \addcontentsline{toc}{chapter}{Table des matières}
        \end{document}<!--</xsl:result-document>--></xsl:template>
    <!-- TITLES -->
    <xsl:template match="//text//head[@type='factum']">
        <xsl:variable name="apos1"> "</xsl:variable>
        <xsl:variable name="apos2">" </xsl:variable>
        <!--\section{<xsl:value-of select="replace(replace(., $apos2, ' \\fg{} '), $apos1, ' \\og ')"/>}-->\section{<xsl:apply-templates/>}
    </xsl:template>
    <xsl:template match="//text//div[@type='collection']">
        \chapter{<xsl:value-of select="./head[@type='collection']"/>}
        <xsl:for-each select="./div[@type='factum']"><xsl:sort select="./head"/><xsl:apply-templates/></xsl:for-each>
    </xsl:template>
    <!-- PARAGRAPHS -->
    <xsl:template match="//text//p">
        <xsl:choose>
            <!-- 1 indent -->
            <xsl:when test="./@rend='lmarg1'">\begin{sommpara}<xsl:text>
        \noindent </xsl:text><xsl:apply-templates/><xsl:text>
          
        </xsl:text>\end{sommpara}
            </xsl:when>
            <!-- 2 indents -->
            <xsl:when test="./@rend='lmarg2'">\begin{sommpara2}<xsl:text>
        \noindent </xsl:text><xsl:apply-templates/><xsl:text>
          
        </xsl:text>\end{sommpara2}</xsl:when>
            <!-- normal text without ident -->
            <xsl:otherwise><xsl:text>
    \noindent </xsl:text><xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- rules for <hi> : superscript text, italic text, bold text, bold + italic text -->
    <xsl:template match="//hi[@rend='sup']">\textsuperscript{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="//hi[@rend='italic']">\textit{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="//hi[@rend='bold']">\textbf{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="//hi[@rend='bolditalic']">\textit{\textbf{<xsl:apply-templates/>}}</xsl:template>
    <!-- page break -->
    <xsl:template match="//pb"><xsl:text>\textbf{[</xsl:text><xsl:value-of select="normalize-space(./@n)"/><xsl:text>]}</xsl:text>
    </xsl:template>
    <!-- footnotes -->
    <xsl:template match="//note"><xsl:text>\footnote{</xsl:text><xsl:apply-templates/><xsl:text>}</xsl:text></xsl:template>
    <!-- tables -->
    <xsl:template match="table">
        <xsl:text>\begin{center}&#10;</xsl:text>
        <xsl:text>\begin{tabularx}{\linewidth}{</xsl:text>
        
        <xsl:for-each select="row[1]/*">
            <xsl:choose>
                <xsl:when test="@colspan">
                    <xsl:for-each select="(//*)[position()&lt;=current()/@colspan]"> X </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>X </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:text>}&#10;</xsl:text>
        
        <xsl:for-each select="row"><xsl:for-each select="cell|th">
            <xsl:if test="self::xth">fseries </xsl:if>
            <xsl:apply-templates />
            <xsl:if test="position() != last()">
                <xsl:text> -&amp;- </xsl:text><!-- in tex tables, & separators will appeared as -&- and shall be replaced by the py script -->
            </xsl:if>
        </xsl:for-each>
            
            <xsl:if test="position()!=last()"> \\&#10;</xsl:if>
        </xsl:for-each>
        
        <xsl:text>\end{tabularx}&#10;</xsl:text>
        <xsl:text>\end{center}</xsl:text>
        
    </xsl:template>
    <xsl:template match="//figure"><xsl:text>\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{</xsl:text><xsl:value-of select="./graphic/@url"/><xsl:text>}
    \caption{Mon image}
    \label{fig1}
    \end{figure}

</xsl:text>
    </xsl:template>
</xsl:stylesheet>