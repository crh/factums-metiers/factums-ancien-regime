# Factums des métiers

**Ce dossier contient le code de la chaîne de traitement des Factums des métiers développée par le Centre de recherches historiques (UMR 8558).**

Le code de l'application est consultable : [Factums TEI Publisher](https://gitlab.huma-num.fr/crh/factums-metiers/factums-app).

## Informations sur le projet

- **Nom du projet** : Factums des métiers
- **Direction du projet :** [Centre de recherches historiques (UMR 8558/ CNRS-EHESS)](http://crh.ehess.fr/)
- **Direction scientifique :** Mathieu Marraud, chargé de recherches au CNRS
- **Direction technique :** Jean-Damien Généro, ingénieur d'études du CNRS
- **Collecte photographique :** Valérie Gratsac-Legendre, ingénieure de recherche de l'EHESS
- **Participation à la saisie des textes :** Geneviève Morin, docteure de l'EHESS
- **Structuration XML-TEI des textes :** Jean-Damien Généro
- **Développement et maintenance de l'application :** Jean-Damien Généro et Bertrand Dumenieu, ingénieur de recherche de l'EHESS. Remerciements : pôle Infrastructures de la DSI de l'EHESS
- **Maquette graphique :** Hugo Chièze (EHESS)
- **Hébergement :** [IR* Huma-Num](https://www.huma-num.fr/)
- **URL :** [factums-metiers.huma-num.fr](https://factums-metiers.huma-num.fr)

## Chaîne de traitement

La chaîne de traitement des factums des métiers est répartie en quatre fichiers Python (`/py/`). Elle possède trois fonctionnalités :

1. **Création d'un ensemble de fichiers XML-TEI correspondant à une collection de factums.**
  - Correspond au choix `O` de la châine de traitement.
  - Déroulé :
    - L'utilisateur choisi une collection de factums
    - Le fichier `.docx` de la collection est converti en un fichier `.xml` primitif avec [Pandoc](https://pandoc.org/) (fonction `pandoc_convert()`)
    - Le fichier `.xml` primitif est nettoyé avec des expressions régulières (regex) (fonction `txt_cleaning()`)
    - Le fichier `.xml` primitif est séparé en plusieurs fichiers `.xml`, en fonction du nombre de factums qu'il contient (fonction `div_separator()`)
    - Ces fichiers sont écrits dans le dossier de la collection (`/xml/<nom de la collection>/`)
2. **Création et compilation d'un fichier LaTeX réunissant tous les factums.**
  - Correspond au choix `P` de la chaîne de traitement.
  - Déroulé :
    - Les textes de chaque factum sont mis à la suite et rassemblés dans un ensemble correspondant à leur collection (fonction `make_doc_list()`)
    - Les collections sont elles-même mises à la suite les unes des autres et insérées dans un fichier `.xml` principal (fonction `make_tei_doc()`)
    - Le fichier `.xml` principal transformé en fichier `.tex` avec une feuille de style XSL (fonction `xsl_transformation()`)
    - Le fichier `.tex` est nettoyé et compilé en PDF (fonction `tex_compil()`)
3. **Ajout d'un ou plusieurs nouveaux factums à une collection déjà existante.**
  - Le fichier `.docx` du ou des nouveaux factums est converti en un fichier `.xml` primitif avec [Pandoc](https://pandoc.org/) (fonction `pandoc_convert()`)
  - Le fichier `.xml` primitif est nettoyé avec des expressions régulières (regex) (fonction `txt_cleaning()`)
  - L'utilisateur choisi la collection cible
  - Un fichier `.xml` principal est créé avec les factums déjà présents dans la collection (fonction `div_gathering()`)
  - Le ou les nouveaux factums sont insérés à l'emplacement défini par l'utilisateur dans ce fichier principal (fonction `add_new_div()`)
  - Le fichier principal est séparé en plusieurs fichiers `.xml`, en fonction du nombre de factums qu'il contient (fonction `div_separator()`)

Pour activer la châine de traitement :
  - Demander à `gestion [.] sourcesetdonnees [at] ehess [.] fr` :
    - le fichier CSV contenant les indexations des factums
    - les document `.docx`
  - Installer un environnement virtuel Python
  - Installer les librairies (`requirements.txt`)
  - Lancer `python3 mani_factum.py`

## Arborescence

```
factums-metiers
  |
  |-- img : images indexées dans les XML
  |
  |-- py : châine de traitement Python
  |
  |-- tei-publisher : notice sur la procédure de correction des textes sur l'application TEI Publisher
  |
  |-- tex : fichier tex des factums
  |
  |-- xml : fichiers XML des factums
  |
  |-- xsl : feuille de style permettant la transformation du XML en tex
```

## Licence

- [Etalab 2.0](https://www.etalab.gouv.fr/licence-ouverte-open-licence/)

## Contact

:email: `gestion [.] sourcesetdonnees [at] ehess [.] fr`
<!--
Full processing pipeline with :

1. Transformation of .docx files into .xml files ;
2. Creating as many .xml files as there are <div> in the .xml file of a factum series ;
3. Creation of a main .xml file with as many <divs> as there are collections of factums and, within these <divs>, as many <divs> as there are factums in a collection
4. Transformation of the main .xml file into a .tex file and compilation of a PDF of all factum series.

# Scripts 
1. `pandoc_convert()` : converts a Word document to XML using pandoc and writes it to `output_from_pandoc/`.
2. `txt_cleaning()`:
    - Calls the `regex_cleaning()` function to clean the code obtained from pandoc conversion using regular expressions and writes it to essai.xml to check for any errors.
    - Calls the `xml_cleaning()` function to parse the document with lxml and modify the `<teiHeader>` by adding new `<fileDesc>` and `<profileDesc>` (for the `<textClass>` where the keywords will be) ;
    Writes the modified document to `output_from_script/`.
3. `add_new_div()`: if necessary, adds `<div>` elements to an existing directory.
4. `div_separator()`:
    - Reads the metadata CSV file (extracted from FileMaker);
    - Creates an empty `<teiHeader>` with mostly empty tags;
    - Iterates over each `<div>` in the `<body>` and populates the `<teiHeader>` with information extracted from the title (the `<head>` of the `<body>`) or the metadata CSV file;
    - Writes the entire TEI tree;
    - Creates the file and writes the code inside.
5. `make_doc_list()`: iterates over the list of directories in main_factum.py to
    - Create a new `<head>` with the chapter number, title, date, and reference;
    - Returns a dictionary where the path of the file (a factum) has its `<body>` `<div>` as its value.
6. `make_tei_doc()` :
    - Creates a `<div>` for each collection of factum, with its dates, from the list in `main_factum.py`;
    - Using the dictionary created by `make_doc_list()`, adds each factum `<div>` into the `<div>` of its collection;
    - Writes the resulting tree to `full_factum.xml.`
7. `tex_compil()` :
    - Transforms `full_factum.xml` into `full_factum.tex` using the `xsl_transformation()` function;
    - Cleans the tex document (symbol escaping, etc.);
    - Runs two compilations of the tex document and writes `full_factum.pdf`.
-->